package com.qwentum.cheatsheet2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.afollestad.materialdialogs.util.DialogUtils;
import com.qwentum.cheatsheet2.object.ClassBody;
import com.qwentum.cheatsheet2.object.ClassLocation;
import com.qwentum.cheatsheet2.object.ClassTeacher;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Lesson;
import com.qwentum.cheatsheet2.object.LessonBody;
import com.qwentum.cheatsheet2.other.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class LessonViewActivity extends AppCompatActivity implements View.OnClickListener, ColorChooserDialog.ColorCallback, View.OnFocusChangeListener {

    int selectedBodyID = -99, selectedLocationID = -99, selectedTeacherID = -99;
    SparseArray<ClassBody> classBodies;
    SparseArray<ClassTeacher> classTeachers;
    SparseArray<ClassLocation> classLocations;
    private Context mCtx;
    private CoreSingleton mCoreS;
    private TextView mStartTime, mEndTime;
    private CircleImageView mCircleColor;
    private TimePicker timePicker = null;
    private Spinner mSpinnerDate;
    private String selectedColor = "FF8BC34A";
    private String lessonToEdit;
    private int lessonToEditPosition, lessonBodyToEdit;
    private AutoCompleteTextView mSubjectName, mSubjectAcronym, mSubjectNumber, mTeacherFirstname, mTeacherSurname, mTeacherDesc, mLocationName, mLocationDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_view);
        mCtx = getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mCtx);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        classBodies = mCoreS.getmBodies();
        classTeachers = mCoreS.getmTeachers();
        classLocations = mCoreS.getmLocations();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            lessonToEdit = extras.getString("lessonToEdit");
            lessonToEditPosition = extras.getInt("lessonToEditPosition");
            lessonBodyToEdit = extras.getInt("lessonBodyToEdit");
        }

        mStartTime = (TextView) findViewById(R.id.text_start_time);
        mStartTime.setOnClickListener(this);
        mEndTime = (TextView) findViewById(R.id.text_end_time);
        mEndTime.setOnClickListener(this);
        mCircleColor = (CircleImageView) findViewById(R.id.circle_subject_color_picker);
        mCircleColor.setOnClickListener(this);
        mSpinnerDate = (Spinner) findViewById(R.id.spinner_subject_day);
        mSpinnerDate.setSelection(Helper.calendarGet(Calendar.DAY_OF_WEEK));

        mSubjectName = (AutoCompleteTextView) findViewById(R.id.auto_complete_subject_name);
        mSubjectAcronym = (AutoCompleteTextView) findViewById(R.id.auto_subject_acronym);
        mSubjectNumber = (AutoCompleteTextView) findViewById(R.id.auto_subject_number);
        mTeacherFirstname = (AutoCompleteTextView) findViewById(R.id.auto_teacher_name);
        mTeacherSurname = (AutoCompleteTextView) findViewById(R.id.auto_teacher_surname);
        mTeacherDesc = (AutoCompleteTextView) findViewById(R.id.auto_teacher_description);
        mLocationName = (AutoCompleteTextView) findViewById(R.id.auto_location_name);
        mLocationDesc = (AutoCompleteTextView) findViewById(R.id.auto_location_description);
    }

    @Override
    protected void onStart() {
        super.onStart();

        ArrayList<String> subjectNames = new ArrayList<>();
        for (int i = 0; i < classBodies.size(); i++) {
            subjectNames.add(classBodies.get(classBodies.keyAt(i)).getmName());
        }

        ArrayList<String> teacherNames = new ArrayList<>();
        ArrayList<String> teacherSurnames = new ArrayList<>();
        for (int i = 0; i < classTeachers.size(); i++) {
            teacherNames.add(classTeachers.get(classTeachers.keyAt(i)).getmName());
            teacherSurnames.add(classTeachers.get(classTeachers.keyAt(i)).getmSurname());
        }

        ArrayList<String> locationNames = new ArrayList<>();
        for (int i = 0; i < classLocations.size(); i++) {
            locationNames.add(classLocations.get(classLocations.keyAt(i)).getmName());
        }

        ArrayAdapter<String> subjectNameAdapter = new ArrayAdapter<>(mCtx, android.R.layout.simple_list_item_1, subjectNames);
        ArrayAdapter<String> teacherNameAdapter = new ArrayAdapter<>(mCtx, android.R.layout.simple_list_item_1, teacherNames);
        ArrayAdapter<String> teacherSurnameAdapter = new ArrayAdapter<>(mCtx, android.R.layout.simple_list_item_1, teacherSurnames);
        ArrayAdapter<String> locationNameAdapter = new ArrayAdapter<>(mCtx, android.R.layout.simple_list_item_1, locationNames);
        mSubjectName.setAdapter(subjectNameAdapter);
        mSubjectName.setOnFocusChangeListener(this);
        mTeacherFirstname.setAdapter(teacherNameAdapter);
        mTeacherFirstname.setOnFocusChangeListener(this);
        mTeacherSurname.setAdapter(teacherSurnameAdapter);
        mTeacherSurname.setOnFocusChangeListener(this);
        mLocationName.setAdapter(locationNameAdapter);
        mLocationName.setOnFocusChangeListener(this);

        mSubjectName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < classBodies.size(); i++) {
                    ClassBody selectedBody = classBodies.get(classBodies.keyAt(i));
                    if (selectedBody.getmName().equals(selection)) {
                        selectedBodyID = classBodies.keyAt(i);
                        mCircleColor.setColorFilter(Color.parseColor(selectedBody.getmColor()));
                        selectedColor = selectedBody.getmColor();
                        mSubjectAcronym.setText(selectedBody.getmAcronym());
                        break;
                    }
                }
            }
        });

        mTeacherFirstname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                ArrayList<String> applicableTeacherSurnames = new ArrayList<>();
                final ArrayList<Integer> selectedTeacherTempID = new ArrayList<>();
                for (int i = 0; i < classTeachers.size(); i++) {
                    ClassTeacher selectedTeacher = classTeachers.get(classTeachers.keyAt(i));
                    if (selectedTeacher.getmName().equals(selection)) {
                        applicableTeacherSurnames.add(selectedTeacher.getmSurname());
                        selectedTeacherTempID.add(i);
                    }
                }
                if (applicableTeacherSurnames.size() == 1) {
                    mTeacherFirstname.setText(selection);
                    mTeacherSurname.setText(applicableTeacherSurnames.get(0));
                    selectedTeacherID = selectedTeacherTempID.get(0);
                } else if (applicableTeacherSurnames.size() > 1) {
                    mTeacherFirstname.setText(selection);
                    mTeacherSurname.setAdapter(new ArrayAdapter<>(mCtx, android.R.layout.simple_list_item_1, applicableTeacherSurnames));
                    mTeacherSurname.showDropDown();
                    mTeacherSurname.requestFocus();
                    mTeacherSurname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectedTeacherID = selectedTeacherTempID.get(position);
                        }
                    });
                }
            }
        });

        mLocationName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < classLocations.size(); i++) {
                    ClassLocation selectedLocation = classLocations.get(classLocations.keyAt(i));
                    if (selectedLocation.getmName().equals(selection)) {
                        selectedLocationID = classLocations.keyAt(i);
                    }
                }
            }
        });

        if (lessonToEdit != null) {
            try {
                //Log.e(getClass().getName(),lessonToEdit);
                //Populate views with supplied lesson
                Lesson editLesson = new Lesson(new JSONObject(lessonToEdit));

                ClassBody selectedBody = classBodies.get(editLesson.getmBodies().get(lessonBodyToEdit).getmBodyId());
                selectedBodyID = editLesson.getmBodies().get(lessonBodyToEdit).getmBodyId();
                ClassLocation selectedLocation = classLocations.get(editLesson.getmBodies().get(lessonBodyToEdit).getmLocationId());
                selectedLocationID = editLesson.getmBodies().get(lessonBodyToEdit).getmLocationId();
                ClassTeacher selectedTeacher = classTeachers.get(editLesson.getmBodies().get(lessonBodyToEdit).getmTeacherId());
                selectedTeacherID = editLesson.getmBodies().get(lessonBodyToEdit).getmTeacherId();

                mSubjectName.setText(selectedBody.getmName());
                mSubjectAcronym.setText(selectedBody.getmAcronym());
                mSubjectNumber.setText(String.valueOf(editLesson.getmNumber()));
                mStartTime.setText(editLesson.getmStart());
                mEndTime.setText(editLesson.getmEnd());
                mCircleColor.setColorFilter(Color.parseColor(selectedBody.getmColor()));
                selectedColor = selectedBody.getmColor();

                mLocationName.setText(selectedLocation.getmName());
                mLocationDesc.setText(selectedLocation.getmDescription());

                mTeacherFirstname.setText(selectedTeacher.getmName());
                mTeacherSurname.setText(selectedTeacher.getmSurname());
                mTeacherDesc.setText(selectedTeacher.getmDescription());

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.auto_complete_subject_name:
                if (hasFocus) {
                    mSubjectName.showDropDown();
                }
                break;
            case R.id.auto_teacher_name:
                if (hasFocus) {
                    mTeacherFirstname.showDropDown();
                }
                break;
            case R.id.auto_teacher_surname:
                if (hasFocus) {
                    mTeacherSurname.showDropDown();
                }
                break;
            case R.id.auto_location_name:
                if (hasFocus) {
                    mLocationName.showDropDown();
                }
                break;
        }
    }

    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.text_end_time:
            case R.id.text_start_time:
                MaterialDialog timePickerDialog = new MaterialDialog.Builder(this)
                        .customView(R.layout.layout_time_picker, false)
                        .positiveText("set")
                        .neutralText("cancel")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (v.getId() == R.id.text_end_time) {
                                    String hour, minute;
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        if (timePicker.getHour() < 10) {
                                            hour = "0" + timePicker.getHour();
                                        } else {
                                            hour = String.valueOf(timePicker.getHour());
                                        }
                                        if (timePicker.getMinute() < 10) {
                                            minute = "0" + timePicker.getMinute();
                                        } else {
                                            minute = String.valueOf(timePicker.getMinute());
                                        }
                                    } else {
                                        if (timePicker.getCurrentHour() < 10) {
                                            hour = "0" + timePicker.getCurrentHour();
                                        } else {
                                            hour = String.valueOf(timePicker.getCurrentHour());
                                        }
                                        if (timePicker.getCurrentMinute() < 10) {
                                            minute = "0" + timePicker.getCurrentMinute();
                                        } else {
                                            minute = String.valueOf(timePicker.getCurrentMinute());
                                        }
                                    }
                                    mEndTime.setText(hour + ":" + minute);
                                } else {
                                    String hour, minute;
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        if (timePicker.getHour() < 10) {
                                            hour = "0" + timePicker.getHour();
                                        } else {
                                            hour = String.valueOf(timePicker.getHour());
                                        }
                                        if (timePicker.getMinute() < 10) {
                                            minute = "0" + timePicker.getMinute();
                                        } else {
                                            minute = String.valueOf(timePicker.getMinute());
                                        }
                                    } else {
                                        if (timePicker.getCurrentHour() < 10) {
                                            hour = "0" + timePicker.getCurrentHour();
                                        } else {
                                            hour = String.valueOf(timePicker.getCurrentHour());
                                        }
                                        if (timePicker.getCurrentMinute() < 10) {
                                            minute = "0" + timePicker.getCurrentMinute();
                                        } else {
                                            minute = String.valueOf(timePicker.getCurrentMinute());
                                        }
                                    }
                                    mStartTime.setText(hour + ":" + minute);
                                }
                            }
                        })
                        .build();
                timePicker = (TimePicker) timePickerDialog.getCustomView().findViewById(R.id.time_picker);
                timePickerDialog.show();
                break;
            case R.id.circle_subject_color_picker:
                new ColorChooserDialog.Builder(this, R.string.text_post_color_chooser)
                        .titleSub(R.string.text_post_color_chooser)  // title of dialog when viewing shades of a color
                        .accentMode(false)  // when true, will display accent palette instead of primary palette
                        .doneButton(R.string.md_done_label)  // changes label of the done button
                        .cancelButton(R.string.md_cancel_label)  // changes label of the cancel button
                        .backButton(R.string.md_back_label)  // changes label of the back button
                        .dynamicButtonColor(true)  // defaults to true, false will disable changing action buttons' color to currently selected color
                        .show();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_lesson, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_new_lesson_save:

                ClassBody tempBody = (selectedBodyID != -99) ? classBodies.get(selectedBodyID) : null;
                ClassLocation tempLocation = (selectedLocationID != -99) ? classLocations.get(selectedTeacherID) : null;
                ClassTeacher tempTeacher = (selectedTeacherID != -99) ? classTeachers.get(selectedLocationID) : null;
                if (selectedBodyID == -99 || (!mSubjectName.equals(tempBody.getmName()) || !mSubjectAcronym.equals(tempBody.getmAcronym()) || !selectedColor.equals("FF" + tempBody.getmColor()))) {
                    mCoreS.addNewClassSubject("{" + "\"name\":\"" + mSubjectName.getText() + "\","
                            + "\"acronym\":\"" + mSubjectAcronym.getText() + "\","
                            + "\"icon\":" + null + ","
                            + "\"color\":\"" + selectedColor + "\"}"
                    );
                    selectedBodyID = mCoreS.numberOfNewSubjects;
                }
                if (selectedTeacherID == -99 || (!mTeacherFirstname.equals(tempTeacher.getmName()) || !mTeacherSurname.equals(tempTeacher.getmSurname()) || !mTeacherDesc.equals(tempTeacher.getmDescription()))) {
                    mCoreS.addNewClassTeacher("{" + "\"name\":\"" + mTeacherFirstname.getText() + "\","
                            + "\"surname\":\"" + mTeacherSurname.getText() + "\","
                            + "\"description\":\"" + mTeacherDesc.getText() + "\"}"
                    );
                    selectedTeacherID = mCoreS.numberOfNewTeachers;
                }
                if (selectedLocationID == -99 || (!mLocationName.equals(tempLocation.getmName()) || !mLocationDesc.equals(tempLocation.getmDescription()))) {
                    mCoreS.addNewClassLocation("{" + "\"name\":\"" + mLocationName.getText() + "\","
                            + "\"description\":\"" + mLocationDesc.getText() + "\"}"
                    );
                    selectedLocationID = mCoreS.numberOfNewLocations;
                }


                Intent data = new Intent();
                if (lessonToEdit != null) {
                    String bodies = "\"bodies\": [";
                    Lesson suppliedLesson = null;
                    try {
                        suppliedLesson = new Lesson(new JSONObject(lessonToEdit));
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                    ArrayList<LessonBody> classBodies = suppliedLesson.getmBodies();
                    for (int i = 0; i < classBodies.size(); i++) {
                        if (i != lessonBodyToEdit) {
                            LessonBody currentBody = classBodies.get(i);
                            bodies = bodies.concat("{\"locationId\":\"" + currentBody.getmLocationId() + "\","
                                    + "\"teacherId\":\"" + currentBody.getmTeacherId() + "\","
                                    + "\"bodyId\":\"" + currentBody.getmBodyId() + "\"}");
                        } else {
                            bodies = bodies.concat("{\"locationId\":\"" + selectedLocationID + "\","
                                    + "\"teacherId\":\"" + selectedTeacherID + "\","
                                    + "\"bodyId\":\"" + selectedBodyID + "\"}");
                        }

                        if (i != classBodies.size() - 1) {
                            bodies = bodies.concat(",");
                        }
                    }
                    bodies = bodies.concat("]");

                    String lessonJSON = "{"
                            + "\"id\":\"" + -1 + "\","
                            + "\"number\":" + mSubjectNumber.getText() + ","
                            + "\"startTime\":\"" + mStartTime.getText() + "\","
                            + "\"endTime\":\"" + mEndTime.getText() + "\","
                            + "\"dayIndex\":" + mSpinnerDate.getSelectedItemPosition() + ","
                            + bodies + "}";

                    data.putExtra("newLesson", lessonJSON);
                    data.putExtra("lessonToEditPosition", lessonToEditPosition);
                } else {
                    data.putExtra("newLesson", "{"
                            + "\"id\":\"" + -1 + "\","
                            //+ "\"position\":\"" + 0 + "\","
                            + "\"number\":" + mSubjectNumber.getText() + ","
                            + "\"startTime\":\"" + mStartTime.getText() + "\","
                            + "\"endTime\":\"" + mEndTime.getText() + "\","
                            + "\"dayIndex\":" + mSpinnerDate.getSelectedItemPosition() + ","
                            + "\"bodies\": [{"
                            + "\"locationId\":\"" + selectedLocationID + "\","
                            + "\"teacherId\":\"" + selectedTeacherID + "\","
                            + "\"bodyId\":\"" + selectedBodyID + "\"}]}");
                }
                setResult(RESULT_OK, data);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onColorSelection(@NonNull ColorChooserDialog dialog, @ColorInt int color) {
        selectedColor = "#FF" + String.format("%06X", 0xFFFFFF & DialogUtils.getActionTextStateList(this, color).getDefaultColor());
        Log.d(getClass().getName(), selectedColor);
        mCircleColor.setColorFilter(Color.parseColor(selectedColor));
    }

    @Override
    public void onColorChooserDismissed(@NonNull ColorChooserDialog dialog) {
        //makeToast("Dismissed", Toast.LENGTH_LONG);
    }
}
