package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Reminder;
import com.qwentum.cheatsheet2.object.SchoolClass;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Marian on 2017-02-22.
 */

public class ReminderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 100;
    public static final int TYPE_ITEM = 101;
    private int mNumberOfHeaders = 0;
    private boolean mShowClass;
    private ArrayList<SchoolClass> mClasses;
    private ArrayList<Reminder> mReminders;
    private ArrayList<Integer> mHeaderPositions = new ArrayList<>();
    private ArrayList<String> mHeaderDates = new ArrayList<>();
    private Context mContext;
    // Pass in the contact array into the constructor
    public ReminderAdapter(ArrayList<Reminder> suppliedReminders, boolean showClass, Context context) {
        mContext = context;
        mReminders = suppliedReminders;
        mNumberOfHeaders = 0;
        mHeaderDates.clear();
        mHeaderPositions.clear();
        if (showClass) {
            mShowClass = showClass;
            mClasses = CoreSingleton.getInstance(mContext).getUserAccount().getSchoolClasses();
        }
        if (mReminders.size() > 0) {
            mHeaderPositions.add(0);
            mHeaderDates.add(mReminders.get(0).getDateOfReminder());
            for (int i = 1; i < mReminders.size(); i++) {
                if (!mHeaderDates.contains(mReminders.get(i).getDateOfReminder())) {
                    int offset = mHeaderPositions.size();
                    mHeaderPositions.add(i + offset);
                    mHeaderDates.add(mReminders.get(i).getDateOfReminder());
                }
            }
        }
    }

    public void updateContent(ArrayList<Reminder> reminders) {
        if (reminders != null) {
            Log.d("Reminders", "reminders + " + reminders.size());
            mReminders = reminders;
            mNumberOfHeaders = 0;
            mHeaderDates.clear();
            mHeaderPositions.clear();
            if (mReminders.size() > 0) {
                mHeaderPositions.add(0);
                mHeaderDates.add(mReminders.get(0).getDateOfReminder());
                for (int i = 1; i < mReminders.size(); i++) {
                    if (!mHeaderDates.contains(mReminders.get(i).getDateOfReminder())) {
                        int offset = mHeaderPositions.size();
                        mHeaderPositions.add(i + offset);
                        mHeaderDates.add(mReminders.get(i).getDateOfReminder());
                    }
                }
            }
        } else {
            Log.d("Reminders", "reminders null");
            mReminders.clear();
            mNumberOfHeaders = 0;
            mHeaderDates.clear();
            mHeaderPositions.clear();
        }
        notifyDataSetChanged();
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_ITEM) {
            View itemView = inflater.inflate(R.layout.item, parent, false);
            return new ReminderItemViewHolder(itemView);
        } else {
            View headerView = inflater.inflate(R.layout.reminder_header, parent, false);
            return new ReminderHeaderViewHolder(headerView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder.getItemViewType() == TYPE_ITEM) {
            Reminder currentReminder = mReminders.get(position - mNumberOfHeaders);
            String className = "";
            if (mClasses != null) {
                for (SchoolClass schoolClass : mClasses) {
                    if (schoolClass.getCLassId().equals(currentReminder.getClassId()))
                        className = schoolClass.getNameShort();
                }
            }
            ReminderItemViewHolder itemViewHolder = (ReminderItemViewHolder) viewHolder;
            itemViewHolder.mReminderName.setText(mShowClass ? className + " - " + currentReminder.getReminderName() : currentReminder.getReminderName());
            itemViewHolder.mReminderDesc.setText(currentReminder.getSubject());
            Resources res = mContext.getResources();
            switch (currentReminder.getType()) {
                case "0":
                    itemViewHolder.mReminderID.bind(res.getDrawable(R.drawable.ic_chrome_reader_mode_black_24dp));
                    itemViewHolder.mReminderID.changeColorByHex("#2196F3");
                    break;
                case "1":
                    itemViewHolder.mReminderID.bind(res.getDrawable(R.drawable.ic_assessment_black_24dp));
                    itemViewHolder.mReminderID.changeColorByHex("#795548");
                    break;
                case "2":
                    itemViewHolder.mReminderID.bind(res.getDrawable(R.drawable.ic_group_black_24dp));
                    itemViewHolder.mReminderID.changeColorByHex("#FF9800");
                    break;
                case "3":
                    itemViewHolder.mReminderID.bind(res.getDrawable(R.drawable.ic_assignment_black_24dp));
                    itemViewHolder.mReminderID.changeColorByHex("#FF5722");
                    break;
                case "4":
                    itemViewHolder.mReminderID.bind(res.getDrawable(R.drawable.ic_assignment_late_black_24dp));
                    itemViewHolder.mReminderID.changeColorByHex("#F44336");
                    break;
                case "5":
                    itemViewHolder.mReminderID.bind(res.getDrawable(R.drawable.ic_book_black_24dp));
                    itemViewHolder.mReminderID.changeColorByHex("#CDDC39");
                    break;
            }
        } else {
            ReminderHeaderViewHolder headerViewHolder = (ReminderHeaderViewHolder) viewHolder;
            Log.d("Reminder", "Number of headers " + mNumberOfHeaders);
            String dateOfReminder = mHeaderDates.get(mNumberOfHeaders);
            headerViewHolder.mReminderDueTime.setText(getTimeString(dateOfReminder));
            headerViewHolder.mReminderDueDate.setText(dateOfReminder);
            mNumberOfHeaders++;
        }
    }

    private String getTimeString(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            Calendar cal = Calendar.getInstance();
            Date date1 = cal.getTime();
            Date date2 = sdf.parse(date);
            long diff = date2.getTime() - date1.getTime();
            int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            switch (days) {
                case 0:
                    if (String.valueOf(cal.get(Calendar.DAY_OF_MONTH)).equals(date.substring(8, 10))) {
                        return "Due today!";
                    } else {
                        return "Due tomorrow!";
                    }
                case 1:
                    if (Integer.parseInt(date.substring(8, 10)) - cal.get(Calendar.DAY_OF_MONTH) == 1) {
                        return "Due tomorrow!";
                    }
                default:
                    return "Due in " + (days + 1) + " days";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mReminders.size() + mHeaderPositions.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mHeaderPositions.contains(position)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    public class ReminderItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mReminderName, mReminderDesc;
        public AvatarView mReminderID;

        public ReminderItemViewHolder(View itemView) {
            super(itemView);
            mReminderID = (AvatarView) itemView.findViewById(R.id.avatarView);
            mReminderName = (TextView) itemView.findViewById(R.id.itemName);
            mReminderDesc = (TextView) itemView.findViewById(R.id.itemDesc);
        }
    }

    public class ReminderHeaderViewHolder extends RecyclerView.ViewHolder {

        public TextView mReminderDueTime, mReminderDueDate;

        public ReminderHeaderViewHolder(View itemView) {
            super(itemView);
            mReminderDueTime = (TextView) itemView.findViewById(R.id.reminder_due_time);
            mReminderDueDate = (TextView) itemView.findViewById(R.id.reminder_due_date);
        }
    }
}
