package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.ClassActivity;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-26.
 */

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityViewHolder> {

    private static final int TYPE_MESSAGE = 0;
    private static final int TYPE_POST = 1;
    private static final int TYPE_REMINDER = 2;
    private ArrayList<ClassActivity> mClassActivities;
    private Context mContext;
    public ActivityAdapter(ArrayList<ClassActivity> classActivities, Context context) {
        mClassActivities = classActivities;
        mContext = context;
    }

    @Override
    public ActivityAdapter.ActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ActivityViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_holder, parent, false));
    }

    public void updateContent(ArrayList<ClassActivity> activities, int start, int count) {
        //mClassActivities.clear();
        //notifyDataSetChanged();
        if (activities != null) {
            mClassActivities = activities;
            notifyItemRangeInserted(start, count);
        } else {
            mClassActivities.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(ActivityAdapter.ActivityViewHolder viewHolder, int position) {
        ClassActivity currentActivity = mClassActivities.get(position);
        Resources res = mContext.getResources();
        switch (viewHolder.getItemViewType()) {
            case TYPE_MESSAGE:
                viewHolder.mPosterAvatar.bind(res.getDrawable(R.drawable.ic_message_black_24dp));
                viewHolder.mPosterAvatar.changeColorByHex("#673AB7");
                viewHolder.mActivityHeaderTitle.setText(currentActivity.getmPosterName() + ' ' + currentActivity.getmPosterSurname() + " posted a new message");
                viewHolder.mActivityHeaderDesc.setText(currentActivity.getmDateCreated());
                viewHolder.mMessageText.setVisibility(View.VISIBLE);
                viewHolder.mMessageText.setText(currentActivity.getmHeader());
                break;
            case TYPE_POST:
                viewHolder.mPosterAvatar.bind(res.getDrawable(R.drawable.ic_file_upload_black_24dp));
                viewHolder.mPosterAvatar.changeColorByHex("#3F51B5");
                viewHolder.mActivityHeaderTitle.setText(currentActivity.getmPosterName() + ' ' + currentActivity.getmPosterSurname() + " created a post");
                viewHolder.mActivityHeaderDesc.setText(currentActivity.getmDateCreated());
                viewHolder.mPostView.setVisibility(View.VISIBLE);
                TextView contentTitle = (TextView) viewHolder.mPostView.findViewById(R.id.activity_post_title);
                contentTitle.setText(currentActivity.getmSubject() + " - " + currentActivity.getmHeader());
                TextView contentDesc = (TextView) viewHolder.mPostView.findViewById(R.id.activity_post_desc);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    contentDesc.setText(Html.fromHtml(currentActivity.getmPostContent(), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    contentDesc.setText(Html.fromHtml(currentActivity.getmPostContent()));
                }
                break;
            case TYPE_REMINDER:
                viewHolder.mPosterAvatar.bind(currentActivity.getmPosterName().substring(0, 1), "");
                viewHolder.mPosterAvatar.changeColorById(R.color.colorAccent);
                viewHolder.mActivityHeaderTitle.setText(currentActivity.getmPosterName() + ' ' + currentActivity.getmPosterSurname() + " created a reminder");
                viewHolder.mActivityHeaderDesc.setText(currentActivity.getmDateCreated());
                viewHolder.mReminderView.setVisibility(View.VISIBLE);
                TextView reminderTitle = (TextView) viewHolder.mReminderView.findViewById(R.id.itemName);
                reminderTitle.setText(currentActivity.getmHeader());
                TextView reminderDesc = (TextView) viewHolder.mReminderView.findViewById(R.id.itemDesc);
                reminderDesc.setText(currentActivity.getmSubject());
                AvatarView reminderAvatar = (AvatarView) viewHolder.mReminderView.findViewById(R.id.avatarView);
                TextView reminderDate = (TextView) viewHolder.mReminderView.findViewById(R.id.itemTime);
                reminderDate.setText(currentActivity.getmReminderDate());
                switch (currentActivity.getmReminderType()) {
                    case "0":
                        reminderAvatar.bind(res.getDrawable(R.drawable.ic_chrome_reader_mode_black_24dp));
                        reminderAvatar.changeColorByHex("#2196F3");
                        break;
                    case "1":
                        reminderAvatar.bind(res.getDrawable(R.drawable.ic_assessment_black_24dp));
                        reminderAvatar.changeColorByHex("#795548");
                        break;
                    case "2":
                        reminderAvatar.bind(res.getDrawable(R.drawable.ic_group_black_24dp));
                        reminderAvatar.changeColorByHex("#FF9800");
                        break;
                    case "3":
                        reminderAvatar.bind(res.getDrawable(R.drawable.ic_assignment_black_24dp));
                        reminderAvatar.changeColorByHex("#FF5722");
                        break;
                    case "4":
                        reminderAvatar.bind(res.getDrawable(R.drawable.ic_assignment_late_black_24dp));
                        reminderAvatar.changeColorByHex("#F44336");
                        break;
                    case "5":
                        reminderAvatar.bind(res.getDrawable(R.drawable.ic_book_black_24dp));
                        reminderAvatar.changeColorByHex("#CDDC39");
                        break;
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mClassActivities.size();
    }

    @Override
    public int getItemViewType(int position) {
        String type = mClassActivities.get(position).getmActivityType();
        switch (type) {
            case "message":
                return TYPE_MESSAGE;
            case "post":
                return TYPE_POST;
            case "reminder":
                return TYPE_REMINDER;
            default:
                return -1;
        }
    }

    public class ActivityViewHolder extends RecyclerView.ViewHolder {

        public AvatarView mPosterAvatar;
        public TextView mActivityHeaderTitle, mActivityHeaderDesc, mMessageText;
        public View mPostView, mReminderView;

        public ActivityViewHolder(View itemView) {
            super(itemView);
            View header = itemView.findViewById(R.id.activity_header);
            mPosterAvatar = (AvatarView) header.findViewById(R.id.avatarView);
            mActivityHeaderTitle = (TextView) header.findViewById(R.id.itemName);
            mActivityHeaderDesc = (TextView) header.findViewById(R.id.itemDesc);
            mMessageText = (TextView) itemView.findViewById(R.id.activity_message);
            mPostView = itemView.findViewById(R.id.activity_post);
            mReminderView = itemView.findViewById(R.id.activity_reminder);
        }
    }
}
