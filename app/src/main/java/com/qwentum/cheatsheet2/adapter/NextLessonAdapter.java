package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.ClassBody;
import com.qwentum.cheatsheet2.object.ClassLocation;
import com.qwentum.cheatsheet2.object.ClassTeacher;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.OverviewLesson;
import com.qwentum.cheatsheet2.object.OverviewLessonBody;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-22.
 */

public class NextLessonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 100;
    public static final int TYPE_ITEM = 101;
    int numberOfHeaders = 0;
    private Context mCtx;
    private CoreSingleton mCoreS;
    private ArrayList<OverviewLesson> mLessons;
    private ArrayList<OverviewLessonBody> mBodies = new ArrayList<>();
    private SparseArray<ClassBody> mClassBodies;
    private SparseArray<ClassTeacher> mClassTeachers;
    private SparseArray<ClassLocation> mClassLocations;
    private ArrayList<Integer> mHeaderPositions = new ArrayList<>();
    private boolean mIsCurrent;
    // Pass in the contact array into the constructor
    public NextLessonAdapter(ArrayList<OverviewLesson> lessons, boolean isCurrent, Context context) {
        mCtx = context;
        mCoreS = CoreSingleton.getInstance(mCtx);
        mClassBodies = mCoreS.getmBodies();
        mClassTeachers = mCoreS.getmTeachers();
        mClassLocations = mCoreS.getmLocations();
        mLessons = lessons;
        mIsCurrent = isCurrent;
        numberOfHeaders = 0;
        if (mLessons.size() > 0) {
            mHeaderPositions.add(0);
            mHeaderPositions.add(mLessons.get(0).getmBodies().size() + 1);
            for (int i = 0; i < mLessons.size(); i++) {
                ArrayList<OverviewLessonBody> bodies = mLessons.get(i).getmBodies();
                for (int j = 0; j < bodies.size(); j++) {
                    mBodies.add(bodies.get(j));
                }
            }
        }
    }

    public void updateContent(ArrayList<OverviewLesson> lessons, boolean isCurrent) {
        if (lessons != null) {
            mLessons = lessons;
            mIsCurrent = isCurrent;
            numberOfHeaders = 0;
            mBodies.clear();
            mHeaderPositions.clear();
            if (mLessons.size() > 0) {
                mHeaderPositions.add(0);
                mHeaderPositions.add(mLessons.get(0).getmBodies().size() + 1);
                for (int i = 0; i < mLessons.size(); i++) {
                    ArrayList<OverviewLessonBody> bodies = mLessons.get(i).getmBodies();
                    for (int j = 0; j < bodies.size(); j++) {
                        mBodies.add(bodies.get(j));
                    }
                }
            }
        } else {
            mLessons.clear();
            mIsCurrent = false;
            numberOfHeaders = 0;
            mBodies.clear();
            mHeaderPositions.clear();
        }
        notifyDataSetChanged();
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_ITEM) {
            View itemView = inflater.inflate(R.layout.item, parent, false);
            return new LessonItemViewHolder(itemView);
        } else {
            View headerView = inflater.inflate(R.layout.reminder_header, parent, false);
            return new LessonHeaderViewHolder(headerView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder.getItemViewType() == TYPE_ITEM) {
            OverviewLessonBody currentLessonBody = mBodies.get(position - numberOfHeaders);
            LessonItemViewHolder itemViewHolder = (LessonItemViewHolder) viewHolder;
            itemViewHolder.mLessonName.setText(currentLessonBody.getmBodyName());
            itemViewHolder.mLessonDesc.setText(currentLessonBody.getmTeacherName() + " " + currentLessonBody.getmTeacherSurname() + " - " + currentLessonBody.getmLocationName());
            itemViewHolder.mLessonID.bind(currentLessonBody.getmBodyName().substring(0, 1), "");
            itemViewHolder.mLessonID.changeColorByHex(currentLessonBody.getmColor());
        } else {
            LessonHeaderViewHolder headerViewHolder = (LessonHeaderViewHolder) viewHolder;
            OverviewLesson currentLesson = mLessons.get(numberOfHeaders);
            if (mIsCurrent && numberOfHeaders == 0) {
                headerViewHolder.mLessonNumber.setText(currentLesson.getmNumber() + " - In progress");
                //headerViewHolder.mLessonTime.setText(currentLesson.getmEnd());
            } else {
                headerViewHolder.mLessonNumber.setText(currentLesson.getmNumber() + " - Upcoming");
            }
            headerViewHolder.mLessonTime.setText(currentLesson.getmStart() + " - " + currentLesson.getmEnd());
            numberOfHeaders++;
        }
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mBodies.size() + mHeaderPositions.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mHeaderPositions.contains(position)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    public class LessonItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mLessonName, mLessonDesc;
        public AvatarView mLessonID;

        public LessonItemViewHolder(View itemView) {
            super(itemView);
            mLessonID = (AvatarView) itemView.findViewById(R.id.avatarView);
            mLessonName = (TextView) itemView.findViewById(R.id.itemName);
            mLessonDesc = (TextView) itemView.findViewById(R.id.itemDesc);
        }
    }

    public class LessonHeaderViewHolder extends RecyclerView.ViewHolder {

        public TextView mLessonNumber, mLessonTime;

        public LessonHeaderViewHolder(View itemView) {
            super(itemView);
            mLessonNumber = (TextView) itemView.findViewById(R.id.reminder_due_time);
            mLessonTime = (TextView) itemView.findViewById(R.id.reminder_due_date);
        }
    }
}
