package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-28.
 */

public class PostTagAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_TAG = 0;
    private static final int TYPE_TAG_CREATE = 1;
    private ArrayList<String> mTags;
    private boolean mCanInteract = true;

    public PostTagAdapter(ArrayList<String> tags) {
        mTags = tags;
    }

    public void addTag(String tag) {
        mTags.add(tag);
    }

    public void removeTag(int index) {
        Log.d("TagAdapter", "Tag count = " + mTags.size() + " removing index " + index);
        mTags.remove(index);
    }

    public String getTagsJson() {
        String start = "[";
        String end = "]";
        String content = "";
        for (int i = 0; i < mTags.size(); i++) {
            content += '"' + mTags.get(i) + '"';
            if (i != mTags.size() - 1) content += ',';
        }
        return start + content + end;
    }

    public void canInteract(boolean can) {
        mCanInteract = can;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Return a new holder instance
        if (viewType == TYPE_TAG) {
            View contactView = inflater.inflate(R.layout.post_tag, parent, false);
            PostTagViewHolder viewHolder = new PostTagViewHolder(contactView);
            return viewHolder;
        } else {
            View contactView = inflater.inflate(R.layout.post_tag_add, parent, false);
            PostTagCreateViewHolder viewHolder = new PostTagCreateViewHolder(contactView);
            return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        // Get the data model based on position

        //TODO VIEW BINDING
        if (viewHolder.getItemViewType() == TYPE_TAG) {
            String tagText = mTags.get(position);
            PostTagViewHolder tagViewHolder = (PostTagViewHolder) viewHolder;
            tagViewHolder.tagText.setText(tagText);
            if (!mCanInteract) tagViewHolder.tagClear.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (mCanInteract) {
            return mTags.size() + 1;
        } else {
            return mTags.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mTags.size() && mCanInteract) {
            return TYPE_TAG_CREATE;
        } else {
            return TYPE_TAG;
        }
    }

    public static class PostTagViewHolder extends RecyclerView.ViewHolder {
        public TextView tagText;
        public ImageView tagClear;

        public PostTagViewHolder(View itemView) {
            super(itemView);
            tagText = (TextView) itemView.findViewById(R.id.post_tag_text);
            tagClear = (ImageView) itemView.findViewById(R.id.post_tag_clear);
        }
    }

    public static class PostTagCreateViewHolder extends RecyclerView.ViewHolder {
        public PostTagCreateViewHolder(View itemView) {
            super(itemView);
        }
    }
}
