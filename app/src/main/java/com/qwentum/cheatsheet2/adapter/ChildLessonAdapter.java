package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.ClassBody;
import com.qwentum.cheatsheet2.object.ClassLocation;
import com.qwentum.cheatsheet2.object.ClassTeacher;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Lesson;
import com.qwentum.cheatsheet2.object.LessonBody;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-22.
 */

public class ChildLessonAdapter extends RecyclerView.Adapter<ChildLessonAdapter.ViewHolder> {

    // Store a member variable for the contacts
    private CoreSingleton mCoreS;
    private ArrayList<LessonBody> mBodies;
    private SparseArray<ClassBody> mClassBodies;
    private SparseArray<ClassTeacher> mClassTeachers;
    private SparseArray<ClassLocation> mClassLocations;
    private Lesson mLesson;
    // Store the context for easy access
    private Context mContext;
    // Pass in the contact array into the constructor
    public ChildLessonAdapter(Lesson suppliedLesson, Context context) {
        mBodies = suppliedLesson.getmBodies();
        mLesson = suppliedLesson;
        mContext = context;
        mCoreS = CoreSingleton.getInstance(mContext);
        mClassBodies = mCoreS.getmBodies();
        mClassTeachers = mCoreS.getmTeachers();
        mClassLocations = mCoreS.getmLocations();
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public ChildLessonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View lessonView = inflater.inflate(R.layout.item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(lessonView);
        return viewHolder;
    }

    // Involves populating data into the item_text through holder
    @Override
    public void onBindViewHolder(ChildLessonAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        //Lesson item_text = mSchoolDay.getLesson(position);
        // Set item_text views based on your views and data model
        //Log.d(TAG, "Lesson name = " + mLesson.getmName(position) + " | position = " + position);
        LessonBody currentLessonBody = mBodies.get(position);
        //Log.d(this.getClass().getName(), "Current lesson body = " + currentLessonBody.toString());
        ClassBody currentClassBody = mClassBodies.get(currentLessonBody.getmBodyId());
        //Log.d(this.getClass().getName(), "Current class body = " + currentClassBody.toString());
        ClassTeacher currentClassTeacher = mClassTeachers.get(currentLessonBody.getmTeacherId());
        //Log.d(this.getClass().getName(), "Current teacher = " + currentClassTeacher.toString());
        ClassLocation currentClassLocation = mClassLocations.get(currentLessonBody.getmLocationId());
        //Log.d(this.getClass().getName(), "Current location ID = " + currentLessonBody.getmLocationId());
        String name = currentClassBody.getmName();
        viewHolder.mLessonName.setText(name);
        viewHolder.mLessonInfo.setText(currentClassTeacher.getmName() + " " + currentClassTeacher.getmSurname() + " - " + currentClassLocation.getmName());
        //viewHolder.mLessonID.setText(name.substring(0,1));
        //TODO TEXT OR DRAWABLE
        if (!name.equals("")) {
            viewHolder.mLessonID.bind(name.substring(0, 1), "");
        }
        viewHolder.mLessonID.changeColorByHex(currentClassBody.getmColor());
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mLesson.getmType();
    }

    // Provide a direct reference to each of the views within a data item_text
    // Used to cache the views within the item_text layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView mLessonName, mLessonInfo;
        public AvatarView mLessonID;
        //public ImageView mLessonColor;

        // We also create a constructor that accepts the entire item_text row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            mLessonID = (AvatarView) itemView.findViewById(R.id.avatarView);
            mLessonName = (TextView) itemView.findViewById(R.id.itemName);
            mLessonInfo = (TextView) itemView.findViewById(R.id.itemDesc);
            //mLessonColor = (ImageView) itemView.findViewById(R.id.lessonColor);
        }
    }
}
