package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.Message;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-26.
 */

public class MessagesInConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_LOCAL_FIRST = 0;
    public static final int TYPE_LOCAL_CENTER = 1;
    public static final int TYPE_LOCAL_LAST = 2;
    public static final int TYPE_LOCAL_ONLY = 3;

    public static final int TYPE_REMOTE_FIRST = 10;
    public static final int TYPE_REMOTE_CENTER = 11;
    public static final int TYPE_REMOTE_LAST = 12;
    public static final int TYPE_REMOTE_ONLY = 13;
    // Store a member variable for the contacts
    private ArrayList<Message> mMessages;
    // Store the context for easy access
    private Context mContext;
    private User mRemoteUser;
    private User mLocalUser;
    private boolean mIsConversationForClass;
    // Pass in the contact array into the constructor
    public MessagesInConversationAdapter(Context context, ArrayList<Message> messages, User remoteUser, User localUser, boolean isConversationForClass) {
        mMessages = messages;
        mContext = context;
        mRemoteUser = remoteUser;
        mLocalUser = localUser;
        mIsConversationForClass = isConversationForClass;
    }

    public void updateMessages(ArrayList<Message> messages) {
        mMessages = messages;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Return a new holder instance
        switch (viewType) {
            case TYPE_LOCAL_FIRST:
            case TYPE_LOCAL_CENTER:
            case TYPE_LOCAL_LAST:
            case TYPE_LOCAL_ONLY:
                View localMessageView = inflater.inflate(R.layout.message_local, parent, false);
                return new LocalMessagesInConversationViewHolder(localMessageView);
            case TYPE_REMOTE_FIRST:
            case TYPE_REMOTE_CENTER:
            case TYPE_REMOTE_LAST:
            case TYPE_REMOTE_ONLY:
                View remoteMessageView = inflater.inflate(R.layout.message_remote, parent, false);
                return new RemoteMessagesInConversationViewHolder(remoteMessageView);
            default:
                return null;
        }
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Message currentMessage = mMessages.get(position);
        LocalMessagesInConversationViewHolder localViewHolder;
        RemoteMessagesInConversationViewHolder remoteViewHolder;
        Resources res = getContext().getResources();
        switch (viewHolder.getItemViewType()) {
            case TYPE_LOCAL_FIRST:
                localViewHolder = (LocalMessagesInConversationViewHolder) viewHolder;
                localViewHolder.mMessageContent.setText(currentMessage.getmContent());
                localViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.local_message_first));
                break;
            case TYPE_LOCAL_CENTER:
                localViewHolder = (LocalMessagesInConversationViewHolder) viewHolder;
                localViewHolder.mMessageContent.setText(currentMessage.getmContent());
                localViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.local_message_center));
                break;
            case TYPE_LOCAL_LAST:
                localViewHolder = (LocalMessagesInConversationViewHolder) viewHolder;
                localViewHolder.mMessageContent.setText(currentMessage.getmContent());
                localViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.local_message_last));
                break;
            case TYPE_LOCAL_ONLY:
                localViewHolder = (LocalMessagesInConversationViewHolder) viewHolder;
                localViewHolder.mMessageContent.setText(currentMessage.getmContent());
                localViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.local_message_only));
                break;
            case TYPE_REMOTE_FIRST:
                remoteViewHolder = (RemoteMessagesInConversationViewHolder) viewHolder;
                remoteViewHolder.mMessageContent.setText(currentMessage.getmContent());
                remoteViewHolder.mMessageContent.setTextColor(ContextCompat.getColor(mContext, R.color.md_grey_900));
                remoteViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.remote_message_first));
                remoteViewHolder.mMessageAvatar.setVisibility(View.INVISIBLE);
                break;
            case TYPE_REMOTE_CENTER:
                remoteViewHolder = (RemoteMessagesInConversationViewHolder) viewHolder;
                remoteViewHolder.mMessageContent.setText(currentMessage.getmContent());
                remoteViewHolder.mMessageContent.setTextColor(ContextCompat.getColor(mContext, R.color.md_grey_900));
                remoteViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.remote_message_center));
                remoteViewHolder.mMessageAvatar.setVisibility(View.INVISIBLE);
                break;
            case TYPE_REMOTE_LAST:
                remoteViewHolder = (RemoteMessagesInConversationViewHolder) viewHolder;
                remoteViewHolder.mMessageContent.setText(currentMessage.getmContent());
                remoteViewHolder.mMessageContent.setTextColor(ContextCompat.getColor(mContext, R.color.md_grey_900));
                remoteViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.remote_message_last));
                if (!mIsConversationForClass) {
                    remoteViewHolder.mMessageAvatar.bind(mRemoteUser.getFirstName().substring(0, 1), mRemoteUser.getUserImage());
                } else {
                    remoteViewHolder.mMessageAvatar.bind(res.getDrawable(R.drawable.ic_group_black_24dp));
                }
                remoteViewHolder.mMessageAvatar.changeColorById(R.color.colorAccent);
                break;
            case TYPE_REMOTE_ONLY:
                remoteViewHolder = (RemoteMessagesInConversationViewHolder) viewHolder;
                remoteViewHolder.mMessageContent.setText(currentMessage.getmContent());
                remoteViewHolder.mMessageContent.setTextColor(ContextCompat.getColor(mContext, R.color.md_grey_900));
                remoteViewHolder.mMessageContent.setBackground(res.getDrawable(R.drawable.remote_message_only));
                if (!mIsConversationForClass) {
                    remoteViewHolder.mMessageAvatar.bind(mRemoteUser.getFirstName().substring(0, 1), mRemoteUser.getUserImage());
                } else {
                    remoteViewHolder.mMessageAvatar.bind(res.getDrawable(R.drawable.ic_group_black_24dp));
                }
                remoteViewHolder.mMessageAvatar.changeColorById(R.color.colorAccent);
                break;
            default:
                break;
        }
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        Message currentMessage = mMessages.get(position);
        Message messageBefore = null;
        Message messageAfter = null;
        if (position != 0) messageBefore = mMessages.get(position - 1);
        if (position != mMessages.size() - 1) messageAfter = mMessages.get(position + 1);

        if (currentMessage.getmAuthorId().equals(mLocalUser.getUserId())) {
            //L
            if (messageBefore == null) {
                //N
                if (messageAfter == null) {
                    //N
                    return TYPE_LOCAL_ONLY;
                } else if (!messageAfter.getmAuthorId().equals(mLocalUser.getUserId())) {
                    //R
                    return TYPE_LOCAL_ONLY;
                } else {
                    //L
                    return TYPE_LOCAL_LAST;
                }
            } else if (!messageBefore.getmAuthorId().equals(mLocalUser.getUserId())) {
                //R
                if (messageAfter == null) {
                    //N
                    return TYPE_LOCAL_ONLY;
                } else if (!messageAfter.getmAuthorId().equals(mLocalUser.getUserId())) {
                    //R
                    return TYPE_LOCAL_ONLY;
                } else {
                    //L
                    return TYPE_LOCAL_LAST;
                }
            } else {
                //L
                if (messageAfter == null) {
                    //N
                    return TYPE_LOCAL_FIRST;
                } else if (!messageAfter.getmAuthorId().equals(mLocalUser.getUserId())) {
                    //R
                    return TYPE_LOCAL_FIRST;
                } else {
                    //L
                    return TYPE_LOCAL_CENTER;
                }
            }
        } else {
            //R
            if (messageBefore == null) {
                //N
                if (messageAfter == null) {
                    //N
                    return TYPE_REMOTE_ONLY;
                } else if (!messageAfter.getmAuthorId().equals(mLocalUser.getUserId())) {
                    //R
                    return TYPE_REMOTE_LAST;
                } else {
                    //L
                    return TYPE_REMOTE_ONLY;
                }
            } else if (!messageBefore.getmAuthorId().equals(mLocalUser.getUserId())) {
                //R
                if (messageAfter == null) {
                    //N
                    return TYPE_REMOTE_FIRST;
                } else if (!messageAfter.getmAuthorId().equals(mLocalUser.getUserId())) {
                    //R
                    return TYPE_REMOTE_CENTER;
                } else {
                    //L
                    return TYPE_REMOTE_FIRST;
                }
            } else {
                //L
                if (messageAfter == null) {
                    //N
                    return TYPE_REMOTE_CENTER;
                } else if (!messageAfter.getmAuthorId().equals(mLocalUser.getUserId())) {
                    //R
                    return TYPE_REMOTE_LAST;
                } else {
                    //L
                    return TYPE_REMOTE_ONLY;
                }
            }
        }
    }

    public static class LocalMessagesInConversationViewHolder extends RecyclerView.ViewHolder {
        public TextView mMessageContent;

        public LocalMessagesInConversationViewHolder(View itemView) {
            super(itemView);

            mMessageContent = (TextView) itemView.findViewById(R.id.local_message_text);
        }
    }

    public static class RemoteMessagesInConversationViewHolder extends RecyclerView.ViewHolder {
        public AvatarView mMessageAvatar;
        public TextView mMessageContent;

        public RemoteMessagesInConversationViewHolder(View itemView) {
            super(itemView);

            mMessageAvatar = (AvatarView) itemView.findViewById(R.id.remote_message_avatar_view);
            mMessageContent = (TextView) itemView.findViewById(R.id.remote_message_text);
        }
    }
}
