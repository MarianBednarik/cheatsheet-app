package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.ClassBody;
import com.qwentum.cheatsheet2.object.ClassLocation;
import com.qwentum.cheatsheet2.object.ClassTeacher;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Post;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-04-22.
 */

public class ClassViewContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // Store a member variable for the contacts
    private ArrayList<Post> mPosts;
    private ArrayList<User> mMembers;
    private SparseArray<ClassTeacher> mTeachers;
    private SparseArray<ClassBody> mBodies;
    private SparseArray<ClassLocation> mLocations;
    // Store the context for easy access
    private Context mCtx;
    private CoreSingleton mCoreS;
    private Type mType;
    // Pass in the contact array into the constructor
    public ClassViewContentAdapter(Context context, Type type, int localClassId) {
        mCtx = context;
        mCoreS = CoreSingleton.getInstance(mCtx);
        mType = type;
        switch (mType) {
            case POSTS:
                mPosts = mCoreS.getmPosts();
                break;
            case MEMBERS:
                mMembers = mCoreS.getUserAccount().getSchoolClass(localClassId).getMembers();
                break;
            case SUBJECTS:
                mBodies = mCoreS.getmBodies();
                break;
            case TEACHERS:
                mTeachers = mCoreS.getmTeachers();
                break;
            case LOCATIONS:
                mLocations = mCoreS.getmLocations();
                break;
        }
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (mType) {
            case POSTS:
                return new ContentPostViewHolder(LayoutInflater.from(mCtx).inflate(R.layout.item_class_view_post, parent, false));
            case MEMBERS:
            case SUBJECTS:
            case TEACHERS:
            case LOCATIONS:
                return new ContentViewHolder(LayoutInflater.from(mCtx).inflate(R.layout.item, parent, false));
            default:
                return null;
        }
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Resources res = mCtx.getResources();
        // Set item views based on your views and data model
        switch (mType) {
            case POSTS:
                Post currentPost = mPosts.get(position);
                ContentPostViewHolder contentViewholder = (ContentPostViewHolder) viewHolder;
                contentViewholder.mAvatarView.bind(currentPost.getmUserName().substring(0, 1), currentPost.getmUserImage());
                contentViewholder.mAvatarView.changeColorById(R.color.colorAccent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    contentViewholder.mContent.setText(Html.fromHtml(currentPost.getmContent(), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    contentViewholder.mContent.setText(Html.fromHtml(currentPost.getmContent()));
                }
                contentViewholder.mHeaderTitle.setText(currentPost.getmName());
                contentViewholder.mHeaderDesc.setText(currentPost.getmUserName() + " " + currentPost.getmUserSurname() + " - " + currentPost.getmSubject());
                break;
            case MEMBERS:
                User currentMember = mMembers.get(position);
                ContentViewHolder contentMembersViewHolder = (ContentViewHolder) viewHolder;
                contentMembersViewHolder.mAvatarView.bind(currentMember.getFirstName().substring(0, 1), currentMember.getUserImage());
                contentMembersViewHolder.mAvatarView.changeColorById(R.color.colorAccent);
                contentMembersViewHolder.mHeaderTitle.setText(currentMember.getFirstName() + " " + currentMember.getSurname());
                if (!currentMember.getUsername().equals("null") && !currentMember.getUsername().equals("")) {
                    contentMembersViewHolder.mHeaderDesc.setText(currentMember.getUsername() + " - " + currentMember.getCheatPoints() + " Cheatpoints");
                } else {
                    contentMembersViewHolder.mHeaderDesc.setText(currentMember.getCheatPoints() + " Cheatpoints");
                }
                break;
            case SUBJECTS:
                ClassBody currentSubject = mBodies.get(mBodies.keyAt(position));
                ContentViewHolder contentSubjectsViewHolder = (ContentViewHolder) viewHolder;
                if (!currentSubject.getmIcon().equals("null") && !currentSubject.getmIcon().equals("")) {
                    //icon what do
                } else {
                    contentSubjectsViewHolder.mAvatarView.bind(currentSubject.getmName().substring(0, 1), "");
                }
                contentSubjectsViewHolder.mAvatarView.changeColorByHex(currentSubject.getmColor());
                contentSubjectsViewHolder.mHeaderTitle.setText(currentSubject.getmName());
                contentSubjectsViewHolder.mHeaderDesc.setText(currentSubject.getmAcronym());
                break;
            case TEACHERS:
                ClassTeacher currentTeacher = mTeachers.get(mTeachers.keyAt(position));
                ContentViewHolder contentTeachersViewHolder = (ContentViewHolder) viewHolder;
                contentTeachersViewHolder.mAvatarView.bind(currentTeacher.getmName().substring(0, 1), "");
                contentTeachersViewHolder.mAvatarView.changeColorById(R.color.colorAccent);
                contentTeachersViewHolder.mHeaderTitle.setText(currentTeacher.getmName() + " " + currentTeacher.getmSurname());
                if (!currentTeacher.getmDescription().equals("null") && !currentTeacher.getmDescription().equals("")) {
                    contentTeachersViewHolder.mHeaderDesc.setText(currentTeacher.getmDescription());
                } else {
                    //no description
                    contentTeachersViewHolder.mHeaderDesc.setVisibility(View.GONE);
                }
                break;
            case LOCATIONS:
                ClassLocation currentLocation = mLocations.get(mLocations.keyAt(position));
                ContentViewHolder contentLocationsViewHolder = (ContentViewHolder) viewHolder;
                //contentLocationsViewHolder.mAvatarView.bind(currentLocation.getmName().substring(0, 1));
	              contentLocationsViewHolder.mAvatarView.bind(res.getDrawable(R.drawable.ic_location_on_black_24dp));
                contentLocationsViewHolder.mAvatarView.changeColorById(R.color.colorAccent);
                contentLocationsViewHolder.mHeaderTitle.setText(currentLocation.getmName());
                if (!currentLocation.getmDescription().equals("null") && !currentLocation.getmDescription().equals("")) {
                    contentLocationsViewHolder.mHeaderDesc.setText(currentLocation.getmDescription());
                } else {
                    contentLocationsViewHolder.mHeaderDesc.setVisibility(View.GONE);
                }
                break;
        }
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        switch (mType) {
            case POSTS:
                return mPosts.size();
            case MEMBERS:
                return mMembers.size();
            case SUBJECTS:
                return mBodies.size();
            case TEACHERS:
                return mTeachers.size();
            case LOCATIONS:
                return mLocations.size();
            default:
                return 0;
        }
    }

    public enum Type {POSTS, MEMBERS, SUBJECTS, TEACHERS, LOCATIONS}

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public static class ContentPostViewHolder extends RecyclerView.ViewHolder {

        public AvatarView mAvatarView;
        public TextView mContent, mHeaderTitle, mHeaderDesc;
        public ImageView mLikeButton, mDislikeButton;

        public ContentPostViewHolder(View itemView) {
            super(itemView);
            View header = itemView.findViewById(R.id.class_view_post_item_header);
            mAvatarView = (AvatarView) header.findViewById(R.id.avatarView);
            mHeaderTitle = (TextView) header.findViewById(R.id.itemName);
            mHeaderDesc = (TextView) header.findViewById(R.id.itemDesc);
            mContent = (TextView) itemView.findViewById(R.id.class_view_post_content);
            mLikeButton = (ImageView) itemView.findViewById(R.id.class_view_post_like);
            mDislikeButton = (ImageView) itemView.findViewById(R.id.class_view_post_dislike);
        }
    }

    public static class ContentViewHolder extends RecyclerView.ViewHolder {

        public AvatarView mAvatarView;
        public TextView mHeaderTitle, mHeaderDesc;

        public ContentViewHolder(View itemView) {
            super(itemView);
            mAvatarView = (AvatarView) itemView.findViewById(R.id.avatarView);
            mHeaderTitle = (TextView) itemView.findViewById(R.id.itemName);
            mHeaderDesc = (TextView) itemView.findViewById(R.id.itemDesc);
        }
    }
}