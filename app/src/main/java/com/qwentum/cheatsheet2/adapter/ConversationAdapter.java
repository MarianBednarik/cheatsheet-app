package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.Conversation;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Marian on 2017-02-26.
 */

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ConversationViewHolder> {

    public static final int TYPE_CLASS = 0;
    public static final int TYPE_USER = 1;
    // Store a member variable for the contacts
    private ArrayList<Conversation> mConvos;
    // Store the context for easy access
    private Context mContext;
    private int mThisUserId;
    // Pass in the contact array into the constructor
    public ConversationAdapter(Context context, ArrayList<Conversation> convos, int thisUserId) {
        mConvos = convos;
        mContext = context;
        mThisUserId = thisUserId;
    }

    public void updateContent(ArrayList<Conversation> convos) {
        mConvos = convos;
        notifyDataSetChanged();
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public ConversationAdapter.ConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_with_time, parent, false);

        // Return a new holder instance
        ConversationViewHolder viewHolder = new ConversationViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(ConversationAdapter.ConversationViewHolder viewHolder, int position) {
        // Get the data model based on position
        Conversation convo = mConvos.get(position);

        // Set item views based on your views and data model
        //TODO VIEW BINDING
        if (viewHolder.getItemViewType() == TYPE_CLASS) {
            CoreSingleton coreS = CoreSingleton.getInstance(mContext);
            User account = coreS.getUserAccount();
            if (convo == null) {
                viewHolder.convoName.setText(account.getSchoolClass(coreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getClassName());
                viewHolder.convoDesc.setText("");
                viewHolder.convoTime.setText("");
            } else {
                viewHolder.convoName.setText(account.getSchoolClass(coreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getClassName());
                viewHolder.convoDesc.setText(convo.getmClassLastName() + ' ' + convo.getmClassLastSurname() + ": " + convo.getmContent());
                viewHolder.convoTime.setText(getTimeString(convo.getmLastMessageTime()));
            }
            viewHolder.convoAvatar.bind(mContext.getResources().getDrawable(R.drawable.ic_group_black_24dp));
            viewHolder.convoAvatar.changeColorById(R.color.colorAccent);
        } else {
            if (convo.getmFirstUserId().equals(String.valueOf(mThisUserId))) {
                viewHolder.convoName.setText(convo.getmSecondName() + ' ' + convo.getmSecondSurname());
                viewHolder.convoDesc.setText(convo.getmContent());
                viewHolder.convoTime.setText(getTimeString(convo.getmLastMessageTime()));
                viewHolder.convoAvatar.bind(convo.getmSecondName().substring(0, 1), convo.getmSecondImage());
                viewHolder.convoAvatar.changeColorById(R.color.colorAccent);
            } else {
                viewHolder.convoName.setText(convo.getmFirstName() + ' ' + convo.getmFirstSurname());
                viewHolder.convoDesc.setText(convo.getmContent());
                viewHolder.convoTime.setText(getTimeString(convo.getmLastMessageTime()));
                viewHolder.convoAvatar.bind(convo.getmFirstName().substring(0, 1), convo.getmFirstImage());
                viewHolder.convoAvatar.changeColorById(R.color.colorAccent);
            }
        }
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mConvos.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_CLASS;
        } else {
            return TYPE_USER;
        }
    }

    private String getTimeString(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            Calendar cal = Calendar.getInstance();
            Date date1 = cal.getTime();
            Date date2 = sdf.parse(date);
            long diff = date1.getTime() - date2.getTime();
            int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            if (days < 7) {
                SimpleDateFormat dateSdf = new SimpleDateFormat("EEE", Locale.US);
                return dateSdf.format(date2);
            } else {
                SimpleDateFormat dateSdf = new SimpleDateFormat("MMM d", Locale.US);
                return dateSdf.format(date2);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public static class ConversationViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public AvatarView convoAvatar;
        public TextView convoName;
        public TextView convoDesc;
        public TextView convoTime;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ConversationViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            convoAvatar = (AvatarView) itemView.findViewById(R.id.avatarView);
            convoName = (TextView) itemView.findViewById(R.id.itemName);
            convoDesc = (TextView) itemView.findViewById(R.id.itemDesc);
            convoTime = (TextView) itemView.findViewById(R.id.itemTime);
        }
    }
}
