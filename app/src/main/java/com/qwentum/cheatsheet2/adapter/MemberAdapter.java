package com.qwentum.cheatsheet2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.view.AvatarView;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-25.
 */

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {

    // Store a member variable for the contacts
    private ArrayList<User> mMembers;
    // Store the context for easy access
    private Context mContext;
    // Pass in the contact array into the constructor
    public MemberAdapter(Context context, ArrayList<User> members) {
        mMembers = members;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public MemberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.member, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(MemberAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        User member = mMembers.get(position);

        // Set item views based on your views and data model
        viewHolder.memberName.setText(member.getFirstName() + ' ' + member.getSurname());
        viewHolder.memberDesc.setText(member.getUsername());
        viewHolder.memberAvatar.bind(member.getFirstName().substring(0, 1) + member.getSurname().substring(0, 1), member.getUserImage());
        viewHolder.memberAvatar.changeColorById(R.color.colorAccent);
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mMembers.size();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public AvatarView memberAvatar;
        public TextView memberName;
        public TextView memberDesc;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            memberAvatar = (AvatarView) itemView.findViewById(R.id.member_avatar_view);
            memberName = (TextView) itemView.findViewById(R.id.member_name);
            memberDesc = (TextView) itemView.findViewById(R.id.member_desc);
        }
    }

}
