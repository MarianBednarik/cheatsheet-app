package com.qwentum.cheatsheet2.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.activity.MessagesActivity;
import com.qwentum.cheatsheet2.adapter.ConversationAdapter;
import com.qwentum.cheatsheet2.adapter.MemberAdapter;
import com.qwentum.cheatsheet2.object.Conversation;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.SchoolClass;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.other.DividerItemDecoration;
import com.qwentum.cheatsheet2.other.ItemClickSupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ConversationsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConversationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConversationsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_CLASSID = "classId";

    // TODO: Rename and change types of parameters
    private int mClassId;

    private OnFragmentInteractionListener mListener;
    private Context mCtx;
    private CoreSingleton mCoreS;
    private User mUserAccout;
    private SchoolClass mSelectedClass;
    private ArrayList<Conversation> mConversations = new ArrayList<>();
    private RecyclerView messagesConvos;
    private ConversationAdapter adapter;
    private OkHttpClient mNetworkClient;

    public ConversationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ConversationsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConversationsFragment newInstance(int classId) {
        ConversationsFragment fragment = new ConversationsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CLASSID, classId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mClassId = getArguments().getInt(ARG_CLASSID);
            mCtx = getActivity().getApplicationContext();
            mCoreS = CoreSingleton.getInstance(mCtx);
            mNetworkClient = mCoreS.getNetworkClient();
            mUserAccout = mCoreS.getUserAccount();
            if (mUserAccout != null) {
                mSelectedClass = mUserAccout.getSchoolClass(mClassId);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getClassMessageOk();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_messages, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView messagesMembers = (RecyclerView) getView().findViewById(R.id.messages_member_recycler_view);
        messagesMembers.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.HORIZONTAL, false));
        messagesMembers.setHasFixedSize(true);
        messagesMembers.setAdapter(new MemberAdapter(mCtx, mSelectedClass.getMembers()));

        messagesConvos = (RecyclerView) getView().findViewById(R.id.messages_conversations_recycler_view);
        messagesConvos.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.VERTICAL, false));
        messagesConvos.setHasFixedSize(true);
        messagesConvos.addItemDecoration(new DividerItemDecoration(mCtx.getResources().getDrawable(R.drawable.line_divider)));
        adapter = new ConversationAdapter(mCtx, mConversations, Integer.parseInt(mUserAccout.getUserId()));
        messagesConvos.setAdapter(adapter);

        ItemClickSupport.addTo(messagesMembers).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Log.d(getClass().getName(), "Member localID: " + position + " classLocalID: " + mClassId);
                        Intent intent = new Intent(getActivity(), MessagesActivity.class);
                        intent.putExtra("userLocalId", position);
                        intent.putExtra("classLocalId", mClassId);
                        startActivity(intent);
                        //Toast.makeText(mCtx,"Clicked on " + chosenUser.getFirstName() + " " + chosenUser.getSurname(),Toast.LENGTH_SHORT).show();
                    }
                }
        );

        ItemClickSupport.addTo(messagesConvos).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Intent intent = new Intent(getActivity(), MessagesActivity.class);
                        if (position != 0) {
                            Conversation selectedConvo = mConversations.get(position);
                            ArrayList<User> users = mSelectedClass.getMembers();
                            String userID = selectedConvo.getmFirstUserId();
                            if (userID.equals(mUserAccout.getUserId()))
                                userID = selectedConvo.getmSecondUserId();
                            for (int i = 0; i < users.size(); i++) {
                                if (userID.equals(users.get(i).getUserId()))
                                    intent.putExtra("userLocalId", i);
                            }
                            intent.putExtra("classLocalId", mClassId);
                        } else {
                            intent.putExtra("classLocalId", mClassId);
                        }
                        startActivity(intent);
                        //Toast.makeText(mCtx,"Clicked on " + chosenUser.getFirstName() + " " + chosenUser.getSurname(),Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    public void onMessagesGot() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.updateContent(mConversations);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getLastMessagesOk() {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mUserAccout.getGoogleAccount().getIdToken())
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getLastMessages.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(getView().findViewById(R.id.messages_convo_root_layout), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            //mConversations.clear();
                            JSONArray lastMessages = jsonResponse.getJSONObject("data").getJSONArray("lastMessages");
                            Log.d(getClass().getName(), "Generating " + lastMessages.length() + " conversations");
                            for (int i = 0; i < lastMessages.length(); i++) {
                                mConversations.add(new Conversation(lastMessages.getJSONObject(i)));
                            }
                            onMessagesGot();
                        } else {
                            Log.e("Messsages", jsonResponse.toString());
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();

                    }
                }
            }
        });
    }

    private void getClassMessageOk() {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mUserAccout.getGoogleAccount().getIdToken())
                .add("classId", mSelectedClass.getCLassId())
                .add("limit", "1")
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getMessages.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(getView().findViewById(R.id.messages_convo_root_layout), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    //getActivity().runOnUiThread(new Runnable() {
                    //    @Override
                    //    public void run() {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            JSONArray lastClassMessage = jsonResponse.getJSONObject("data").getJSONArray("messages");
                            mConversations.clear();
                            if (lastClassMessage.length() == 1) {
                                mConversations.add(new Conversation(lastClassMessage.getJSONObject(0)));
                            } else {
                                mConversations.add(null);
                            }
                            getLastMessagesOk();
                        } else {
                            Log.e("Messsages", jsonResponse.toString());
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    //    }
                    //});

                }
            }
        });
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
