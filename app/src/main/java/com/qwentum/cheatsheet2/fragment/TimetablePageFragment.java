package com.qwentum.cheatsheet2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.SchoolDay;
import com.qwentum.cheatsheet2.other.Helper;

import java.util.ArrayList;
import java.util.Calendar;

public class TimetablePageFragment extends Fragment {

    public TabLayout tabLayout;
    public ViewPager viewPager;
    public SparseArray<TimetableCardFragment> generatedFragments = new SparseArray<>();
    private Context mCtx;
    private CoreSingleton mCoreS;
    private ArrayList<SchoolDay> mTimetable;
    private int mNullDays = 0;
    private String TAG = "TTPageFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.fragment_timetable_page, container, false);
        mCtx = getActivity().getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mCtx);
        mTimetable = mCoreS.getTimetable();

        tabLayout = (TabLayout) inflatedView.findViewById(R.id.sliding_tabs);
        for (int i = 0; i < mTimetable.size(); i++) {
            SchoolDay day = mTimetable.get(i);
            if (day != null) {
                switch (i) {
                    case 0:
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_tab_sunday));
                        break;
                    case 1:
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_tab_monday));
                        break;
                    case 2:
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_tab_tuesday));
                        break;
                    case 3:
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_tab_wednesday));
                        break;
                    case 4:
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_tab_thursday));
                        break;
                    case 5:
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_tab_friday));
                        break;
                    case 6:
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_tab_saturday));
                        break;
                }
            } else {
                mNullDays++;
            }
        }
        viewPager = (ViewPager) inflatedView.findViewById(R.id.viewpager);
        viewPager.setAdapter(new TimetablePagerAdapter(getChildFragmentManager()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //TODO make this more modular for other instances, not only ours
        if (!CoreSingleton.isWeekend()) {
            viewPager.setCurrentItem(Helper.calendarGet(Calendar.DAY_OF_WEEK) - 1);
        } else {
            viewPager.setCurrentItem(0);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return inflatedView;
    }

    public class TimetablePagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = mTimetable.size() - mNullDays;

        public TimetablePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            return TimetableCardFragment.newInstance(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TimetableCardFragment fragment = (TimetableCardFragment) super.instantiateItem(container, position);
            generatedFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            generatedFragments.remove(position);
        }
    }
}