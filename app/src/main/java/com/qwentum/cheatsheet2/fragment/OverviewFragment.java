package com.qwentum.cheatsheet2.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.activity.PostActivity;
import com.qwentum.cheatsheet2.adapter.ActivityAdapter;
import com.qwentum.cheatsheet2.adapter.NextLessonAdapter;
import com.qwentum.cheatsheet2.adapter.ReminderAdapter;
import com.qwentum.cheatsheet2.object.ClassActivity;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.OverviewLesson;
import com.qwentum.cheatsheet2.object.Reminder;
import com.qwentum.cheatsheet2.object.SchoolClass;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.other.DividerItemDecoration;
import com.qwentum.cheatsheet2.other.ItemClickSupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OverviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OverviewFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "lessonsToDisplay";
    private static final String ARG_PARAM2 = "isCurrent";

    private CoreSingleton mCoreS;
    //private int mSubjectFromToday, mNextSubjectFromToday;
    private boolean mIsCurrent;
    private User mUser;
    //private SchoolClass mCurrentClass;
    private Context mCtx;
    private OkHttpClient mNetworkClient;
    private ArrayList<ClassActivity> mActivities = new ArrayList<>();
    private ArrayList<Reminder> mReminders = new ArrayList<>();
    private ArrayList<OverviewLesson> mLessons = new ArrayList<>();
    private ActivityAdapter mActivityAdapter;
    private ReminderAdapter mReminderAdapter;
    private NextLessonAdapter mLessonAdapter;
    private Button loadMoreButton;
    private View remindersView;
    private View activityView;
    private View mRootLayout;
    private int numberOfHeaders = 0;

    private OnFragmentInteractionListener mListener;

    public OverviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment OverviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OverviewFragment newInstance(String lessonsToDisplay, boolean isCurrent) {
        OverviewFragment fragment = new OverviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, lessonsToDisplay);
        args.putBoolean(ARG_PARAM2, isCurrent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mCtx == null) {
            Log.d("OverviewFragment", "onCreate()");
            mCtx = getActivity().getApplicationContext();
            mCoreS = CoreSingleton.getInstance(mCtx);
            mNetworkClient = mCoreS.getNetworkClient();
            mActivityAdapter = new ActivityAdapter(mActivities, mCtx);
            mReminderAdapter = new ReminderAdapter(new ArrayList<Reminder>(), false, mCtx);
            mLessonAdapter = new NextLessonAdapter(new ArrayList<OverviewLesson>(), true, mCtx);
            //mCoreS.setRemindersListener(this);
            mUser = mCoreS.getUserAccount();
            //mCurrentClass = mUser.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0));
            String mLessonsToShowJson = null;
            if (getArguments() != null) {
                mLessonsToShowJson = getArguments().getString(ARG_PARAM1);
                mIsCurrent = getArguments().getBoolean(ARG_PARAM2);
            }
            try {
                JSONObject lessons = new JSONObject(mLessonsToShowJson);
                mLessons.clear();
                mLessons.add(new OverviewLesson(lessons.getJSONObject("firstSubject")));
                mLessons.add(new OverviewLesson(lessons.getJSONObject("secondSubject")));
                //mSubjectFromToday = lessons.getInt("subjectFromToday");
                //mNextSubjectFromToday = lessons.getInt("nextSubjectFromToday");
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_overview, container, false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_load_more:
                loadMoreActivities(mActivities.size());
                break;
        }
    }

    public void updateLessons(final ArrayList<OverviewLesson> lessons, final boolean isCurrent) {
        Log.d("Overview", "Updating lesson view");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLessons = lessons;
                mIsCurrent = isCurrent;
                //lessonAdapter.updateContent(null, false);
                mLessonAdapter.updateContent(lessons, isCurrent);
            }
        });
    }

    public void updateActivity(final ArrayList<ClassActivity> activities, final int start, final int count) {
        if (activities.size() > 0) activityView.setVisibility(View.VISIBLE);
        if (count < 10) {
            loadMoreButton.setVisibility(View.GONE);
        } else {
            loadMoreButton.setVisibility(View.VISIBLE);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mActivityAdapter.updateContent(activities, start, count);
            }
        });
    }

    public void updateAll() {
        mReminderAdapter.updateContent(null);
        mActivityAdapter.updateContent(null, 0, 0);
        mLessonAdapter.updateContent(null, false);
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mUser.getGoogleAccount().getIdToken())
                .add("classId", mUser.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getCLassId())
                .add("dateNow", Calendar.getInstance().getTime().toString())
                .add("numberOfReminders", "20")
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getClassData.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(mRootLayout, "getClassData : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        final JSONObject jsonResponse = new JSONObject(response.body().string());
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonData = jsonResponse.getJSONObject("data");
                                    if (jsonData.has("reminders")) {
                                        try {
                                            mReminders.clear();
                                            for (int i = 0; i < jsonData.getJSONArray("reminders").length(); i++) {
                                                mReminders.add(new Reminder(jsonData.getJSONArray("reminders").getJSONObject(i)));
                                            }
                                        } catch (JSONException ex) {
                                            ex.printStackTrace();
                                        }
                                        remindersView.setVisibility(View.VISIBLE);
                                    } else {
                                        remindersView.setVisibility(View.GONE);
                                    }
                                    final JSONObject timetable = jsonData.getJSONObject("timetableData");
                                    mLessons.clear();
                                    mLessons.add(new OverviewLesson(timetable.getJSONObject("firstSubject")));
                                    mLessons.add(new OverviewLesson(timetable.getJSONObject("secondSubject")));
                                    mIsCurrent = timetable.getBoolean("isCurrent");
                                    //lessonAdapter.updateContent(mUpdatedLesson, mIsCurrent);
                                    //try {
                                    getActivityOk("[\"" + mUser.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getCLassId() + "\"]", "0", false);
                                    /*if (timetable.getInt("subjectFromToday") == 0) {
                                        JSONObject subjectToTime = timetable.getJSONObject("subject");
                                        if (timetable.getBoolean("isCurrent")) {
                                            setUpTimer(subjectToTime.getString("end"));
                                        } else {
                                            setUpTimer(subjectToTime.getString("start"));
                                        }
                                    }
                                } catch (JSONException ex){
                                    ex.printStackTrace();
                                }*/
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRootLayout = getView().findViewById(R.id.fragment_overview_root);
        updateAll();
        setUpViews();
    }

    private void setUpViews() {
        final Resources res = mCtx.getResources();
        final RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(res.getDrawable(R.drawable.line_divider));
        View currentLessonView = getView().findViewById(R.id.overview_lessons);
        loadMoreButton = (Button) getView().findViewById(R.id.button_load_more);
        loadMoreButton.setOnClickListener(this);
        loadMoreButton.setVisibility(View.GONE);

        //Current lesson
        /*if (mSubjectFromToday > 1){
            currentLessonView.setVisibility(View.GONE);
        } else {*/
        TextView lessonStatus = (TextView) currentLessonView.findViewById(R.id.overview_reminders_text);
        lessonStatus.setText("Lessons");
        RecyclerView lessonBodiesRecyclerView = (RecyclerView) currentLessonView.findViewById(R.id.days_reminder_recycler_view);
        lessonBodiesRecyclerView.setAdapter(mLessonAdapter);
        mLessonAdapter.updateContent(mLessons, mIsCurrent);
        lessonBodiesRecyclerView.setHasFixedSize(false);
        lessonBodiesRecyclerView.setNestedScrollingEnabled(false);
        lessonBodiesRecyclerView.addItemDecoration(itemDecoration);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mCtx, LinearLayoutManager.VERTICAL, false);
        lessonBodiesRecyclerView.setLayoutManager(layoutManager);
        //}

        //Reminders
        remindersView = getView().findViewById(R.id.overview_reminders);
        //if (reminders.size() > 0) {
        remindersView.setVisibility(View.VISIBLE);
        RecyclerView reminderRecyclerView = (RecyclerView) remindersView.findViewById(R.id.days_reminder_recycler_view);
        reminderRecyclerView.setAdapter(mReminderAdapter);
        reminderRecyclerView.setHasFixedSize(false);
        reminderRecyclerView.setNestedScrollingEnabled(false);
        reminderRecyclerView.addItemDecoration(itemDecoration);
        LinearLayoutManager reminderLayoutManager = new LinearLayoutManager(mCtx, LinearLayoutManager.VERTICAL, false);
        reminderRecyclerView.setLayoutManager(reminderLayoutManager);
        ItemClickSupport.addTo(reminderRecyclerView).setOnItemLongClickListener(
                new ItemClickSupport.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClicked(RecyclerView recyclerView, final int position, View v) {
                        RecyclerView.Adapter adapter = recyclerView.getAdapter();
                        //ReminderAdapter.ReminderItemViewHolder test = (ReminderAdapter.ReminderItemViewHolder) recyclerView.getChildViewHolder(v);
                        if (adapter.getItemViewType(position) != 100) {
                            numberOfHeaders = 0;
                            for (int i = position - 1; i >= 0; i--) {
                                if (adapter.getItemViewType(i) == 100) numberOfHeaders++;
                            }
                            Vibrator vibrator = (Vibrator) mCtx.getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(50);
                            //Toast.makeText(mCtx, mUser.getReminder(position-numberOfHeaders).getDateOfReminder(), Toast.LENGTH_SHORT).show();
                            new MaterialDialog.Builder(getActivity())
                                    .title("Remove reminder?")
                                    .content("Are you sure you want to delete this reminder?")
                                    .positiveText("YES")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            removeReminderOk(mReminders.get(position - numberOfHeaders).getReminderId());
                                        }
                                    })
                                    .negativeText("NO")
                                    .show();
                        }
                        return false;
                    }
                }
        );
        //} else {
        //    remindersView.setVisibility(View.GONE);
        //}

        //Feed
        activityView = getView().findViewById(R.id.overview_activity);
        RecyclerView activityRecyclerView = (RecyclerView) activityView.findViewById(R.id.overview_activity_recycler);
        activityRecyclerView.setHasFixedSize(false);
        activityRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager feedManager = new LinearLayoutManager(mCtx, LinearLayoutManager.VERTICAL, false);
        activityRecyclerView.setLayoutManager(feedManager);
        activityRecyclerView.addItemDecoration(new DividerItemDecoration(res.getDrawable(R.drawable.line_divider)));
        activityRecyclerView.setAdapter(mActivityAdapter);
        ItemClickSupport.addTo(activityRecyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                        RecyclerView.Adapter adapter = recyclerView.getAdapter();
                        //Toast.makeText(mCtx, mCurrentClass.getmActivity(position).getmActivityType(), Toast.LENGTH_SHORT).show();
                        if (adapter.getItemViewType(position) == 1) {
                            Intent intent = new Intent(getActivity(), PostActivity.class);
                            intent.putExtra("postId", position);
                            intent.putExtra("isFromActivity", true);
                            intent.putExtra("currentClass", mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0));
                            startActivity(intent);
                        }
                        //ReminderAdapter.ReminderItemViewHolder test = (ReminderAdapter.ReminderItemViewHolder) recyclerView.getChildViewHolder(v);
                    }
                }
        );
    }

    private void loadMoreActivities(int offset) {
        getActivityOk("[\"" + mUser.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getCLassId() + "\"]", String.valueOf(offset), true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void removeReminderOk(String reminderId) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mUser.getGoogleAccount().getIdToken())
                .add("reminderId", reminderId)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/remove/removeReminder.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(mRootLayout, "getClassData : " + response.code() + " " + response.message(), Snackbar.LENGTH_LONG).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            Snackbar.make(mRootLayout, "Successfully removed reminder", Snackbar.LENGTH_SHORT).show();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateAll();
                                }
                            });
                        } else {
                            Snackbar.make(mRootLayout, "Couldn't remove reminder", Snackbar.LENGTH_SHORT).show();
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    public void getActivityOk(final String classIdArray, final String offset, final boolean update) {

        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mUser.getGoogleAccount().getIdToken())
                .add("filter", classIdArray)
                .add("offset", offset)
                .add("limit", "10")
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getActivity.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(mRootLayout, "getClassData : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.getJSONObject("data");
                        final SchoolClass selectedClass = mUser.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0));
                        JSONArray activites = (jsonData.has("activity")) ? jsonData.getJSONArray("activity") : null;
                        for (int i = 0; i < activites.length(); i++) {
                            mActivities.add(new ClassActivity(activites.getJSONObject(i)));
                        }
                        selectedClass.setmActivity(mActivities);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateActivity(mActivities, Integer.parseInt(offset), mActivities.size() - Integer.parseInt(offset));
                                if (!update) {
                                    if (mReminders.size() > 0) {
                                        remindersView.setVisibility(View.VISIBLE);
                                        mReminderAdapter.updateContent(mReminders);
                                    } else {
                                        mReminderAdapter.updateContent(mReminders);
                                        remindersView.setVisibility(View.GONE);
                                    }
                                    mLessonAdapter.updateContent(mLessons, mIsCurrent);
                                }
                            }
                        });
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
