package com.qwentum.cheatsheet2.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.activity.PostActivity;
import com.qwentum.cheatsheet2.adapter.ClassViewContentAdapter;
import com.qwentum.cheatsheet2.object.ClassBody;
import com.qwentum.cheatsheet2.object.ClassLocation;
import com.qwentum.cheatsheet2.object.ClassTeacher;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.other.DividerItemDecoration;
import com.qwentum.cheatsheet2.other.ItemClickSupport;
import com.qwentum.cheatsheet2.view.AvatarView;

/**
 * Created by Marian on 2017-04-19.
 */

public class ClassViewContent extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String ARG_CLASS_ID = "ARG_CLASS_ID";

    private int mPage, mLocalClassId;
    private Context mCtx;
    private CoreSingleton mCoreS;
    private RecyclerView mContentRecyclerView;

    public static ClassViewContent newInstance(int page, int classId) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putInt(ARG_CLASS_ID, classId);
        ClassViewContent fragment = new ClassViewContent();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        mLocalClassId = getArguments().getInt(ARG_CLASS_ID);
        mCtx = getActivity().getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mCtx);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_class_view_content, container, false);
        if (mPage == 1)
            view = inflater.inflate(R.layout.fragment_class_view_posts, container, false);

        mContentRecyclerView = (RecyclerView) view.findViewById(R.id.content_recycler_view);

        ClassViewContentAdapter adapter;
        switch (mPage) {
            case 1:
                //POSTS
                adapter = new ClassViewContentAdapter(mCtx, ClassViewContentAdapter.Type.POSTS, mLocalClassId);
                break;
            case 2:
                //MEMBERS
                adapter = new ClassViewContentAdapter(mCtx, ClassViewContentAdapter.Type.MEMBERS, mLocalClassId);
                break;
            case 3:
                //SUBJECTS
                adapter = new ClassViewContentAdapter(mCtx, ClassViewContentAdapter.Type.SUBJECTS, mLocalClassId);
                break;
            case 4:
                //TEACHERS
                adapter = new ClassViewContentAdapter(mCtx, ClassViewContentAdapter.Type.TEACHERS, mLocalClassId);
                break;
            case 5:
                //LOCATIONS
                adapter = new ClassViewContentAdapter(mCtx, ClassViewContentAdapter.Type.LOCATIONS, mLocalClassId);
                break;
            default:
                adapter = null;
                break;
        }

        final Resources res = mCtx.getResources();
        final RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(res.getDrawable(R.drawable.line_divider));

        mContentRecyclerView.setAdapter(adapter);
        mContentRecyclerView.setLayoutManager(new LinearLayoutManager(mCtx));
        mContentRecyclerView.setHasFixedSize(false);
        mContentRecyclerView.setNestedScrollingEnabled(false);
        mContentRecyclerView.addItemDecoration(itemDecoration);
        ItemClickSupport.addTo(mContentRecyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                        switch (mPage) {
                            case 1:
                                //POSTS
                                Intent postIntent = new Intent(getActivity(), PostActivity.class);
                                postIntent.putExtra("postId", position);
                                postIntent.putExtra("isFromActivity", false);
                                postIntent.putExtra("currentClass", mLocalClassId);
                                startActivity(postIntent);
                                break;
                            case 2:
                                //MEMBERS
                                mCoreS.showUserProfile(getContext(), position);
                                break;
                            case 3:
                                //SUBJECTS
                                EditText subjectName, subjectAcronym;
                                ImageView colorImage, subjectHeaderImage;
                                AvatarView subjectAvatar;
                                TextView headerTitle, headerDesc;

                                ClassBody selectedBody = mCoreS.getmBodies().get(mCoreS.getmBodies().keyAt(position));
                                MaterialDialog subjectDialog = new MaterialDialog.Builder(getContext())
                                        .customView(R.layout.subject_view_layout, false)
                                        .positiveText("save")
                                        .neutralText("cancel")
                                        .build();

                                View dialogView = subjectDialog.getCustomView();
                                subjectName = (EditText) dialogView.findViewById(R.id.subject_view_name_edit_text);
                                subjectAcronym = (EditText) dialogView.findViewById(R.id.subject_view_acronym_edit_text);
                                colorImage = (ImageView) dialogView.findViewById(R.id.subject_view_color);
                                subjectHeaderImage = (ImageView) dialogView.findViewById(R.id.subject_view_header_background);
                                View headerPreview = dialogView.findViewById(R.id.subject_view_item_view);
                                subjectAvatar = (AvatarView) headerPreview.findViewById(R.id.avatarView);
                                headerTitle = (TextView) headerPreview.findViewById(R.id.itemName);
                                headerDesc = (TextView) headerPreview.findViewById(R.id.itemDesc);


                                subjectName.setText(selectedBody.getmName());
                                subjectAcronym.setText(selectedBody.getmAcronym());
                                colorImage.setColorFilter(Color.parseColor(selectedBody.getmColor()));
                                subjectHeaderImage.setBackgroundColor(Color.parseColor(selectedBody.getmColor()));
                                subjectAvatar.bind(selectedBody.getmName().substring(0,1), "");
                                subjectAvatar.changeColorByHex(selectedBody.getmColor());
                                headerTitle.setText(selectedBody.getmName());
                                headerDesc.setText(selectedBody.getmAcronym());

                                subjectDialog.show();
                                break;
                            case 4:
                                //TEACHERS
                                ClassTeacher selectedTeacher = mCoreS.getmTeachers().get(mCoreS.getmTeachers().keyAt(position));

                                EditText teacherName, teacherSurname, teacherDescription;
                                AvatarView teacherAvatar;
                                TextView teacherHeaderTitle, teacherHeaderDesc;

                                MaterialDialog teacherDialog = new MaterialDialog.Builder(getContext())
                                        .customView(R.layout.teacher_view_layout, false)
                                        .positiveText("save")
                                        .neutralText("cancel")
                                        .show();

                                View teacherDialogView = teacherDialog.getCustomView();
                                teacherName = (EditText) teacherDialogView.findViewById(R.id.teacher_view_name_edit_text);
                                teacherSurname = (EditText) teacherDialogView.findViewById(R.id.teacher_view_surname_edit_text);
                                teacherDescription = (EditText) teacherDialogView.findViewById(R.id.teacher_view_desc_edit_text);
                                View teacherHeaderPreview = teacherDialogView.findViewById(R.id.teacher_view_item_view);
                                teacherAvatar = (AvatarView) teacherHeaderPreview.findViewById(R.id.avatarView);
                                teacherHeaderTitle = (TextView) teacherHeaderPreview.findViewById(R.id.itemName);
                                teacherHeaderDesc = (TextView) teacherHeaderPreview.findViewById(R.id.itemDesc);

                                teacherName.setText(selectedTeacher.getmName());
                                teacherHeaderTitle.setText(selectedTeacher.getmName() + " " + selectedTeacher.getmSurname());
                                teacherSurname.setText(selectedTeacher.getmSurname());
                                if (!selectedTeacher.getmDescription().equals("null") && !selectedTeacher.getmDescription().equals("")) {
                                    teacherDescription.setText(selectedTeacher.getmDescription());
                                    teacherHeaderDesc.setText(selectedTeacher.getmDescription());
                                } else {
                                    teacherDescription.setText("");
                                    teacherHeaderDesc.setText("No description");
                                }
                                teacherAvatar.bind(selectedTeacher.getmName().substring(0,1), "");
                                break;
                            case 5:
                                //LOCATIONS
                                ClassLocation selectedLocation = mCoreS.getmLocations().get(mCoreS.getmLocations().keyAt(position));

                                EditText locationName, locationDescription;
                                AvatarView locationAvatar;
                                TextView locationHeaderTitle, locationHeaderDesc;

                                MaterialDialog locationDialog = new MaterialDialog.Builder(getContext())
                                        .customView(R.layout.location_view_layout, false)
                                        .positiveText("save")
                                        .neutralText("cancel")
                                        .show();

                                View locationDialogView = locationDialog.getCustomView();
                                locationName = (EditText) locationDialogView.findViewById(R.id.location_view_name_edit_text);
                                locationDescription = (EditText) locationDialogView.findViewById(R.id.location_view_desc_edit_text);
                                View locationHeaderPreview = locationDialogView.findViewById(R.id.location_view_item_view);
                                locationAvatar = (AvatarView) locationHeaderPreview.findViewById(R.id.avatarView);
                                locationHeaderTitle = (TextView) locationHeaderPreview.findViewById(R.id.itemName);
                                locationHeaderDesc = (TextView) locationHeaderPreview.findViewById(R.id.itemDesc);

                                locationName.setText(selectedLocation.getmName());
                                locationHeaderTitle.setText(selectedLocation.getmName());
                                if (!selectedLocation.getmDescription().equals("null") && !selectedLocation.getmDescription().equals("")) {
                                    locationDescription.setText(selectedLocation.getmDescription());
                                    locationHeaderDesc.setText(selectedLocation.getmDescription());
                                } else {
                                    locationDescription.setText("");
                                    locationHeaderDesc.setText("No description");
                                }
                                locationAvatar.bind(res.getDrawable(R.drawable.ic_location_on_black_24dp));

                                break;
                            default:
                                //DEFAULT
                                break;
                        }
                    }
                }
        );
        //textView.setText("Fragment #" + mPage);
        //Toast.makeText(getActivity().getApplicationContext(),"fragment " + mPage, Toast.LENGTH_LONG).show();
        return view;
    }
}
