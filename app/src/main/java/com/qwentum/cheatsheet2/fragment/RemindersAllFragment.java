package com.qwentum.cheatsheet2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.adapter.ReminderAdapter;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Reminder;
import com.qwentum.cheatsheet2.other.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class RemindersAllFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    private Context mCtx;
    private CoreSingleton mCoreS;
    private ArrayList<Reminder> mReminders = new ArrayList<>();
    private ReminderAdapter reminderAllAdapter;

    public RemindersAllFragment() {
        // Required empty public constructor
    }

    public static RemindersAllFragment newInstance() {
        RemindersAllFragment fragment = new RemindersAllFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = getActivity().getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mCtx);
        reminderAllAdapter = new ReminderAdapter(new ArrayList<Reminder>(), true, mCtx);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reminders_all, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView remindersAllRecyclerView = (RecyclerView) getView().findViewById(R.id.reminders_all_recycler_view);
        remindersAllRecyclerView.setAdapter(reminderAllAdapter);
        remindersAllRecyclerView.setLayoutManager(new LinearLayoutManager(mCtx));
        remindersAllRecyclerView.setHasFixedSize(true);
        remindersAllRecyclerView.addItemDecoration(new DividerItemDecoration(mCtx.getResources().getDrawable(R.drawable.line_divider)));
        getRemindersOk();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void getRemindersOk() {

        OkHttpClient mNetworkClient = mCoreS.getNetworkClient();
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mCoreS.getUserAccount().getGoogleAccount().getIdToken())
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getReminders.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(getView().findViewById(R.id.reminders_all_root_view), "getReminders : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject responseJson = new JSONObject(response.body().string());
                        if (responseJson.getBoolean("success")) {
                            JSONArray remindersArray = responseJson.getJSONObject("data").getJSONArray("reminders");
                            if (mReminders.size() != remindersArray.length()) {
                                mReminders.clear();
                                for (int i = 0; i < remindersArray.length(); i++) {
                                    mReminders.add(new Reminder(remindersArray.getJSONObject(i)));
                                }
                            }
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    reminderAllAdapter.updateContent(mReminders);
                                    reminderAllAdapter.notifyDataSetChanged();
                                }
                            });

                        } else {
                            Snackbar.make(getView().findViewById(R.id.reminders_all_root_view), "Could not get reminders", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
}
