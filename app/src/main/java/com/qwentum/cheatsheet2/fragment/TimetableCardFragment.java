package com.qwentum.cheatsheet2.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qwentum.cheatsheet2.ItemTouchHelperAdapter;
import com.qwentum.cheatsheet2.ItemTouchHelperViewHolder;
import com.qwentum.cheatsheet2.LessonViewActivity;
import com.qwentum.cheatsheet2.OnStartDragListener;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.SimpleItemTouchHelperCallback;
import com.qwentum.cheatsheet2.adapter.ChildLessonAdapter;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Lesson;
import com.qwentum.cheatsheet2.object.SchoolDay;
import com.qwentum.cheatsheet2.object.SimpleDividerItemDecoration;
import com.qwentum.cheatsheet2.other.ItemClickSupport;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Marian on 2016-02-29.
 */
public class TimetableCardFragment extends Fragment implements OnStartDragListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    private static final String TAG = "CardFragment";
    public RecyclerView mRecyclerView;
    private ItemTouchHelper mItemTouchHelper;
    private CoreSingleton mCore;
    private SharedPreferences SP;
    private SchoolDay currentPageTimetable;
    private LessonAdapter adapter;
    private Context mCtx;
    private Resources res;
    private ArrayList<Lesson> mLessons;

    public static TimetableCardFragment newInstance(int page) {
        //Log.d(TAG, "Generating Timetable with ID #" + (page) + "...");
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        TimetableCardFragment fragment = new TimetableCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mCtx = getActivity().getApplicationContext();
            mCore = CoreSingleton.getInstance(mCtx);
            SP = PreferenceManager.getDefaultSharedPreferences(mCtx);
            //+1 because there is no sunday, need to make this more modular
            currentPageTimetable = mCore.getTimetable(getArguments().getInt(ARG_PAGE, 0) + 1);
            res = mCtx.getResources();
        }
        super.onCreate(savedInstanceState);
    }

    /*@Override
    public void onStart() {
        if (!timetable.areLessonsDone(Helper.calendarGet(Calendar.DAY_OF_WEEK)) && !timetable.isWeekend())
            autoSmoothScrollTo(timetable.getCurrentSubjectID(true));
        super.onStart();
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        adapter = new LessonAdapter(currentPageTimetable, this);
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.recycler_view, container, false);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.VERTICAL, false));

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter, mCtx);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
        return mRecyclerView;
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    /*public void autoSmoothScrollTo(int position) {
        if ((Helper.calendarGet(Calendar.DAY_OF_WEEK) == getArguments().getInt(ARG_PAGE, 0)) && timetable.areLessonsDone(Helper.calendarGet(Calendar.DAY_OF_WEEK))) {
            if (!currentDay._startsWithZero && position > 0) position -= 1;
            mRecyclerView.smoothScrollToPosition(position);
        }
    }*/

    public LessonAdapter getAdapter() {
        return adapter;
    }

    public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> implements ItemTouchHelperAdapter {

        public LessonAdapter(SchoolDay currentDayTimetable, OnStartDragListener dragStartListener) {
            //mDragStartListener = dragStartListener;
            mLessons = currentDayTimetable.getLessons();
        }

        //public final ArrayList<Lesson> mLessons;
        //private final OnStartDragListener mDragStartListener;

        @Override
        public LessonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);

            // Inflate the custom layout
            View lessonView = inflater.inflate(R.layout.item_timetable_const, parent, false);

            // Return a new holder instance
            return new ViewHolder(lessonView);
        }

        @Override
        public void onBindViewHolder(final LessonAdapter.ViewHolder holder, int position) {
            //Call child RecyclerView
            Lesson lesson = mLessons.get(position);

            ChildLessonAdapter adapter = new ChildLessonAdapter(lesson, mCtx);
            holder.childRecyclerView.setAdapter(adapter);
            holder.childRecyclerView.setHasFixedSize(false);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mCtx, LinearLayoutManager.VERTICAL, false);
            holder.childRecyclerView.setLayoutManager(layoutManager);

            ItemClickSupport.addTo(holder.childRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                    if (mCore.isTimetableInEditMode()) {
                        Intent intent = new Intent(mCtx, LessonViewActivity.class);
                        intent.putExtra("lessonToEdit", Lesson.toJSON(mLessons.get(holder.getAdapterPosition())));
                        intent.putExtra("lessonToEditPosition", holder.getAdapterPosition());
                        intent.putExtra("lessonBodyToEdit", position);
                        getParentFragment().getActivity().startActivityForResult(intent, 20);
                        //Log.d(getClass().getName(), Lesson.toJSON(mLessons.get(holder.getAdapterPosition())));
                    }
                }
            });

            holder.mLessonNum.setText(res.getString(R.string.text_lesson_number, lesson.getmNumber()));
            holder.mTimes.setText(lesson.getmStart() + " - " + lesson.getmEnd());
            RecyclerView.ItemDecoration itemDecoration = new
                    SimpleDividerItemDecoration(mCtx, 4);
            holder.childRecyclerView.addItemDecoration(itemDecoration);

            /*holder.handleView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        mDragStartListener.onStartDrag(holder);
                    }
                    return false;
                }
            });*/
        }

        @Override
        public int getItemCount() {
            return mLessons.size();
        }

        @Override
        public void onItemDismiss(int position) {
            mLessons.remove(position);
            notifyItemRemoved(position);
        }

        @Override
        public boolean onItemMove(int fromPosition, int toPosition) {
            Collections.swap(mLessons, fromPosition, toPosition);
            notifyItemMoved(fromPosition, toPosition);
            return true;
        }

        public void onItemAdd(Lesson input) {
            mLessons.add(input);
            notifyItemRangeInserted(mLessons.size() - 1, 1);
        }

        public void onItemChange(Lesson input, int index) {
            mLessons.set(index, input);
            adapter.notifyItemChanged(index);
        }

        private void HandleLessonMove(int fromPosition, int toPosition) {
            Lesson originalLesson = mLessons.get(fromPosition);
            Lesson newLesson = mLessons.get(toPosition);
            originalLesson.setmBodies(newLesson.getmBodies());
            originalLesson.setmEnd(newLesson.getmEnd());
            originalLesson.setmStart(newLesson.getmStart());
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {

            //Public so they can be used when binding elsewhere, but can be private a binding method in here
            public TextView mLessonNum, mTimes;
            //public CardView mCardView;
            public RecyclerView childRecyclerView;

            public ViewHolder(View itemView) {
                super(itemView);
                //Find views
                mLessonNum = (TextView) itemView.findViewById(R.id.lessonNumber);
                mTimes = (TextView) itemView.findViewById(R.id.lessonTime);
                //mCardView = (CardView) itemView.findViewById(R.id.cardView);
                childRecyclerView = (RecyclerView) itemView.findViewById(R.id.lessonRecycler);
            }

            @Override
            public void onItemSelected() {
                itemView.setBackgroundColor(ContextCompat.getColor(mCtx, R.color.md_amber_200));
            }

            @Override
            public void onItemClear() {
                itemView.setBackgroundColor(ContextCompat.getColor(mCtx, R.color.md_grey_50));
            }
        }
    }
}
