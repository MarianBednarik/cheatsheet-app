package com.qwentum.cheatsheet2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.SignInButton;
import com.qwentum.cheatsheet2.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SignInFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignInFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "type";
    private int mType;
    private OnFragmentInteractionListener mListener;

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance(int param1) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mType == 0) {
            return inflater.inflate(R.layout.fragment_sign_in, container, false);
        } else {
            return inflater.inflate(R.layout.fragment_sign_up, container, false);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mType == 0) {
            //Google Sign in
            //getView().findViewById(R.id.button_sign_up).setOnClickListener(this);
            SignInButton mSignInButton = (SignInButton) getView().findViewById(R.id.button_google_sign_in);
            mSignInButton.setSize(SignInButton.SIZE_WIDE);
            mSignInButton.setOnClickListener(this);
        } else {
            //Cheatsheet sign in
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //OnCLick Listeners
    @Override
    public void onClick(View v) {
        if (mListener != null) {
            switch (v.getId()) {
                case R.id.button_google_sign_in:
                    mListener.OnUserInteraction(SignInType.GOOGLE);
                    break;
                case R.id.button_sign_up:
                    mListener.makeSnackBar("Signing in through Cheatsheet is currently disabled. Sign in with Google", Snackbar.LENGTH_LONG);
                    break;
            }
        }
    }

    public enum SignInType {GOOGLE, CHEATSHEET}

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void OnUserInteraction(SignInType type);

        void makeSnackBar(String message, int type);
    }
}
