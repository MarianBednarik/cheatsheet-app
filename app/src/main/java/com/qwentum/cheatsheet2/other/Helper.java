package com.qwentum.cheatsheet2.other;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by marian on 09/05/16.
 */
public class Helper {
    private static Calendar CAL;
    private static SharedPreferences SP;

    public static int calendarGet(int i) {
        CAL = Calendar.getInstance(Locale.UK);
        /*Log.d("HELPER", "MON " + Calendar.MONDAY);
        Log.d("HELPER", "TUE " + Calendar.TUESDAY);
        Log.d("HELPER", "WED " + Calendar.WEDNESDAY);
        Log.d("HELPER", "THU " + Calendar.THURSDAY);
        Log.d("HELPER", "FRI " + Calendar.FRIDAY);
        Log.d("HELPER", "SAT " + Calendar.SATURDAY);
        Log.d("HELPER", "SUN " + Calendar.SUNDAY);*/
        return CAL.get(i) - 1;
    }

    public static int getSharedPref(String pref, Context context) {
        SP = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(SP.getString(pref, "0"));
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
