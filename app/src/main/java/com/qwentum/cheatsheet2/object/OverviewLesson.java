package com.qwentum.cheatsheet2.object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-03-31.
 */

public class OverviewLesson {
    private int mSubjectId;
    private int mDayIndex;
    private int mNumber;
    private String mStart = null;
    private String mEnd = null;
    private int mPosition;
    private ArrayList<OverviewLessonBody> mBodies = new ArrayList<>();
    private int mType = 0;

    public OverviewLesson(JSONObject jsonObject) {
        try {
            //mId = jsonObject.getInt("id");
            mSubjectId = jsonObject.getInt("subjectId");
            mDayIndex = jsonObject.getInt("dayIndex");
            mPosition = jsonObject.getInt("position");
            mNumber = jsonObject.getInt("number");
            mStart = jsonObject.getString("startTime");
            mEnd = jsonObject.getString("endTime");

            JSONArray bodies = jsonObject.getJSONArray("bodies");
            mType = bodies.length();
            for (int i = 0; i < bodies.length(); i++) {
                mBodies.add(new OverviewLessonBody(bodies.getJSONObject(i)));
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    public int getmSubjectId() {
        return mSubjectId;
    }

    public int getmDayIndex() {
        return mDayIndex;
    }

    public int getmPosition() {
        return mPosition;
    }

    public String getmStart() {
        return mStart;
    }

    public ArrayList<OverviewLessonBody> getmBodies() {
        return mBodies;
    }

    public int getmNumber() {
        return mNumber;
    }

    public String getmEnd() {
        return mEnd;
    }

    public int getmType() {
        return mType;
    }
}
