package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-01-08.
 */

public class LessonBody {

    private int mBodyId;
    private int mLocationId;
    private int mTeacherId;

    public LessonBody(JSONObject body) {
        try {
            mBodyId = Integer.parseInt(body.getString("bodyId"));
            mLocationId = Integer.parseInt(body.getString("locationId"));
            mTeacherId = Integer.parseInt(body.getString("teacherId"));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public int getmBodyId() {
        return mBodyId;
    }

    public int getmLocationId() {
        return mLocationId;
    }

    public int getmTeacherId() {
        return mTeacherId;
    }
}
