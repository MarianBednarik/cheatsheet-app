package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-02-26.
 */

public class Message {
    private String mId;
    private String mContent;
    private String mDateCreated;
    private String mAuthorId;

    public Message(JSONObject message) {
        try {
            if (message.has("id")) mId = message.getString("id");
            mContent = message.getString("content");
            if (message.has("created")) mDateCreated = message.getString("created");
            mAuthorId = (message.has("authorId")) ? message.getString("authorId") : message.getString("userId");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmId() {
        return mId;
    }

    public String getmContent() {
        return mContent;
    }

    public String getmDateCreated() {
        return mDateCreated;
    }

    public String getmAuthorId() {
        return mAuthorId;
    }
}
