package com.qwentum.cheatsheet2.object;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.adapter.ReminderAdapter;
import com.qwentum.cheatsheet2.other.Helper;

/**
 * Created by Marian on 2017-01-12.
 */

public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;
    private int mMargin;

    public SimpleDividerItemDecoration(Context context, int padding) {
        mDivider = context.getResources().getDrawable(R.drawable.line_divider);
        mMargin = (int) Helper.pxFromDp(context, padding);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 1; i < childCount; i++) {
            if (parent.getChildViewHolder(parent.getChildAt(i - 1)).getItemViewType() != ReminderAdapter.TYPE_HEADER && parent.getChildViewHolder(parent.getChildAt(i)).getItemViewType() != ReminderAdapter.TYPE_HEADER) {
                View child = parent.getChildAt(i);
                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getTop() - params.bottomMargin - mMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = 0;
        outRect.right = 0;
        outRect.bottom = mMargin;
        outRect.top = mMargin;
        // Add top margin only for the first item to avoid double space between items
        //if (parent.getChildAdapterPosition(view) == 0) outRect.top = 4;
    }
}
