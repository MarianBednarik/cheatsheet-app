package com.qwentum.cheatsheet2.object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by marian on 11/06/16.
 */
public class Lesson {
    private int mId;
    private int mDayIndex;
    private int mNumber;
    private String mStart = null;
    private String mEnd = null;
    private ArrayList<LessonBody> mBodies = new ArrayList<>();
    private boolean mChanged;
    private int mType = 0;

    public Lesson(JSONObject jsonObject) {
        try {
            mId = jsonObject.getInt("id");
            //Log.d(this.getClass().getName(), jsonObject.toString());
            mNumber = jsonObject.getInt("number");
            mStart = jsonObject.getString("startTime");
            mEnd = jsonObject.getString("endTime");
            if (jsonObject.has("changed")) mChanged = jsonObject.getBoolean("changed");
            if (jsonObject.has("dayIndex")) mDayIndex = jsonObject.getInt("dayIndex");

            JSONArray bodies = jsonObject.getJSONArray("bodies");
            mType = bodies.length();
            for (int i = 0; i < bodies.length(); i++) {
                mBodies.add(new LessonBody(bodies.getJSONObject(i)));
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    public static String toJSON(Lesson input) {
        String lessonJSON;
        String bodies = "\"bodies\": [";
        ArrayList<LessonBody> classBodies = input.getmBodies();
        for (int i = 0; i < classBodies.size(); i++) {
            LessonBody currentBody = classBodies.get(i);
            bodies = bodies.concat("{\"locationId\":\"" + currentBody.getmLocationId() + "\","
                    + "\"teacherId\":\"" + currentBody.getmTeacherId() + "\","
                    + "\"bodyId\":\"" + currentBody.getmBodyId() + "\"}");
            if (i != classBodies.size() - 1) {
                bodies = bodies.concat(",");
            }
        }
        bodies = bodies.concat("]");

        lessonJSON = "{"
                + "\"id\":\"" + -1 + "\","
                + "\"number\":" + input.getmNumber() + ","
                + "\"startTime\":\"" + input.getmStart() + "\","
                + "\"endTime\":\"" + input.getmEnd() + "\","
                + bodies + "}";
        return lessonJSON;
    }

    public String getmStart() {
        return mStart;
    }

    public void setmStart(String mStart) {
        this.mStart = mStart;
    }

    public ArrayList<LessonBody> getmBodies() {
        return mBodies;
    }

    public void setmBodies(ArrayList<LessonBody> mBodies) {
        this.mBodies = mBodies;
    }

    public int getmNumber() {
        return mNumber;
    }

    public void setmNumber(int mNumber) {
        this.mNumber = mNumber;
    }

    public String getmEnd() {
        return mEnd;
    }

    public void setmEnd(String mEnd) {
        this.mEnd = mEnd;
    }

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public int getmId() {
        return mId;
    }

    public boolean ismChanged() {
        return mChanged;
    }

    public int getmDayIndex() {
        return mDayIndex;
    }
}
