package com.qwentum.cheatsheet2.object;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-20.
 */

public class User {
    private String mUserId, mUsername, mName, mSurname, mGender, mImage, mCheatPoints, mEmail;
    private ArrayList<SchoolClass> mSchoolClasses = new ArrayList<>();
    private ArrayList<Reminder> mReminders = new ArrayList<>();
    private GoogleSignInAccount mGoogleAccount;
    private int reminderClassId = -1;

    public User(JSONObject userData, GoogleSignInAccount acct) {
        try {

            mUserId = userData.getString("id");
            mUsername = userData.getString("username");
            mName = userData.getString("name");
            mSurname = userData.getString("surname");
            mGender = userData.getString("gender");
            mImage = userData.getString("image");
            mCheatPoints = userData.getString("cheatpoints");
            if (userData.has("mail")) mEmail = userData.getString("mail");
            if (userData.has("classes")) {
                JSONArray classesData = userData.getJSONArray("classes");
                for (int i = 0; i < classesData.length(); i++) {
                    JSONObject object = classesData.getJSONObject(i);
                    mSchoolClasses.add(new SchoolClass(object));
                }
            }
            if (acct != null) mGoogleAccount = acct;
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void setReminders(JSONArray remindersArray, int classId) {
        reminderClassId = classId;
        mReminders.clear();
        try {
            for (int i = 0; i < remindersArray.length(); i++) {
                mReminders.add(new Reminder(remindersArray.getJSONObject(i)));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void addReminder(JSONObject reminder) {
        mReminders.add(new Reminder(reminder));
    }

    public String getUserId() {
        return mUserId;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getFirstName() {
        return mName;
    }

    public String getSurname() {
        return mSurname;
    }

    public String getGender() {
        return mGender;
    }

    public String getUserImage() {
        return mImage;
    }

    public String getCheatPoints() {
        return mCheatPoints;
    }

    public String getEmail() {
        return mEmail;
    }

    public ArrayList<Reminder> getReminders() {
        return mReminders;
    }

    public Reminder getReminder(int index) {
        return mReminders.get(index);
    }

    public ArrayList<SchoolClass> getSchoolClasses() {
        return mSchoolClasses;
    }

    public SchoolClass getSchoolClass(int index) {
        return mSchoolClasses.get(index);
    }

    public GoogleSignInAccount getGoogleAccount() {
        return mGoogleAccount;
    }
}
