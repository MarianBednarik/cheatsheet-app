package com.qwentum.cheatsheet2.object;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.activity.MessagesActivity;
import com.qwentum.cheatsheet2.view.AvatarView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.R.string.ok;

//import com.android.volley.Request;
//import com.android.volley.Response;

/**
 * Created by marian on 11/06/16.
 */
public class CoreSingleton {
    public static final String PREF_USER_SIGNED_IN = "isUserSignedIn";
    public static final String PREF_TIMETABLE_CACHED = "isCurrentTimetableCached";
    public static final String PREF_SELECTED_CLASS = "selectedClass";
    private static final String TAG = "CoreSingleton";
    private static CoreSingleton mInstance;
    private static Context mCtx;
    public int numberOfNewSubjects = 0;
    public int numberOfNewLocations = 0;
    public int numberOfNewTeachers = 0;
    private boolean isTimetableGenerated = false;
    private boolean isTimetableInEditMode = false;
    private SharedPreferences prefMngr;
    //private RequestQueue mRequestQueue;
    private OkHttpClient mNetworkClient;
    //private boolean mIsSignedIn;
    private boolean mShouldSignOut;
    //private GoogleSignInAccount mGoogleAccount;
    private User mUserAccount;
    //private String jsonTimetable;
    private String cachePath;
    private ArrayList<SchoolDay> mTimetable = new ArrayList<>();
    private SparseArray<ClassTeacher> mTeachers = new SparseArray<>();
    private SparseArray<ClassBody> mBodies = new SparseArray<>();
    private SparseArray<ClassLocation> mLocations = new SparseArray<>();
    private ArrayList<Post> mPosts = new ArrayList<>();
    private ArrayList<ClassBody> mBodiesToSend = new ArrayList<>();
    private ArrayList<ClassTeacher> mTeachersToSend = new ArrayList<>();
    private ArrayList<ClassLocation> mLocationsToSend = new ArrayList<>();
    private ArrayList<Lesson> mLessonsToSend = new ArrayList<>();
    private TimetableGeneratedEventListener mTimetableListener;
    private RemindersGetEventListener mRemindersListener;

    private CoreSingleton(Context context) {
        mCtx = context;
        //mRequestQueue = getRequestQueue();
        /*int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(new File(context.getCacheDir(),"networkCache"), cacheSize);
        networkClient = new OkHttpClient.Builder().cache(cache).build();*/
        mNetworkClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        Glide.get(context).register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(mNetworkClient));
        prefMngr = PreferenceManager.getDefaultSharedPreferences(context);
        //mIsSignedIn = getPreference("isUserSignedIn");
    }

    public static synchronized CoreSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CoreSingleton(context);
        }
        return mInstance;
    }

    public static boolean isWeekend() {
        Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
    }

    public OkHttpClient getNetworkClient() {
        return mNetworkClient;
    }

    //Timetable
    public void setUpTimetable(Activity activity, String classId) {
        downloadTimetableJsonOk(classId, activity);
    }

    private void downloadTimetableJsonOk(String classId, final Activity activity) {
        RequestBody formBody = new FormBody.Builder()
                .add("classId", classId)
                .add("idToken", mUserAccount.getGoogleAccount().getIdToken())
                .build();
        Request timetableRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getNewTimetableData.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(timetableRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                //System.out.println(response.body().string());
                try {
                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    if (jsonResponse.getBoolean("success")) {
                        JSONObject jsonTimetable = jsonResponse.getJSONObject("data");
                        //writeToFile(jsonTimetable.toString(), cachePath);
                        new AsyncGenerateTimetable().execute(jsonTimetable);
                    } else {
                        JSONObject error = jsonResponse.getJSONObject("error");
                        new android.support.v7.app.AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.myDialog))
                                .setTitle(error.getString("message"))
                                .setMessage(error.getString("details"))
                                .setPositiveButton(ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .show();
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public String newTimetableBuilder() {
        String newTimetable = "{";
        if (mBodiesToSend.size() > 0) {
            newTimetable = newTimetable.concat("\"bodies\":{");
            for (int i = 0; i < mBodiesToSend.size(); i++) {
                newTimetable = newTimetable.concat("\"" + (i + 1) + "\": {"
                        + "\"id\": \"-1\","
                        + "\"name\": \"" + mBodiesToSend.get(i).getmName() + "\","
                        + "\"acronym\": \"" + mBodiesToSend.get(i).getmAcronym() + "\","
                        + "\"icon\": " + null + ","
                        + "\"changed\": " + true + ","
                        + "\"color\": \"" + mBodiesToSend.get(i).getmColor() + "\"}");
                if (i != mBodiesToSend.size() - 1) {
                    newTimetable = newTimetable.concat(",");
                }
            }
            newTimetable = newTimetable.concat("}");
            if (mLocationsToSend.size() > 0 || mTeachersToSend.size() > 0 || mLessonsToSend.size() > 0)
                newTimetable = newTimetable.concat(",");
        }
        if (mLocationsToSend.size() > 0) {
            newTimetable = newTimetable.concat("\"locations\":{");
            for (int i = 0; i < mLocationsToSend.size(); i++) {
                newTimetable = newTimetable.concat("\"" + (i + 1) + "\": {"
                        + "\"id\": \"-1\","
                        + "\"name\": \"" + mLocationsToSend.get(i).getmName() + "\","
                        + "\"changed\": " + true + ","
                        + "\"description\": \"" + mLocationsToSend.get(i).getmDescription() + "\"}");
                if (i != mLocationsToSend.size() - 1) {
                    newTimetable = newTimetable.concat(",");
                }
            }
            newTimetable = newTimetable.concat("}");
            if (mTeachersToSend.size() > 0 || mLessonsToSend.size() > 0)
                newTimetable = newTimetable.concat(",");
        }
        if (mTeachersToSend.size() > 0) {
            newTimetable = newTimetable.concat("\"teachers\":{");
            for (int i = 0; i < mTeachersToSend.size(); i++) {
                newTimetable = newTimetable.concat("\"" + (i + 1) + "\": {"
                        + "\"id\": \"-1\","
                        + "\"name\": \"" + mTeachersToSend.get(i).getmName() + "\","
                        + "\"surname\": \"" + mTeachersToSend.get(i).getmSurname() + "\","
                        + "\"image\": " + null + ","
                        + "\"changed\": " + true + ","
                        + "\"description\": \"" + mTeachersToSend.get(i).getmDescription() + "\"}");
                if (i != mTeachersToSend.size() - 1) {
                    newTimetable = newTimetable.concat(",");
                }
            }
            newTimetable = newTimetable.concat("}");
            if (mLessonsToSend.size() > 0) newTimetable = newTimetable.concat(",");
        }
        if (mLessonsToSend.size() > 0) {
            newTimetable = newTimetable.concat("\"subjects\":[{");
            for (int i = 0; i < mLessonsToSend.size(); i++) {
                Lesson currentLesson = mLessonsToSend.get(i);
                String bodies = "\"bodies\": [";
                for (int j = 0; j < mLessonsToSend.get(i).getmBodies().size(); j++) {
                    LessonBody currentBody = mLessonsToSend.get(i).getmBodies().get(j);
                    bodies = bodies.concat("{\"locationId\":\"" + currentBody.getmLocationId() + "\","
                            + "\"teacherId\":\"" + currentBody.getmTeacherId() + "\","
                            + "\"bodyId\":\"" + currentBody.getmBodyId() + "\"}");
                    if (j != mLessonsToSend.get(i).getmBodies().size() - 1) {
                        bodies = bodies.concat(",");
                    }
                }
                bodies = bodies.concat("]");

                newTimetable = newTimetable.concat("\"id\": \"-1\","
                        + "\"dateIndex\": \"" + currentLesson.getmDayIndex() + "\","
                        + "\"number\": \"" + currentLesson.getmNumber() + "\","
                        + "\"changed\": " + true + ","
                        + "\"startTime\": \"" + currentLesson.getmStart() + "\","
                        + "\"endTime\": \"" + currentLesson.getmEnd() + "\","
                        + bodies + "}");
                if (i != mTeachersToSend.size() - 1) {
                    newTimetable = newTimetable.concat(",");
                }
            }
            newTimetable = newTimetable.concat("]");
        }
        newTimetable = newTimetable.concat("}");
        return newTimetable;
    }

    public void setTimetableOk() {
        RequestBody formBody = new FormBody.Builder()
                .add("classId", mUserAccount.getSchoolClass(getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getCLassId())
                .add("idToken", mUserAccount.getGoogleAccount().getIdToken())
                .add("timetableData", newTimetableBuilder())
                .build();
        Request timetableRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/set/setTimetable.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(timetableRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                //System.out.println(response.body().string());
                try {
                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    if (jsonResponse.getBoolean("success")) {
                        JSONObject jsonTimetable = jsonResponse.getJSONObject("data");
                        Log.d(getClass().getName(), jsonResponse.toString());
                    } else {
                        JSONObject error = jsonResponse.getJSONObject("error");
                        Log.e(getClass().getName(), error.toString());
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void showUserProfile(final Context context, final int position) {
        User localUser = getUserAccount().getSchoolClass(getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS,0)).getMember(position);
        MaterialDialog userCard = new MaterialDialog.Builder(context)
                .title(localUser.getFirstName() + "'s profile info")
                .customView(R.layout.user_profile_card, true)
                .positiveText("MESSAGE")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(context, MessagesActivity.class);
                        intent.putExtra("userLocalId", position);
                        intent.putExtra("classLocalId", getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0));
                        context.startActivity(intent);
                    }
                })
                .neutralText("CANCEL")
                .build();

        View userCardView = userCard.getCustomView();
        AvatarView avatar = (AvatarView) userCardView.findViewById(R.id.user_card_avatar);
        avatar.bind(localUser.getFirstName().substring(0, 1) + localUser.getSurname().substring(0, 1), localUser.getUserImage());
        avatar.changeColorById(R.color.colorAccent);
        TextView name = (TextView) userCardView.findViewById(R.id.user_card_name);
        name.setText(localUser.getFirstName() + ' ' + localUser.getSurname());
        TextView info = (TextView) userCardView.findViewById(R.id.user_card_info);
        info.setText(localUser.getUsername() + " #" + localUser.getUserId() + " - " + localUser.getGender());
        TextView cheatpoints = (TextView) userCardView.findViewById(R.id.user_card_cheatpoints);
        int cheatpointsNumber = Integer.parseInt(localUser.getCheatPoints());
        cheatpoints.setText(cheatpointsNumber == 1 ? cheatpointsNumber + " Cheatpoint" : cheatpointsNumber + " Cheatpoints");

        userCard.show();
    }

    public void setTimetableListener(TimetableGeneratedEventListener eventListener) {
        mTimetableListener = eventListener;
    }

    public void setRemindersListener(RemindersGetEventListener eventListener) {
        mRemindersListener = eventListener;
    }

    public boolean isTimetableInEditMode() {
        return isTimetableInEditMode;
    }

    public void setTimetableInEditMode(boolean input) {
        isTimetableInEditMode = input;
    }

    public boolean getIsTimetableGenerated() {
        return isTimetableGenerated;
    }

    public SchoolDay getTimetable(int day) {
        if (mTimetable != null || mTimetable.size() != 0) {
            return mTimetable.get(day);
        } else {
            throw new NullPointerException();
        }
    }

    public ArrayList<SchoolDay> getTimetable() {
        return mTimetable;
    }

    public SparseArray<ClassTeacher> getmTeachers() {
        return mTeachers;
    }

    public SparseArray<ClassBody> getmBodies() {
        return mBodies;
    }

    public SparseArray<ClassLocation> getmLocations() {
        return mLocations;
    }

    public void addNewClassSubject(String json) {
        try {
            addNewClassSubjectToSend(json);
            numberOfNewSubjects--;
            mBodies.put(numberOfNewSubjects, new ClassBody(new JSONObject(json)));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void addNewClassLocation(String json) {
        try {
            addNewClassLocationToSend(json);
            numberOfNewLocations--;
            mLocations.put(numberOfNewLocations, new ClassLocation(new JSONObject(json)));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void addNewClassTeacher(String json) {
        try {
            addNewClassTeacherToSend(json);
            numberOfNewTeachers--;
            mTeachers.put(numberOfNewTeachers, new ClassTeacher(new JSONObject(json)));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void addNewClassSubjectToSend(String json) {
        try {
            mBodiesToSend.add(new ClassBody(new JSONObject(json)));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void addNewClassLocationToSend(String json) {
        try {
            mLocationsToSend.add(numberOfNewLocations, new ClassLocation(new JSONObject(json)));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void addNewClassTeacherToSend(String json) {
        try {
            mTeachersToSend.add(new ClassTeacher(new JSONObject(json)));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void addNewClassLessonToSend(String json) {
        try {
            mLessonsToSend.add(new Lesson(new JSONObject(json)));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Post> getmPosts() {
        return mPosts;
    }

    public void setmPosts(JSONArray posts) {
        try {
            for (int i = 0; i < posts.length(); i++) {
                mPosts.add(new Post(posts.getJSONObject(i)));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public boolean getPreferenceBoolean(String name, boolean defaultValue) {
        return prefMngr.getBoolean(name, defaultValue);
    }

    public int getPreferenceInt(String name, int defaultValue) {
        return prefMngr.getInt(name, defaultValue);
    }

    public void setPreference(String name, boolean value) {
        SharedPreferences.Editor editor = prefMngr.edit();
        editor.putBoolean(name, value).apply();
    }

    public void setPreference(String name, int value) {
        SharedPreferences.Editor editor = prefMngr.edit();
        editor.putInt(name, value).apply();
    }

    public boolean isUserSignedIn() {
        return getPreferenceBoolean("isUserSignedIn", false);
    }

    public boolean shouldSignOut() {
        return mShouldSignOut;
    }

    public void setShouldSignOut(boolean input) {
        mShouldSignOut = input;
    }

    public User getUserAccount() {
        return mUserAccount;
    }

    //region User account
    public void setUserAccount(User account) {
        if (account == null) {
            mShouldSignOut = true;
            setPreference("isUserSignedIn", false);
            //mIsSignedIn = false;
            mUserAccount = null;
        } else {
            setPreference("isUserSignedIn", true);
            //mIsSignedIn = true;
            mUserAccount = account;
        }
    }

    //region General
    public Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public boolean isServerReachable() {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 176.32.230.45");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public interface TimetableGeneratedEventListener {
        void onTimetableGenerated();
    }
    //endregion

    public interface RemindersGetEventListener {
        void onRemindersDownloaded();
    }

    public class AsyncGenerateTimetable extends AsyncTask<JSONObject, Void, Boolean> {
        protected void onPreExecute() {
            // Runs on the UI thread
        }

        protected Boolean doInBackground(JSONObject... jsonTimetable) {
            generateTimetable(jsonTimetable[0]);
            return true;
        }

        protected void onPostExecute(Boolean isTimetableGeneratedBool) {
            Log.d(TAG, "Timetable generated: " + isTimetableGeneratedBool);
            isTimetableGenerated = true;
            mTimetableListener.onTimetableGenerated();
        }

        public void generateTimetable(JSONObject jsonTimetable) {

            try {
                String[] days = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};
                JSONObject timetable = jsonTimetable.getJSONObject("timetable");
                JSONObject bodies = jsonTimetable.getJSONObject("bodies");
                JSONObject teachers = jsonTimetable.getJSONObject("teachers");
                JSONObject locations = jsonTimetable.getJSONObject("locations");

                //Timetable
                for (String day : days) {
                    ArrayList<Lesson> lessonArray = null;
                    if (timetable.has(day)) {
                        //Log.d(TAG, "Generating timetable for " + day);
                        lessonArray = new ArrayList<>();
                        JSONArray dayTimetable = timetable.getJSONArray(day);
                        //if (dayTimetable.length() != 0) {
                        for (int i = 0; i < dayTimetable.length(); i++) {
                            lessonArray.add(new Lesson(dayTimetable.getJSONObject(i)));
                        }
                        //Log.d(TAG, dayTimetable.toString());
                            /*JSONArray lessonIds = dayTimetable.names();
                            for (int i = 0; i < lessonIds.length(); i++) {
                                //Log.d(TAG, dayTimetable.getJSONObject(lessonIds.get(i).toString()).toString());
                                lessonArray.add(new Lesson(dayTimetable.getJSONObject(lessonIds.get(i).toString())));
                            }*/
                        //} else {

                        //}


                    }

                    if (lessonArray != null) {
                        mTimetable.add(new SchoolDay(lessonArray));
                    } else {
                        mTimetable.add(null);
                    }
                }

                //Bodies
                JSONArray bodyIds = bodies.names();
                Log.d(TAG, bodyIds.toString());
                for (int i = 0; i < bodyIds.length(); i++) {
                    //Log.d(TAG, "Generating body with id " + bodyIds.get(i));
                    mBodies.put(Integer.parseInt(bodyIds.get(i).toString()), new ClassBody(bodies.getJSONObject(bodyIds.get(i).toString())));
                }

                //Teachers
                JSONArray teacherIds = teachers.names();
                for (int i = 0; i < teacherIds.length(); i++) {
                    //Log.d(TAG, "Generating teacher with id " + teacherIds.get(i));
                    mTeachers.put(Integer.parseInt(teacherIds.get(i).toString()), new ClassTeacher(teachers.getJSONObject(teacherIds.get(i).toString())));
                }

                //Locations
                JSONArray locationIds = locations.names();
                for (int i = 0; i < locationIds.length(); i++) {
                    //Log.d(TAG, "Generating location with id " + locationIds.get(i));
                    mLocations.put(Integer.parseInt(locationIds.get(i).toString()), new ClassLocation(locations.getJSONObject(locationIds.get(i).toString())));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    /*public static Context getApplicationContext() {
        return mCtx;
    }*/
    //endregion
}
