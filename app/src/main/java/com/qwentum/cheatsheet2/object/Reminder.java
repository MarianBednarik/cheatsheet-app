package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-02-21.
 */

public class Reminder {
    String mReminderId, mClassId, mReminderName, mType, mSubject, mDateOfReminder;

    public Reminder(JSONObject reminder) {
        try {
            if (reminder.has("id")) mReminderId = reminder.getString("id");
            if (reminder.has("classId")) mClassId = reminder.getString("classId");
            mReminderName = (reminder.has("name")) ? reminder.getString("name") : reminder.getString("reminderName");
            mType = (reminder.has("type")) ? reminder.getString("type") : reminder.getString("reminderType");
            mSubject = reminder.getString("subject");
            mDateOfReminder = reminder.getString("dateOfReminder");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getReminderId() {
        return mReminderId;
    }

    public String getClassId() {
        return mClassId;
    }

    public String getReminderName() {
        return mReminderName;
    }

    public String getType() {
        return mType;
    }

    public String getSubject() {
        return mSubject;
    }

    public String getDateOfReminder() {
        return mDateOfReminder;
    }
}
