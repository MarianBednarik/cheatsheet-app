package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-03-31.
 */

public class ClassLocation {

    private String mName;
    private String mDescription;

    public ClassLocation(JSONObject input) {
        try {
            mName = input.getString("name");
            mDescription = input.getString("description");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmName() {
        return mName;
    }

    public String getmDescription() {
        return mDescription;
    }
}
