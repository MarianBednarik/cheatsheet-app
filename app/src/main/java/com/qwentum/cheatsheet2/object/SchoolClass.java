package com.qwentum.cheatsheet2.object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-20.
 */

public class SchoolClass {
    private String mClassId, mNameShort, mSchool, mClassName;
    private ArrayList<User> mMembers = new ArrayList<>();
    private ArrayList<ClassActivity> mActivities = new ArrayList<>();

    public SchoolClass(JSONObject object) {
        try {
            mClassId = object.getString("classId");
            mNameShort = object.getString("nameShort");
            mSchool = object.getString("school");
            mClassName = object.getString("name");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getCLassId() {
        return mClassId;
    }

    public String getNameShort() {
        return mNameShort;
    }

    public String getSchool() {
        return mSchool;
    }

    public String getClassName() {
        return mClassName;
    }

    public void setmActivity(ArrayList<ClassActivity> activities) {
        mActivities = activities;
    }

    public ArrayList<ClassActivity> getmActivities() {
        return mActivities;
    }

    public ClassActivity getmActivity(int index) {
        return mActivities.get(index);
    }

    public ArrayList<User> getMembers() {
        return mMembers;
    }

    public void setMembers(JSONArray memberArray) {
        if (mMembers.isEmpty()) {
            for (int i = 0; i < memberArray.length(); i++) {
                try {
                    mMembers.add(new User(memberArray.getJSONObject(i), null));
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public User getMember(int index) {
        return mMembers.get(index);
    }
}
