package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-03-31.
 */

public class ClassTeacher {
    private String mName;
    private String mSurname;
    private String mDescription;

    public ClassTeacher(JSONObject input) {
        try {
            mName = input.getString("name");
            mSurname = input.getString("surname");
            mDescription = input.getString("description");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmSurname() {
        return mSurname;
    }

    public String getmName() {
        return mName;
    }

    public String getmDescription() {
        return mDescription;
    }

}
