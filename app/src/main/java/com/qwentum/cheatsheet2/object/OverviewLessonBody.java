package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-03-31.
 */

public class OverviewLessonBody {

    private int mBodyId;
    private String mBodyName;
    private String mAcronym;
    private String mIcon;
    private String mColor;
    private int mTeacherId;
    private String mTeacherName;
    private String mTeacherSurname;
    private String mTeacherDescription;
    private int mLocationId;
    private String mLocationName;
    private String mLocationDescription;

    public OverviewLessonBody(JSONObject body) {
        try {
            mBodyId = Integer.parseInt(body.getString("bodyId"));
            mLocationId = Integer.parseInt(body.getString("locationId"));
            mTeacherId = Integer.parseInt(body.getString("teacherId"));
            mBodyName = body.getString("bodyName");
            mAcronym = body.getString("acronym");
            mIcon = body.getString("icon");
            mColor = body.getString("color");
            mTeacherName = body.getString("teacherName");
            mTeacherSurname = body.getString("teacherSurname");
            mTeacherDescription = body.getString("teacherDescription");
            mLocationName = body.getString("locationName");
            mLocationDescription = body.getString("locationDescription");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmBodyName() {
        return mBodyName;
    }

    public String getmAcronym() {
        return mAcronym;
    }

    public String getmIcon() {
        return mIcon;
    }

    public String getmColor() {
        return mColor;
    }

    public String getmTeacherName() {
        return mTeacherName;
    }

    public String getmTeacherSurname() {
        return mTeacherSurname;
    }

    public String getmTeacherDescription() {
        return mTeacherDescription;
    }

    public String getmLocationName() {
        return mLocationName;
    }

    public String getmLocationDescription() {
        return mLocationDescription;
    }

    public int getmBodyId() {
        return mBodyId;
    }

    public int getmLocationId() {
        return mLocationId;
    }

    public int getmTeacherId() {
        return mTeacherId;
    }

}
