package com.qwentum.cheatsheet2.object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-02-26.
 */

public class ClassActivity {
    private String mId;
    private String mActivityType;
    private String mHeader;
    private String mSubject;
    private String mPosterName;
    private String mPosterSurname;
    private String mDateCreated;
    private String mReminderType;
    private String mReminderDate;
    private String mPostContent;
    private String mLikeCount;
    private String mDislikeCount;
    private ArrayList<String> mTags = new ArrayList<>();

    public ClassActivity(JSONObject activity) {
        try {
            mId = activity.getString("id");
            mActivityType = activity.getString("activityType");
            mHeader = activity.getString("header");
            if (activity.has("subject")) mSubject = activity.getString("subject");
            mPosterName = activity.getString("name");
            mPosterSurname = activity.getString("surname");
            mDateCreated = activity.getString("created");
            if (activity.has("reminderType")) mReminderType = activity.getString("reminderType");
            if (activity.has("reminderDate")) mReminderDate = activity.getString("reminderDate");
            if (activity.has("postContent")) mPostContent = activity.getString("postContent");
            mLikeCount = activity.getString("likeCount");
            mDislikeCount = activity.getString("dislikeCount");
            if (activity.has("tags")) {
                JSONArray tags = activity.getJSONArray("tags");
                for (int i = 0; i < tags.length(); i++) {
                    mTags.add(tags.getJSONObject(i).getString("tag"));
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmId() {
        return mId;
    }

    public String getmActivityType() {
        return mActivityType;
    }

    public String getmHeader() {
        return mHeader;
    }

    public String getmSubject() {
        return mSubject;
    }

    public String getmPosterName() {
        return mPosterName;
    }

    public String getmPosterSurname() {
        return mPosterSurname;
    }

    public String getmDateCreated() {
        return mDateCreated;
    }

    public String getmReminderType() {
        return mReminderType;
    }

    public String getmReminderDate() {
        return mReminderDate;
    }

    public String getmPostContent() {
        return mPostContent;
    }

    public String getmLikeCount() {
        return mLikeCount;
    }

    public String getmDislikeCount() {
        return mDislikeCount;
    }

    public ArrayList<String> getmTags() {
        return mTags;
    }
}
