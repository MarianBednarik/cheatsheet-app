package com.qwentum.cheatsheet2.object;

import java.util.ArrayList;

/**
 * Created by marian on 12/06/16.
 */
public class SchoolDay {
    private ArrayList<Lesson> mLessons = new ArrayList<>();
    private String mStartTime = null;
    private String mEndTime = null;

    public SchoolDay(ArrayList<Lesson> lessons) {
        if (lessons != null) {
            mLessons = lessons;
            mStartTime = mLessons.get(0).getmStart();
            mEndTime = mLessons.get(mLessons.size() - 1).getmEnd();
        }
    }

    public Lesson getLesson(int position) {
        return mLessons.get(position);
    }

    public ArrayList<Lesson> getLessons() {
        return mLessons;
    }

    public int getLessonLength() {
        return mLessons.size();
    }

    public String getStartTime() {
        return mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public void setmLessons(ArrayList<Lesson> mLessons) {
        this.mLessons = mLessons;
    }
}
