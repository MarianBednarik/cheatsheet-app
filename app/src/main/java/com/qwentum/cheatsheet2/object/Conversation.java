package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-02-26.
 */

public class Conversation {
    private String mId;
    private String mFirstUserId;
    private String mSecondUserId;
    private String mContent;
    private String mLastMessageTime;
    private String mFirstName;
    private String mFirstSurname;
    private String mSecondName;
    private String mSecondSurname;
    private String mFirstImage;
    private String mSecondImage;

    private String mClassLastName;
    private String mClassLastSurname;

    public Conversation(JSONObject convo) {
        try {
            mId = convo.getString("id");
            if (convo.has("firstUserId")) mFirstUserId = convo.getString("firstUserId");
            if (convo.has("firstImage")) mFirstImage = convo.getString("firstImage");
            if (convo.has("secondImage")) mSecondImage = convo.getString("secondImage");
            if (convo.has("secondUserId")) mSecondUserId = convo.getString("secondUserId");
            mContent = convo.getString("content");
            mLastMessageTime = (convo.has("lastMessage")) ? convo.getString("lastMessage") : convo.getString("created");
            if (convo.has("firstName")) mFirstName = convo.getString("firstName");
            if (convo.has("firstSurname")) mFirstSurname = convo.getString("firstSurname");
            if (convo.has("secondName")) mSecondName = convo.getString("secondName");
            if (convo.has("secondSurname")) mSecondSurname = convo.getString("secondSurname");
            if (convo.has("name")) mClassLastName = convo.getString("name");
            if (convo.has("surname")) mClassLastSurname = convo.getString("surname");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmId() {
        return mId;
    }

    public String getmFirstUserId() {
        return mFirstUserId;
    }

    public String getmSecondUserId() {
        return mSecondUserId;
    }

    public String getmContent() {
        return mContent;
    }

    public String getmLastMessageTime() {
        return mLastMessageTime;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public String getmFirstSurname() {
        return mFirstSurname;
    }

    public String getmSecondName() {
        return mSecondName;
    }

    public String getmSecondSurname() {
        return mSecondSurname;
    }

    public String getmFirstImage() {
        return mFirstImage;
    }

    public String getmSecondImage() {
        return mSecondImage;
    }

    public String getmClassLastName() {
        return mClassLastName;
    }

    public String getmClassLastSurname() {
        return mClassLastSurname;
    }
}
