package com.qwentum.cheatsheet2.object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Marian on 2017-04-20.
 */

public class Post {

    private String mId;
    private String mName;
    private String mSubject;
    private String mContent;
    private String mCreated;
    private String mUserId;
    private String mUserName;
    private String mUserSurname;
    private String mUserImage;
    private String mLikeCount;
    private String mDislikeCount;
    private ArrayList<String> mTags = new ArrayList<>();

    public Post(JSONObject json) {
        try {
            mId = json.getString("id");
            mName = json.getString("name");
            mSubject = json.getString("subject");
            mContent = json.getString("content");
            mCreated = json.getString("created");
            mUserId = json.getString("userId");
            mUserName = json.getString("userName");
            mUserSurname = json.getString("userSurname");
            mUserImage = json.getString("userImage");
            mLikeCount = json.getString("likeCount");
            mDislikeCount = json.getString("dislikeCount");
            JSONArray tags = json.getJSONArray("tags");
            for (int i = 0; i < tags.length(); i++) {
                mTags.add(tags.getJSONObject(i).getString("tag"));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmId() {
        return mId;
    }

    public String getmName() {
        return mName;
    }

    public String getmSubject() {
        return mSubject;
    }

    public String getmContent() {
        return mContent;
    }

    public String getmCreated() {
        return mCreated;
    }

    public String getmUserId() {
        return mUserId;
    }

    public String getmUserName() {
        return mUserName;
    }

    public String getmUserSurname() {
        return mUserSurname;
    }

    public String getmUserImage() {
        return mUserImage;
    }

    public String getmLikeCount() {
        return mLikeCount;
    }

    public String getmDislikeCount() {
        return mDislikeCount;
    }

    public ArrayList<String> getmTags() {
        return mTags;
    }
}
