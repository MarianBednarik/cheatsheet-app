package com.qwentum.cheatsheet2.object;

/**
 * Created by Marian on 2016-02-24.
 */
public class Hardcoded {
    public String[] namesA = {"Adam Abrahám",
            "Marián Bednárik", "Richard Borbély",
            "Adrián Boros", "Roman Ďurovič",
            "Matej Holý", "Alexander Kaiser",
            "Eduard Krivánek", "Marek Kurča",
            "Gabriel Levai", "Tomáš Maťko",
            "Patrik Németh", "Alex Sporni",
            "Dominik Telkesi", "Matej Záhorský",
            "Marek Zeleňák"};
    public String[] namesB = {"Daniel Faludi",
            "Juraj Garaj", "Ján Kučera",
            "Michal Kuruc", "Patrik Máčik",
            "Igor Mjasojedov", "Veronika Nagyová",
            "Michal Packa", "Michal Toth",
            "Daniel Weis", "Tomáš Žigo"};
}
