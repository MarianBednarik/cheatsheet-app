package com.qwentum.cheatsheet2.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marian on 2017-03-31.
 */

public class ClassBody {
    private String mName;
    private String mAcronym;
    private String mIcon;
    private String mColor;

    public ClassBody(JSONObject input) {
        try {
            mName = input.getString("name");
            mAcronym = input.getString("acronym");
            mIcon = input.getString("icon");
            mColor = input.getString("color");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public String getmName() {
        return mName;
    }

    public String getmAcronym() {
        return mAcronym;
    }

    public String getmIcon() {
        return mIcon;
    }

    public String getmColor() {
        return mColor;
    }
}
