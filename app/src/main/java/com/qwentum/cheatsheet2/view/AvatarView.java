package com.qwentum.cheatsheet2.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qwentum.cheatsheet2.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Marian on 2017-02-11.
 */

public class AvatarView extends FrameLayout {
    CircleImageView profile;
    ImageView background, icon;
    TextView initial;
    Context mCtx;

    public AvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mCtx = context;
        LayoutInflater.from(context).inflate(R.layout.avatar, this);

        profile = (CircleImageView) findViewById(R.id.profile_image);
        icon = (ImageView) findViewById(R.id.icon_drawable);
        background = (ImageView) findViewById(R.id.background_circle);
        initial = (TextView) findViewById(R.id.initial_text);
    }

    public void bind(Drawable drawable) {
        icon.setVisibility(View.VISIBLE);
        icon.setImageDrawable(drawable);
    }

    public void bind(String nameAcronym, String imageLink) {
        if (!imageLink.equals("null") && !imageLink.equals("")) {
            profile.setVisibility(View.VISIBLE);
            Glide.with(mCtx)
                    .load(imageLink)
                    .into(profile);
        } else {
            initial.setVisibility(View.VISIBLE);
            initial.setText(nameAcronym);
        }
    }

    public void changeColorByHex(String color) {
        Drawable temp = background.getDrawable().mutate();
        temp.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
        background.setImageDrawable(temp);
    }

    public void changeColorById(int color) {
        Drawable temp = background.getDrawable().mutate();
        temp.setColorFilter(ContextCompat.getColor(mCtx, color), PorterDuff.Mode.MULTIPLY);
        background.setImageDrawable(temp);
    }
}
