package com.qwentum.cheatsheet2.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.CoreSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NewUserActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mClassShortName = null;
    EditText mClassLongName = null;
    EditText mClassSchoolName = null;
    private Context mCtx;
    private String mIdToken;
    private OkHttpClient mNetworkClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        mCtx = getApplicationContext();
        mIdToken = getIntent().getExtras().getString("idToken");
        mNetworkClient = CoreSingleton.getInstance(mCtx).getNetworkClient();

        findViewById(R.id.button_create_class).setOnClickListener(this);
        findViewById(R.id.button_join_class).setOnClickListener(this);
    }

    private void joinClassOk(String classToken) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mIdToken)
                .add("token", classToken)
                .build();
        Request joinClassRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/user/joinClass.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(joinClassRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.root_layout_activity_new_user), mCtx.getResources().getString(R.string.text_snackbar_server_error) + ' ' + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            finish();
                        } else {
                            JSONObject error = jsonResponse.getJSONObject("error");
                            Log.e(this.getClass().getName(), error.getString("message") + ": " + error.getString("details"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 69) {
            finish();
        }
    }*/

    private void createClassOk() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("idToken", mIdToken);
        formBuilder.add("nameShort", mClassShortName.getText().toString());
        formBuilder.add("name", mClassLongName.getText().toString());
        formBuilder.add("school", mClassSchoolName.getText().toString());
        Request joinClassRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/create/createClass.php")
                .post(formBuilder.build())
                .build();

        mNetworkClient.newCall(joinClassRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.root_layout_activity_new_user), mCtx.getResources().getString(R.string.text_snackbar_server_error) + ' ' + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            finish();
                        } else {
                            JSONObject error = jsonResponse.getJSONObject("error");
                            Log.e(this.getClass().getName(), error.getString("message") + ": " + error.getString("details"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_create_class:
                /*Intent intent = new Intent(mCtx, AddClassActivity.class);
                intent.putExtra("idToken", mIdToken);
                startActivityForResult(intent, 69);*/
                final MaterialDialog newClassDialog = new MaterialDialog.Builder(this)
                        .title("New class")
                        .customView(R.layout.new_class_dialog, true)
                        .positiveText("Create")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                /*String reminderJson = "{" + "\"idToken\":\"" + mIdToken + "\","
                                        + "\"nameShort\":\"" + classShortName.getText().toString() + "\","
                                        + "\"name\":\"" + classLongName.getText().toString() + "\","
                                        + "\"school\":\"" + classSchoolName.getText().toString() + "\"}";*/
                                createClassOk();
                            }
                        })
                        .build();
                View tempView = newClassDialog.getCustomView();
                mClassShortName = (EditText) tempView.findViewById(R.id.edit_text_newclass_shortname);
                mClassLongName = (EditText) tempView.findViewById(R.id.edit_text_newclass_longname);
                mClassSchoolName = (EditText) tempView.findViewById(R.id.edit_text_newclass_school);
                newClassDialog.show();
                break;
            case R.id.button_join_class:
                new MaterialDialog.Builder(this)
                        .title("Class token")
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("input token...", null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                joinClassOk(input.toString());
                            }
                        }).show();
                break;
        }
    }
}
