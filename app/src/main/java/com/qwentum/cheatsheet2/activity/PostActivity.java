package com.qwentum.cheatsheet2.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.afollestad.materialdialogs.util.DialogUtils;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.adapter.PostTagAdapter;
import com.qwentum.cheatsheet2.object.ClassActivity;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Post;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.other.DividerItemDecoration;
import com.qwentum.cheatsheet2.other.ItemClickSupport;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import jp.wasabeef.richeditor.RichEditor;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostActivity extends AppCompatActivity implements View.OnClickListener, ColorChooserDialog.ColorCallback {

    private RichEditor mEditor;
    private Context mCtx;
    private CoreSingleton mCoreS;
    private boolean mIsBgColorChanged;
    private boolean mIsTextColorChanged;
    private ClassActivity mSelectedPostActivity;
    private Post mSelectedPost;
    private EditText mPostTitleEdittext;
    private EditText postDialogSubject;
    private TextView mPostTitleSubtitle;
    private PostTagAdapter mPostTagAdapter;
    private int mNumberOfTags = 0;
    private boolean mCanEdit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        ActionBar actionBar = getSupportActionBar();
        mCtx = getApplicationContext();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.post_edit_title_layout);
            actionBar.setDisplayHomeAsUpEnabled(true);
            mPostTitleEdittext = (EditText) actionBar.getCustomView().findViewById(R.id.post_title_edit_text);
            mPostTitleSubtitle = (TextView) actionBar.getCustomView().findViewById(R.id.post_title_subtitle);
        }


        mCoreS = CoreSingleton.getInstance(mCtx);

        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setPlaceholder("Loading...");
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            //setTitle("Post editor");
            mPostTitleEdittext.setText("New post");
            mEditor.setPlaceholder("Insert text here...");
            mPostTagAdapter = new PostTagAdapter(new ArrayList<String>());
            mPostTagAdapter.canInteract(mCanEdit);
        } else {
            if (extras.getBoolean("isFromActivity")) {
                mSelectedPostActivity = mCoreS.getUserAccount().getSchoolClass(extras.getInt("currentClass")).getmActivity(extras.getInt("postId"));
                mPostTitleEdittext.setText(mSelectedPostActivity.getmHeader());
                mPostTitleSubtitle.setText(mSelectedPostActivity.getmSubject());
                mEditor.setHtml(mSelectedPostActivity.getmPostContent());
                mPostTagAdapter = new PostTagAdapter(mSelectedPostActivity.getmTags());
            } else {
                mSelectedPost = mCoreS.getmPosts().get(extras.getInt("postId"));
                mPostTitleEdittext.setText(mSelectedPost.getmName());
                mPostTitleSubtitle.setText(mSelectedPost.getmSubject());
                mEditor.setHtml(mSelectedPost.getmContent());
                mPostTagAdapter = new PostTagAdapter(mSelectedPost.getmTags());
            }
            mCanEdit = false;
            mPostTitleEdittext.setKeyListener(null);
            mPostTitleSubtitle.setVisibility(View.VISIBLE);
            mEditor.setFocusableInTouchMode(false);
            mPostTagAdapter.canInteract(mCanEdit);
            findViewById(R.id.tools).setVisibility(View.GONE);

        }


        mEditor.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
            @Override
            public void onTextChange(String text) {
                Log.d("Editor", text);
            }
        });

        findViewById(R.id.action_align_center).setOnClickListener(this);
        findViewById(R.id.action_align_left).setOnClickListener(this);
        findViewById(R.id.action_align_right).setOnClickListener(this);
        findViewById(R.id.action_bg_color).setOnClickListener(this);
        findViewById(R.id.action_blockquote).setOnClickListener(this);
        findViewById(R.id.action_bold).setOnClickListener(this);
        findViewById(R.id.action_heading1).setOnClickListener(this);
        findViewById(R.id.action_heading2).setOnClickListener(this);
        findViewById(R.id.action_heading3).setOnClickListener(this);
        findViewById(R.id.action_heading4).setOnClickListener(this);
        findViewById(R.id.action_heading5).setOnClickListener(this);
        findViewById(R.id.action_heading6).setOnClickListener(this);
        findViewById(R.id.action_indent).setOnClickListener(this);
        findViewById(R.id.action_insert_bullets).setOnClickListener(this);
        findViewById(R.id.action_insert_checkbox).setOnClickListener(this);
        findViewById(R.id.action_undo).setOnClickListener(this);
        findViewById(R.id.action_underline).setOnClickListener(this);
        findViewById(R.id.action_txt_color).setOnClickListener(this);
        findViewById(R.id.action_insert_image).setOnClickListener(this);
        findViewById(R.id.action_insert_link).setOnClickListener(this);
        findViewById(R.id.action_insert_numbers).setOnClickListener(this);
        findViewById(R.id.action_superscript).setOnClickListener(this);
        findViewById(R.id.action_subscript).setOnClickListener(this);
        findViewById(R.id.action_redo).setOnClickListener(this);
        findViewById(R.id.action_italic).setOnClickListener(this);
        findViewById(R.id.action_strikethrough).setOnClickListener(this);
        findViewById(R.id.action_outdent).setOnClickListener(this);
    }

    private void makeToast(final String message, final int length) {
        PostActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(PostActivity.this, message, length).show();
            }
        });
    }

    private void makeLog(final String message) {
        Log.d("PostActivity", message);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_align_center:
                mEditor.setAlignCenter();
                break;
            case R.id.action_align_left:
                mEditor.setAlignLeft();
                break;
            case R.id.action_align_right:
                mEditor.setAlignRight();
                break;
            case R.id.action_bg_color:
                if (!mIsBgColorChanged) {
                    new ColorChooserDialog.Builder(this, R.string.text_post_color_chooser_background)
                            .titleSub(R.string.text_post_color_chooser_background)  // title of dialog when viewing shades of a color
                            .accentMode(false)  // when true, will display accent palette instead of primary palette
                            .doneButton(R.string.md_done_label)  // changes label of the done button
                            .cancelButton(R.string.md_cancel_label)  // changes label of the cancel button
                            .backButton(R.string.md_back_label)  // changes label of the back button
                            .dynamicButtonColor(false)  // defaults to true, false will disable changing action buttons' color to currently selected color
                            .show();
                } else {
                    mEditor.setTextBackgroundColor(Color.WHITE);
                    mIsBgColorChanged = !mIsBgColorChanged;
                }
                break;
            case R.id.action_blockquote:
                mEditor.setBlockquote();
                break;
            case R.id.action_bold:
                mEditor.setBold();
                break;
            case R.id.action_heading1:
                mEditor.setHeading(1);
                break;
            case R.id.action_heading2:
                mEditor.setHeading(2);
                break;
            case R.id.action_heading3:
                mEditor.setHeading(3);
                break;
            case R.id.action_heading4:
                mEditor.setHeading(4);
                break;
            case R.id.action_heading5:
                mEditor.setHeading(5);
                break;
            case R.id.action_heading6:
                mEditor.setHeading(6);
                break;
            case R.id.action_indent:
                mEditor.setIndent();
                break;
            case R.id.action_insert_bullets:
                mEditor.setBullets();
                break;
            case R.id.action_insert_checkbox:
                mEditor.insertTodo();
                break;
            case R.id.action_insert_image:
                //mEditor.insertTodo();
                makeToast("Inserting images is currently not supported", Toast.LENGTH_LONG);
                break;
            case R.id.action_insert_link:
                new MaterialDialog.Builder(this)
                        .title("Insert link")
                        .positiveText("OK")
                        .negativeText("Cancel")
                        .input("https://", null, false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                mEditor.insertLink(input.toString().trim(), input.toString().trim());
                            }
                        })
                        .show();
                break;
            case R.id.action_insert_numbers:
                mEditor.setNumbers();
                break;
            case R.id.action_italic:
                mEditor.setItalic();
                break;
            case R.id.action_outdent:
                mEditor.setOutdent();
                break;
            case R.id.action_redo:
                mEditor.redo();
                break;
            case R.id.action_strikethrough:
                mEditor.setStrikeThrough();
                break;
            case R.id.action_subscript:
                mEditor.setSubscript();
                break;
            case R.id.action_superscript:
                mEditor.setSuperscript();
                break;
            case R.id.action_txt_color:
                if (!mIsTextColorChanged) {
                    new ColorChooserDialog.Builder(this, R.string.text_post_color_chooser)
                            .titleSub(R.string.text_post_color_chooser)  // title of dialog when viewing shades of a color
                            .accentMode(false)  // when true, will display accent palette instead of primary palette
                            .doneButton(R.string.md_done_label)  // changes label of the done button
                            .cancelButton(R.string.md_cancel_label)  // changes label of the cancel button
                            .backButton(R.string.md_back_label)  // changes label of the back button
                            //.preselect(accent ? accentPreselect : primaryPreselect)  // optionally preselects a color
                            .dynamicButtonColor(false)  // defaults to true, false will disable changing action buttons' color to currently selected color
                            .show();
                } else {
                    mEditor.setTextColor(Color.BLACK);
                    mIsTextColorChanged = !mIsTextColorChanged;
                }
                break;
            case R.id.action_underline:
                mEditor.setUnderline();
                break;
            case R.id.action_undo:
                mEditor.undo();
                break;
        }
    }

    @Override
    public void onColorSelection(@NonNull ColorChooserDialog dialog, @ColorInt int color) {
        if (dialog.getTitle() == R.string.text_post_color_chooser) {
            mEditor.setTextColor(DialogUtils.getActionTextStateList(this, color).getDefaultColor());
            mIsTextColorChanged = !mIsTextColorChanged;
        } else {
            mEditor.setTextBackgroundColor(DialogUtils.getActionTextStateList(this, color).getDefaultColor());
            mIsBgColorChanged = !mIsBgColorChanged;
        }
    }

    @Override
    public void onColorChooserDismissed(@NonNull ColorChooserDialog dialog) {
        makeToast("Dismissed", Toast.LENGTH_LONG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post_editor, menu);
        MenuItem actionbarIcon = menu.findItem(R.id.action_send_post);
        if (!mCanEdit)
            actionbarIcon.setIcon(mCtx.getResources().getDrawable(R.drawable.ic_info_black_24dp));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_send_post:
                if (mCanEdit) {
                    postDialogSubject = null;
                    final MaterialDialog postDialog = new MaterialDialog.Builder(this)
                            .title("Post information")
                            .customView(R.layout.post_upload_dialog_layout, true)
                            .positiveText("POST")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    createPostOk(mPostTitleEdittext.getText().toString(), postDialogSubject.getText().toString(), mEditor.getHtml(), mPostTagAdapter.getTagsJson());
                                }
                            })
                            .neutralText("CANCEL")
                            .build();

                    View dialogCustomView = postDialog.getCustomView();
                    postDialogSubject = (EditText) dialogCustomView.findViewById(R.id.edit_text_post_dialog_subject);
                    RecyclerView tagsRecycler = (RecyclerView) dialogCustomView.findViewById(R.id.post_dialog_tags_recycler);
                    tagsRecycler.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.HORIZONTAL, false));
                    tagsRecycler.setAdapter(mPostTagAdapter);
                    tagsRecycler.addItemDecoration(new DividerItemDecoration(mCtx.getResources().getDrawable(R.drawable.line_divider)));
                    ItemClickSupport.addTo(tagsRecycler).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            if (mPostTagAdapter.getItemViewType(position) == 0) {
                                //normal tag
                                mPostTagAdapter.removeTag(position);
                                mPostTagAdapter.notifyItemRemoved(position);
                                mNumberOfTags--;
                            } else {
                                //add tag
                                new MaterialDialog.Builder(postDialog.getContext())
                                        .title("Add tag")
                                        .input("Tag", null, false, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                mPostTagAdapter.addTag(input.toString().trim());
                                                mPostTagAdapter.notifyItemInserted(mNumberOfTags);
                                                mNumberOfTags++;
                                            }
                                        })
                                        .positiveText("Add")
                                        .neutralText("Cancel")
                                        .show();
                            }
                        }
                    });
                    postDialog.show();
                    return true;
                } else {
                    MaterialDialog postDialog = new MaterialDialog.Builder(this)
                            .title("Post information")
                            .customView(R.layout.post_upload_dialog_layout, true)
                            .positiveText("OK")
                            .build();

                    View dialogCustomView = postDialog.getCustomView();
                    dialogCustomView.findViewById(R.id.post_info_dialog_text_subject).setVisibility(View.GONE);
                    dialogCustomView.findViewById(R.id.edit_text_post_dialog_subject).setVisibility(View.GONE);
                    RecyclerView tagsRecycler = (RecyclerView) dialogCustomView.findViewById(R.id.post_dialog_tags_recycler);
                    tagsRecycler.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.HORIZONTAL, false));
                    tagsRecycler.setAdapter(mPostTagAdapter);
                    tagsRecycler.addItemDecoration(new DividerItemDecoration(mCtx.getResources().getDrawable(R.drawable.line_divider)));
                    postDialog.show();
                    return true;
                }

        }
        return super.onOptionsItemSelected(item);
    }

    private void createPostOk(String header, String subject, String content, String tags) {
        User user = mCoreS.getUserAccount();
        OkHttpClient networkClient = mCoreS.getNetworkClient();
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", user.getGoogleAccount().getIdToken())
                .add("classId", user.getSchoolClasses().get(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getCLassId())
                .add("header", header)
                .add("subject", subject)
                .add("content", content)
                .add("tags", tags)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/create/createPost.php")
                .post(formBody)
                .build();

        networkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    makeToast("createReminder : " + response.code() + " " + response.message(), Toast.LENGTH_SHORT);
                } else {
                    try {
                        JSONObject responseSuccess = new JSONObject(response.body().string());
                        if (responseSuccess.getBoolean("success")) {
                            makeToast("Post successfuly created", Toast.LENGTH_SHORT);
                        } else {
                            makeToast("Couldn't create post", Toast.LENGTH_SHORT);
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
}

