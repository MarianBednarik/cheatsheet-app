package com.qwentum.cheatsheet2.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.CoreSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AddClassActivity extends AppCompatActivity implements View.OnClickListener {

    CoreSingleton mCoreS;
    Context mCtx;
    OkHttpClient mNetworkClient;
    TextInputEditText mClassShortName;
    TextInputEditText mClassLongName;
    TextInputEditText mClassSchool;
    String mIdToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);
        setupActionBar();
        mCtx = getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mCtx);
        mNetworkClient = mCoreS.getNetworkClient();
        mIdToken = getIntent().getExtras().getString("idToken");
        mClassShortName = (TextInputEditText) findViewById(R.id.input_class_short_name);
        mClassLongName = (TextInputEditText) findViewById(R.id.input_class_full_name);
        mClassSchool = (TextInputEditText) findViewById(R.id.input_school);
        findViewById(R.id.button_add_class).setOnClickListener(this);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_class:
                createClassOk();
                break;
        }
    }

    private void createClassOk() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("idToken", mIdToken);
        formBuilder.add("nameShort", mClassShortName.getText().toString());
        formBuilder.add("name", mClassLongName.getText().toString());
        formBuilder.add("school", mClassSchool.getText().toString());
        Request joinClassRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/create/createClass.php")
                .post(formBuilder.build())
                .build();

        mNetworkClient.newCall(joinClassRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.root_layout_activity_new_user), mCtx.getResources().getString(R.string.text_snackbar_server_error) + ' ' + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            finish();
                        } else {
                            JSONObject error = jsonResponse.getJSONObject("error");
                            Log.e(this.getClass().getName(), error.getString("message") + ": " + error.getString("details"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
}
