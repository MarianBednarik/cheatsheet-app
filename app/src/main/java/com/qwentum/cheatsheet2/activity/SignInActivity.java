package com.qwentum.cheatsheet2.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.facebook.stetho.Stetho;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//import com.android.volley.Request;
//import com.android.volley.Response;

public class SignInActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    //Static variables
    private final static String TAG = "SignInActivity";
    private final static int RC_SIGN_IN = 0;

    //Variables
    //private float mScreenDpiDensity;
    //private boolean mIsAnimationDone = false;
    //private boolean mIsEditTextFocused = false;
    private boolean mIsSignInCached = false;
    private Context mCtx;
    private int fragmentState = 0;
    private GoogleSignInAccount mSignInAccount;

    //Views
    //private ImageView mLogo;
    //private TextView mAppName;
    //private ConstraintLayout mLayoutLoginOptions;
    private ConstraintLayout mRootLayout;

    //Objects
    private CoreSingleton mCoreS;
    private GoogleApiClient mGoogleApiClient;
    //private ProgressDialog mProgressDialog;
    //private SignInButton mSignInButton;
    //private AccelerateDecelerateInterpolator mASInterpolator;
    private OkHttpClient mNetworkClient;
    //private SharedPreferences prefMngr;
    private FragmentManager fm;
    //private SignInFragment signInFragment, signUpFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Log.e(TAG, "savedInstanceState for SignInActivity " + savedInstanceState);
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_sign_in_new);
        mCtx = getApplication();
        fm = getSupportFragmentManager();
        Stetho.initializeWithDefaults(this);

        //Initialize variables
        mCoreS = CoreSingleton.getInstance(getApplication());
        mNetworkClient = mCoreS.getNetworkClient();
        mIsSignInCached = mCoreS.isUserSignedIn();
        SignInButton mSignInButton = (SignInButton) findViewById(R.id.button_google_sign_in);
        mSignInButton.setOnClickListener(this);
        //Log.d(TAG, "Sign in cached: " + mIsSignInCached);

        mRootLayout = (ConstraintLayout) findViewById(R.id.root_login_layout);

        //Sign In Setup
        apiClientSetup();
        //findViewById(R.id.text_change_sign_in).setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mIsSignInCached) {
            findViewById(R.id.frame_layout_loading).setVisibility(View.GONE);
        }

        if (!mCoreS.shouldSignOut()) {
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
            if (opr.isDone()) {
                // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                // and the GoogleSignInResult will be available instantly.
                Log.d(TAG, "Got cached sign-in");
                //mIsSignInCached = true;
                GoogleSignInResult result = opr.get();
                handleSignInResult(result);
            } else {
                // If the user has not previously signed in on this device or the sign-in has expired,
                // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                // single sign-on will occur in this branch.
                //showProgressDialog();
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                        //hideProgressDialog();
                        handleSignInResult(googleSignInResult);
                    }
                });
            }
        } else {
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void apiClientSetup() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("823958943773-avq5f2qst0q7t0l8u1eddhv3rtr73c5h.apps.googleusercontent.com")
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //mSignInButton.setSize(SignInButton.SIZE_WIDE);
        //mSignInButton.setScopes(gso.getScopeArray());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            if (mCoreS.isNetworkAvailable()) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            } else {
                //No network
                makeSnackBar(R.string.text_snackbar_no_internet_connection, Snackbar.LENGTH_LONG);
            }
        }
        if (requestCode == 70) {
            getUserOk(mSignInAccount);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            mSignInAccount = acct;
            //Log.d("Login status", "Logged in as " + acct.getDisplayName());
            /*SharedPreferences.Editor editor = prefMngr.edit();
            editor.putBoolean("isSignInCached", true).apply();*/
            if (mIsSignInCached) {
                getUserOk(acct);
            } else {
                onLoginOk(acct);
            }
        } else {
            // Signed out, show unauthenticated UI.
            if (result.getStatus().getStatusCode() != 4) {
                Snackbar.make(mRootLayout, R.string.text_snackbar_sign_in_failed, Snackbar.LENGTH_LONG).show();
            }
            Log.e(TAG, "Failed to sign in with google services : " + result.getStatus());
        }
    }

    private void signInWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOutWithGoogle() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Log.d(TAG, "Signed out with status : " + status.toString());
                        if (status.isSuccess()) {
                            Snackbar.make(mRootLayout, "Successfully signed out", Snackbar.LENGTH_LONG).show();
                            mCoreS.setShouldSignOut(false);
                            findViewById(R.id.frame_layout_loading).setVisibility(View.GONE);
                        }
                    }
                });
    }


    private void onLoginOk(final GoogleSignInAccount acct) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", acct.getIdToken())
                .build();
        Request onLoginRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/user/onLogin.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(onLoginRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(mRootLayout, mCtx.getResources().getString(R.string.text_snackbar_server_error) + ' ' + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            if (!jsonResponse.getBoolean("newUser")) {
                                getUserOk(acct);
                            } else {
                                Intent intent = new Intent(mCtx, NewUserActivity.class);
                                intent.putExtra("idToken", acct.getIdToken());
                                startActivityForResult(intent, 70);
                            }
                        } else {
                            JSONObject error = jsonResponse.getJSONObject("error");
                            new android.support.v7.app.AlertDialog.Builder(new ContextThemeWrapper(SignInActivity.this, R.style.myDialog))
                                    .setTitle(error.getString("message"))
                                    .setMessage(error.getString("details"))
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                        }
                                    })
                                    .show();
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    private void getUserOk(final GoogleSignInAccount acct) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", acct.getIdToken())
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/user/getUser.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(mRootLayout, mCtx.getResources().getString(R.string.text_snackbar_server_error) + ' ' + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            JSONObject userData = jsonResponse.getJSONObject("data").getJSONObject("userData");
                            if (userData.getJSONArray("classes").length() > 0) {
                                User user = new User(userData, acct);
                                mCoreS.setUserAccount(user);
                                //mCoreS.setUpTimetable(SignInActivity.this, user.getSchoolClasses().get(mCoreS.getPreferenceInt("selectedClass",0)).getCLassId());
                                Intent intent = new Intent(getApplication(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(mCtx, NewUserActivity.class);
                                intent.putExtra("idToken", acct.getIdToken());
                                startActivityForResult(intent, 70);
                            }
                        } else {
                            JSONObject error = jsonResponse.getJSONObject("error");
                            Log.e(TAG, error.getString("message") + " : " + error.getString("details"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    public void makeSnackBar(int message, int type) {
        Snackbar.make(mRootLayout, message, type).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_google_sign_in:
                signInWithGoogle();
                break;
        }
    }

    //Google API connection listeners
    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "Google API connected successfully");
        if (mCoreS.shouldSignOut()) {
            Log.d(TAG, "Should sign out");
            signOutWithGoogle();
        }
    }

    public void onConnectionSuspended(int cause) {
        Log.e(TAG, "Google API connection is suspended");
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google API failed to connect");
    }
}
