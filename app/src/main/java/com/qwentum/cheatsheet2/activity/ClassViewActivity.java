package com.qwentum.cheatsheet2.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.adapter.ClassViewContentAdapter;
import com.qwentum.cheatsheet2.adapter.MemberAdapter;
import com.qwentum.cheatsheet2.fragment.ClassViewContent;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.SchoolClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ClassViewActivity extends AppCompatActivity implements View.OnClickListener {

    int localClassId;
    SchoolClass localClass;
    boolean selected = false;
    RecyclerView membersRecycler;
    MemberAdapter memberAdapter;
    MaterialDialog tokenDialog;
    private Context mCtx;
    private CoreSingleton mCoreS;
    private OkHttpClient mNetworkClient;
    private ClassViewContentAdapter mContentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_view);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        mCtx = getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mCtx);
        mNetworkClient = mCoreS.getNetworkClient();
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),
                mCtx));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);


        localClassId = getIntent().getIntExtra("localClassId", 0);
        mContentAdapter = new ClassViewContentAdapter(mCtx, ClassViewContentAdapter.Type.POSTS, localClassId);
        localClass = mCoreS.getUserAccount().getSchoolClass(localClassId);
        setTitle(localClass.getClassName());
    }

    public ClassViewContentAdapter getmContentAdapter() {
        return mContentAdapter;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_get_token) {
            getInviteTokenOk(mCoreS.getUserAccount().getGoogleAccount().getIdToken(), localClass.getCLassId());
            tokenDialog = new MaterialDialog.Builder(this)
                    .title("Class token")
                    .positiveText("Copy")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("Token", tokenDialog.getContentView().getText());
                            clipboard.setPrimaryClip(clip);
                        }
                    })
                    .negativeText("Share")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            ShareCompat.IntentBuilder
                                    .from(ClassViewActivity.this)
                                    .setText(tokenDialog.getContentView().getText().toString())
                                    .setType("text/plain")
                                    .setChooserTitle("Share token with...")
                                    .startChooser();
                        }
                    })
                    .neutralText("Cancel")
                    .build();
        }
    }

    private void setTokenText(final String token) {
        ClassViewActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tokenDialog.setContent(token);
                tokenDialog.show();
            }
        });
    }

    private String getInviteTokenOk(String idToken, String classId) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", idToken)
                .add("classId", classId)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getInviteToken.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.fragment_container), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            setTokenText(jsonResponse.getJSONObject("data").getString("token"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();

                    }
                }
            }
        });
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getClassMembersOk(String idToken, String classId, String dateNow) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", idToken)
                .add("classId", classId)
                .add("dateNow", dateNow)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getClassData.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.fragment_container), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.getJSONObject("data");

                        if (jsonData.has("members")) {

                            mCoreS.getUserAccount().getSchoolClass(localClassId).setMembers(jsonData.getJSONArray("members"));
                            ClassViewActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memberAdapter = new MemberAdapter(mCtx, localClass.getMembers());
                                    membersRecycler.setAdapter(memberAdapter);
                                    memberAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();

                    }
                }
            }
        });
    }

    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[]{"Posts", "Members", "Subjects", "Teachers", "Locations"};
        private SparseArray<ClassViewContent> mContentFragments = new SparseArray<>();
        private Context context;

        public SampleFragmentPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Fragment getItem(int position) {
            if (mContentFragments.get(position) == null) {
                mContentFragments.put(position, ClassViewContent.newInstance(position + 1, localClassId));
            }
            return mContentFragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }
}
