package com.qwentum.cheatsheet2.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.adapter.MessagesInConversationAdapter;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Message;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.other.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MessagesActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mCtx;
    private CoreSingleton mCoreS;
    private User mLocalUser;
    private User mRemoteUser;
    private OkHttpClient mNetworkClient;
    private int mClassLocalId;
    private int mRemoteUserLocalId;
    private ArrayList<Message> mMessages = new ArrayList<>();
    //public int mOffset = 0;
    private RecyclerView mMessageRecyclerView;
    private MessagesInConversationAdapter mMessageAdapter;
    private EditText mMessageEditText;
    private EndlessRecyclerViewScrollListener scrollListener;
    private boolean mIsConversationForClass = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mCtx = getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mCtx);
        mNetworkClient = mCoreS.getNetworkClient();
        mLocalUser = mCoreS.getUserAccount();
        Bundle extras = getIntent().getExtras();
        mClassLocalId = extras.getInt("classLocalId", 0);
        mRemoteUserLocalId = extras.getInt("userLocalId", -1);
        if (mRemoteUserLocalId == -1) {
            mIsConversationForClass = true;
            setTitle(mLocalUser.getSchoolClass(mClassLocalId).getClassName());
        } else {
            mRemoteUser = mLocalUser.getSchoolClass(mClassLocalId).getMember(mRemoteUserLocalId);
            setTitle(mRemoteUser.getFirstName() + ' ' + mRemoteUser.getSurname());
        }
        findViewById(R.id.button_send_message).setOnClickListener(this);
        mMessageEditText = (EditText) findViewById(R.id.edit_text_create_message);

        mMessageRecyclerView = (RecyclerView) findViewById(R.id.conversation_messages_recycler_view);
        mMessageAdapter = new MessagesInConversationAdapter(mCtx, mMessages, mRemoteUser, mLocalUser, mIsConversationForClass);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setReverseLayout(true);
        llm.setStackFromEnd(true);
        mMessageRecyclerView.setLayoutManager(llm);
        mMessageRecyclerView.setHasFixedSize(true);
        mMessageRecyclerView.setAdapter(mMessageAdapter);
        mMessageRecyclerView.scrollToPosition(0);

        scrollListener = new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                loadMoreMessages(totalItemsCount);
            }
        };
        // Adds the scroll listener to RecyclerView
        mMessageRecyclerView.addOnScrollListener(scrollListener);

        mMessageEditText.setOnClickListener(this);
        mMessageEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Log.d("MESSAGES", "has focus");
                    mMessageRecyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mMessageRecyclerView.scrollToPosition(0);
                        }
                    }, 400);
                }
            }
        });
        if (!mIsConversationForClass) {
            getDirectMessagesOk(mLocalUser.getGoogleAccount().getIdToken(), mRemoteUser.getUserId(), 0);
        } else {
            getClassMessagesOk(mLocalUser.getGoogleAccount().getIdToken(), mLocalUser.getSchoolClass(mClassLocalId).getCLassId(), 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateRecyclerView(final int start, final int count) {
        Log.d("CONVO", "Starting at " + start + " count " + count);
        MessagesActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessageAdapter.updateMessages(mMessages);
                mMessageAdapter.notifyItemRangeInserted(start, count);
                if (start == 0) mMessageRecyclerView.scrollToPosition(0);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_send_message:
                if (!mMessageEditText.getText().toString().equals("")) {
                    String messageText = mMessageEditText.getText().toString().trim();
                    if (!messageText.equals("")) {
                        if (!mIsConversationForClass) {
                            createDirectMessagesOk(mLocalUser.getGoogleAccount().getIdToken(), mRemoteUser.getUserId(), mMessageEditText.getText().toString());
                        } else {
                            createClassMessagesOk(mLocalUser.getGoogleAccount().getIdToken(), mLocalUser.getSchoolClass(mClassLocalId).getCLassId(), mMessageEditText.getText().toString());
                        }
                    }
                }
                break;
            case R.id.edit_text_create_message:
                mMessageRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mMessageRecyclerView.scrollToPosition(0);
                    }
                }, 400);
                break;
        }
    }

    private void loadMoreMessages(int offset) {
        if (!mIsConversationForClass) {
            getDirectMessagesOk(mLocalUser.getGoogleAccount().getIdToken(), mRemoteUser.getUserId(), offset);
        } else {
            getClassMessagesOk(mLocalUser.getGoogleAccount().getIdToken(), mLocalUser.getSchoolClass(mClassLocalId).getCLassId(), offset);
        }
    }

    private void getDirectMessagesOk(String idToken, String remoteUserId, final int offset) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", idToken)
                .add("otherUserId", remoteUserId)
                .add("offset", String.valueOf(offset))
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getDirectMessages.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.fragment_container), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            JSONArray messages = jsonResponse.getJSONObject("data").getJSONArray("messages");
                            for (int i = 0; i < messages.length(); i++) {
                                mMessages.add(new Message(messages.getJSONObject(i)));
                            }
                            //mOffset += messages.length();
                        }
                        updateRecyclerView(offset, mMessages.size() - offset);
                    } catch (JSONException ex) {
                        ex.printStackTrace();

                    }
                }
            }
        });
    }

    private void createDirectMessagesOk(String idToken, String remoteUserId, final String content) {
        if (content.length() < 65536) {
            RequestBody formBody = new FormBody.Builder()
                    .add("idToken", idToken)
                    .add("otherUserId", remoteUserId)
                    .add("content", content)
                    .build();
            Request getUserRequest = new Request.Builder()
                    .url("http://www.cheatsheet.sk/php/create/createDirectMessage.php")
                    .post(formBody)
                    .build();

            mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        Snackbar.make(findViewById(R.id.fragment_container), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                    } else {
                        try {
                            JSONObject jsonResponse = new JSONObject(response.body().string());
                            if (jsonResponse.getBoolean("success")) {
                                MessagesActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mMessageEditText.setText("");
                                        /*mMessageEditText.clearFocus();
                                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(mMessageEditText.getWindowToken(), 0);*/
                                        try {
                                            mMessages.add(0, new Message(new JSONObject("{\"content\":\"" + content + "\",\"authorId\":\"" + mLocalUser.getUserId() + "\"}")));
                                        } catch (JSONException ex) {
                                            ex.printStackTrace();
                                        }
                                        updateRecyclerView(0, 1);
                                        Toast.makeText(mCtx, "Message sent", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();

                        }
                    }
                }
            });
        }
    }

    private void getClassMessagesOk(String idToken, String classId, final int offset) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", idToken)
                .add("classId", classId)
                .add("offset", String.valueOf(offset))
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getMessages.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.fragment_container), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            JSONArray messages = jsonResponse.getJSONObject("data").getJSONArray("messages");
                            for (int i = 0; i < messages.length(); i++) {
                                mMessages.add(new Message(messages.getJSONObject(i)));
                            }
                            //mOffset += messages.length();
                        }
                        updateRecyclerView(offset, mMessages.size() - offset);
                    } catch (JSONException ex) {
                        ex.printStackTrace();

                    }
                }
            }
        });
    }

    private void createClassMessagesOk(String idToken, String classId, final String content) {
        if (content.length() < 65536) {
            RequestBody formBody = new FormBody.Builder()
                    .add("idToken", idToken)
                    .add("classId", classId)
                    .add("content", content)
                    .build();
            Request getUserRequest = new Request.Builder()
                    .url("http://www.cheatsheet.sk/php/create/createMessage.php")
                    .post(formBody)
                    .build();

            mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        Snackbar.make(findViewById(R.id.fragment_container), "getClassMembers : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                    } else {
                        try {
                            JSONObject jsonResponse = new JSONObject(response.body().string());
                            if (jsonResponse.getBoolean("success")) {
                                MessagesActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mMessageEditText.setText("");
                                        /*mMessageEditText.clearFocus();
                                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(mMessageEditText.getWindowToken(), 0);*/
                                        try {
                                            mMessages.add(0, new Message(new JSONObject("{\"content\":\"" + content + "\",\"authorId\":\"" + mLocalUser.getUserId() + "\"}")));
                                        } catch (JSONException ex) {
                                            ex.printStackTrace();
                                        }
                                        updateRecyclerView(0, 1);
                                        Toast.makeText(mCtx, "Message sent", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();

                        }
                    }
                }
            });
        }
    }
}
