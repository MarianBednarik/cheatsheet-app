package com.qwentum.cheatsheet2.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.qwentum.cheatsheet2.BuildConfig;
import com.qwentum.cheatsheet2.LessonViewActivity;
import com.qwentum.cheatsheet2.R;
import com.qwentum.cheatsheet2.fragment.ConversationsFragment;
import com.qwentum.cheatsheet2.fragment.OverviewFragment;
import com.qwentum.cheatsheet2.fragment.RemindersAllFragment;
import com.qwentum.cheatsheet2.fragment.TimetablePageFragment;
import com.qwentum.cheatsheet2.object.CoreSingleton;
import com.qwentum.cheatsheet2.object.Lesson;
import com.qwentum.cheatsheet2.object.OverviewLesson;
import com.qwentum.cheatsheet2.object.SchoolClass;
import com.qwentum.cheatsheet2.object.User;
import com.qwentum.cheatsheet2.other.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//import com.android.volley.Response;
//import com.android.volley.toolbox.ImageRequest;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OverviewFragment.OnFragmentInteractionListener,
        CoreSingleton.TimetableGeneratedEventListener,
        ConversationsFragment.OnFragmentInteractionListener,
        View.OnClickListener {

    private static final String TAG = "Main";
    //private ArrayList<Integer> fragmentStack = new ArrayList<>();
    TimetablePageFragment timetablePageFragment;
    OverviewFragment overviewFragment;
    ConversationsFragment mConversationsFragment;
    RemindersAllFragment mRemidersAllFragment;
    TextView textTimer;
    Toolbar toolbar;
    Calendar cal;
    FragmentTransaction fragmentTransaction;
    SimpleDateFormat tf = new SimpleDateFormat("HH:mm:ss");
    //Timetable timetable;
    CoreSingleton mCoreS;
    Context mContext;
    User mUserAccount;
    SchoolClass currentSelectedClass;
    OkHttpClient mNetworkClient;
    EditText reminderSubject;
    Spinner reminderType;
    TextView reminderDate;
    Menu optionsMenu;
    FloatingActionsMenu fabMenu;
    FloatingActionButton fabEditTimetable;
    ImageView cancelEditButton;
    MenuItem saveButton;
    GoogleSignInAccount gAcct;
    private ContentType currentContent;
    private String path = "/timetable/json.txt";
    private EditText mClassShortName;
    private EditText mClassLongName;
    private EditText mClassSchoolName;

    //Dialogs
    private EditText reminderName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = getApplicationContext();
        mCoreS = CoreSingleton.getInstance(mContext);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate()");
        mUserAccount = mCoreS.getUserAccount();

        if (mUserAccount == null) {
            Intent firstIntent = new Intent(this, SignInActivity.class);

            firstIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(firstIntent);

            finish();
            return;
        }

        //fragmentStack.add(0);
        currentContent = ContentType.OVERVIEW;
        //path = getCacheDir() + path;

        mNetworkClient = mCoreS.getNetworkClient();
        mCoreS.setTimetableListener(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textTimer = new TextView(this);
        setTitle(mUserAccount.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getNameShort());

        fabMenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);
        fabEditTimetable = (FloatingActionButton) findViewById(R.id.fab_timetable_edit);
        fabEditTimetable.setOnClickListener(this);
        cancelEditButton = (ImageView) findViewById(R.id.button_cancel_timetable_edit);
        cancelEditButton.setOnClickListener(this);
        FloatingActionButton fabNewReminder = (FloatingActionButton) findViewById(R.id.fab_new_reminder);
        FloatingActionButton fabNewPost = (FloatingActionButton) findViewById(R.id.fab_new_post);
        FloatingActionButton fabNewClassMessage = (FloatingActionButton) findViewById(R.id.fab_new_class_message);
        fabNewReminder.setIcon(R.drawable.ic_event_black_24dp);
        fabNewReminder.setOnClickListener(this);
        fabNewPost.setIcon(R.drawable.ic_edit_black_24dp);
        fabNewPost.setOnClickListener(this);
        fabNewClassMessage.setIcon(R.drawable.ic_message_black_24dp);
        fabNewClassMessage.setOnClickListener(this);

        FrameLayout container = (FrameLayout) findViewById(R.id.fragment_container);
        View loading_bar = getLayoutInflater().inflate(R.layout.loading_fragment, null);
        container.addView(loading_bar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        TextView textName = (TextView) header.findViewById(R.id.text_drawer_username);
        TextView textEmail = (TextView) header.findViewById(R.id.text_drawer_email);
        navigationView.getMenu().getItem(5).getSubMenu().getItem(3).setTitle("Version " + BuildConfig.VERSION_NAME);
        final ImageView imageProfile = (ImageView) header.findViewById(R.id.image_profile_picture);

        //Adding user classes to the drawer
        Menu menu = navigationView.getMenu().getItem(4).getSubMenu();
        ArrayList<SchoolClass> classes = mUserAccount.getSchoolClasses();
        for (int i = 0; i < classes.size(); i++) {
            menu.add(R.id.drawer_group_classes, i, Menu.NONE, classes.get(i).getNameShort()).setIcon(R.drawable.ic_star_black_24dp);
        }
        //menu.getItem(mCoreS.getPreferenceInt("selectedClass",0) + 1).setChecked(true);
        currentSelectedClass = classes.get(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0));
        //Adding user info to drawer
        gAcct = mUserAccount.getGoogleAccount();
        textName.setText(gAcct.getDisplayName());
        textEmail.setText(gAcct.getEmail());

        /*mNetworkClient.newCall(photoUrlRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.e(TAG, response.body().toString());
                } else {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            InputStream inputStream = response.body().byteStream();
                            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                            imageProfile.setImageBitmap(bitmap);
                        }
                    });
                }
            }
        });*/
        Glide.get(mContext).register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(mNetworkClient));
        Glide.with(mContext)
                .load(gAcct.getPhotoUrl().toString())
                .into(imageProfile);
        //TODO UNCOMMENT NEXT LINE ONLY
        getClassDataOk(currentSelectedClass.getCLassId(), Calendar.getInstance().getTime().toString(), "10");
        getPostsOk(gAcct, currentSelectedClass.getCLassId(), "10", "0");
    }

    private void getPostsOk(final GoogleSignInAccount acct, String classID, String limit, String offset) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", acct.getIdToken())
                .add("classId", classID)
                .add("limit", limit)
                .add("offset", offset)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getPosts.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    //Snackbar.make(mRootLayout, mCtx.getResources().getString(R.string.text_snackbar_server_error) + ' ' + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            JSONArray posts = jsonResponse.getJSONObject("data").getJSONArray("posts");
                            if (posts.length() > 0) {
                                mCoreS.setmPosts(posts);
                                //mCoreS.setUpTimetable(SignInActivity.this, user.getSchoolClasses().get(mCoreS.getPreferenceInt("selectedClass",0)).getCLassId());
                                /*Intent intent = new Intent(getApplication(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);*/
                            } else {
                                Toast.makeText(mContext, "No posts no load", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            JSONObject error = jsonResponse.getJSONObject("error");
                            Log.e(TAG, error.getString("message") + " : " + error.getString("details"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
        Resources res = getResources();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.setTaskDescription(new ActivityManager.TaskDescription(res.getString(R.string.app_name),
                    BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher),
                    ContextCompat.getColor(mContext, R.color.colorPrimaryDark)));
        }

        int currentClassId = mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0);
        if (currentSelectedClass.getCLassId() != mUserAccount.getSchoolClass(currentClassId).getCLassId()) {
            currentSelectedClass = mUserAccount.getSchoolClass(currentClassId);
            //TODO uncomment
            getClassDataOk(currentSelectedClass.getCLassId(), Calendar.getInstance().getTime().toString(), "0");
            setTitle(mUserAccount.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getNameShort());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        saveButton = menu.findItem(R.id.save_button);
        saveButton.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mCoreS.setTimetableOk();
                return false;
            }
        });
        textTimer.setTextColor(ContextCompat.getColor(mContext, R.color.md_grey_50));
        //tv.setPadding(5, 0, 5, 0);
        textTimer.setTextSize(21);
        menu.add(0, Menu.FIRST + 1, Menu.NONE, "00:00").setActionView(textTimer).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    public void onFragmentInteraction(Uri uri) {
        //TODO fragment interaction
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item_text clicks here.
        int id = item.getItemId();
        //if (id != currentPage) {
        switch (id) {
            case R.id.nav_overview:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.setElevation(16);
                }
                currentContent = ContentType.OVERVIEW;
                findViewById(R.id.fab_timetable_edit).setVisibility(View.GONE);
                findViewById(R.id.fab_menu).setVisibility(View.VISIBLE);
                setTitle(mUserAccount.getSchoolClass(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getNameShort());
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, overviewFragment).commit();
                break;
            case R.id.nav_timetable:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.setElevation(0);
                }
                currentContent = ContentType.TIMETABLE;
                findViewById(R.id.fab_timetable_edit).setVisibility(View.VISIBLE);
                findViewById(R.id.fab_menu).setVisibility(View.GONE);
                setTitle(R.string.text_drawer_option_timetable);
                if (timetablePageFragment == null)
                    timetablePageFragment = new TimetablePageFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, timetablePageFragment).commit();
                break;
            case R.id.nav_messages:
                //getMessagesOk();
                currentContent = ContentType.MESSAGES;
                findViewById(R.id.fab_timetable_edit).setVisibility(View.GONE);
                findViewById(R.id.fab_menu).setVisibility(View.VISIBLE);
                setTitle("Messages");
                if (mConversationsFragment == null)
                    mConversationsFragment = ConversationsFragment.newInstance(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0));
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mConversationsFragment).commit();
                break;
            case R.id.nav_assignments:
                currentContent = ContentType.REMINDERS;
                findViewById(R.id.fab_timetable_edit).setVisibility(View.GONE);
                findViewById(R.id.fab_menu).setVisibility(View.VISIBLE);
                setTitle("Reminders");
                if (mRemidersAllFragment == null)
                    mRemidersAllFragment = RemindersAllFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mRemidersAllFragment).commit();
                break;
            /*case R.id.nav_classmates:
                setTitle(R.string.text_drawer_option_classmates);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.setElevation(16);
                }
                ClassmatesFragment classmatesFragment = new ClassmatesFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, classmatesFragment).commit();
                break;*/
                /*case R.id.nav_classes_add_class:
                    startActivity(new Intent(this, AddClassActivity.class));
                    break;*/
            case R.id.nav_other_account:
                break;
            case R.id.nav_other_settings:
                //startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.nav_other_log_out:
                new MaterialDialog.Builder(this)
                        .title("Sign out")
                        .content("Are you sure you want sign out?")
                        .positiveText("Yes")
                        .negativeText("No")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                mCoreS.setUserAccount(null);
                                quit();
                            }
                        })
                        .show();
                break;
            case R.id.nav_other_app_version:
                new android.support.v7.app.AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.myDialog))
                        .setTitle("About application")
                        .setMessage("Created by Marián Bednárik \n" +
                                "App version code: " + BuildConfig.VERSION_CODE + '\n' +
                                "App version: " + BuildConfig.VERSION_NAME)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .show();
                break;
            case R.id.nav_classes_add_class:
                final MaterialDialog newClassDialog = new MaterialDialog.Builder(this)
                        .title("New class")
                        .customView(R.layout.new_class_dialog, true)
                        .positiveText("Create")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                /*String reminderJson = "{" + "\"idToken\":\"" + mIdToken + "\","
                                        + "\"nameShort\":\"" + classShortName.getText().toString() + "\","
                                        + "\"name\":\"" + classLongName.getText().toString() + "\","
                                        + "\"school\":\"" + classSchoolName.getText().toString() + "\"}";*/
                                createClassOk();
                            }
                        })
                        .build();
                View tempView = newClassDialog.getCustomView();
                mClassShortName = (EditText) tempView.findViewById(R.id.edit_text_newclass_shortname);
                mClassLongName = (EditText) tempView.findViewById(R.id.edit_text_newclass_longname);
                mClassSchoolName = (EditText) tempView.findViewById(R.id.edit_text_newclass_school);
                newClassDialog.show();
                break;
            default:
                //Only addded classes should pass here
                //Snackbar.make(findViewById(R.id.coordinator_layout),"Loaded class ID " +  item.getItemId(), Snackbar.LENGTH_SHORT).show();
                /*item.setChecked(true);
                mCoreS.setPreference(CoreSingleton.PREF_SELECTED_CLASS, item.getItemId());
                getClassDataOk(mUserAccount.getGoogleAccount().getIdToken(),mUserAccount.getSchoolClasses().get(item.getItemId()).getCLassId(),Calendar.getInstance().getTime().toString());*/
                //currentPage = -1;
                Intent intent = new Intent(this, ClassViewActivity.class);
                intent.putExtra("localClassId", item.getItemId());
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
        //}
        //return false;
    }

    private void createClassOk() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("idToken", mUserAccount.getGoogleAccount().getIdToken());
        formBuilder.add("nameShort", mClassShortName.getText().toString());
        formBuilder.add("name", mClassLongName.getText().toString());
        formBuilder.add("school", mClassSchoolName.getText().toString());
        Request joinClassRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/create/createClass.php")
                .post(formBuilder.build())
                .build();

        mNetworkClient.newCall(joinClassRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.root_layout_activity_new_user), mContext.getResources().getString(R.string.text_snackbar_server_error) + ' ' + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.getBoolean("success")) {
                            mCoreS.setPreference(CoreSingleton.PREF_SELECTED_CLASS,mCoreS.getUserAccount().getSchoolClasses().size() + 1);
                            finish();
                        } else {
                            JSONObject error = jsonResponse.getJSONObject("error");
                            Log.e(this.getClass().getName(), error.getString("message") + ": " + error.getString("details"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    private void quit() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    private void createMessageOk(String content) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", mUserAccount.getGoogleAccount().getIdToken())
                .add("classId", mUserAccount.getSchoolClasses().get(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getCLassId())
                .add("content", content)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/create/createMessage.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.coordinator_layout), "createReminder : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject responseSuccess = new JSONObject(response.body().string());
                        if (responseSuccess.getBoolean("success")) {
                            Snackbar.make(findViewById(R.id.coordinator_layout), "Message successfully sent", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(findViewById(R.id.coordinator_layout), "Could not create message.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    public void onClick(View view) {
        fabMenu.collapse();
        switch (view.getId()) {
            case R.id.button_cancel_timetable_edit:
                cancelEditButton.setVisibility(View.GONE);
                mCoreS.setTimetableInEditMode(false);
                fabEditTimetable.setIconDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_edit_black_24dp));
                saveButton.setVisible(false);
                setTitle("Timetable");
                break;
            case R.id.fab_timetable_edit:
                if (!mCoreS.isTimetableInEditMode()) {
                    //Start timetable editing
                    mCoreS.setTimetableInEditMode(true);
                    cancelEditButton.setVisibility(View.VISIBLE);
                    fabEditTimetable.setIconDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_add_black_24dp));
                    saveButton.setVisible(true);
                    setTitle("Editing Timetable");
                } else {
                    //Add lesson
                    Intent intent = new Intent(mContext, LessonViewActivity.class);
                    startActivityForResult(intent, 14);
                }
                break;
            case R.id.fab_new_reminder:
                final MaterialDialog reminderDialog = new MaterialDialog.Builder(this)
                        .title("New reminder")
                        .customView(R.layout.new_reminder, true)
                        .positiveText("Create")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                String reminderJson = "{" + "\"reminderName\":\"" + reminderName.getText().toString() + "\","
                                        + "\"subject\":\"" + reminderSubject.getText().toString() + "\","
                                        + "\"reminderType\":\"" + String.valueOf(reminderType.getSelectedItemId()) + "\","
                                        + "\"dateOfReminder\":\"" + reminderDate.getText() + "\"}";
                                createReminderOk(reminderJson, currentContent == ContentType.OVERVIEW);
                            }
                        })
                        .build();

                reminderName = (EditText) reminderDialog.getCustomView().findViewById(R.id.location_view_desc_edit_text);
                reminderSubject = (EditText) reminderDialog.getCustomView().findViewById(R.id.edit_text_reminder_subject);
                reminderType = (Spinner) reminderDialog.getCustomView().findViewById(R.id.spinner_reminder_type);
                reminderDate = (TextView) reminderDialog.getCustomView().findViewById(R.id.edit_text_reminder_date);

                reminderDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new MaterialDialog.Builder(reminderDialog.getContext())
                                .customView(R.layout.layout_date_picker, false)
                                .positiveText(android.R.string.ok)
                                .negativeText(android.R.string.cancel)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        DatePicker dp = (DatePicker) dialog.getCustomView().findViewById(R.id.datePicker);
                                        String month = (dp.getMonth() < 10) ? "0" + (dp.getMonth() + 1) : String.valueOf(dp.getMonth() + 1);
                                        String day = (dp.getDayOfMonth() < 10) ? "0" + dp.getDayOfMonth() : String.valueOf(dp.getDayOfMonth());
                                        reminderDate.setText(dp.getYear() + "-" + month + "-" + day);
                                    }
                                })
                                .show();
                    }
                });
                Calendar cal = Calendar.getInstance();
                reminderDate.setText(cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH));

                reminderDialog.show();
                break;
            case R.id.fab_new_post:
                startActivity(new Intent(this, PostActivity.class));
                break;
            case R.id.fab_new_class_message:
                final MaterialDialog classMessageDialog = new MaterialDialog.Builder(this)
                        .title("New message to " + currentSelectedClass.getClassName())
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .inputRange(1, 65535)
                        .positiveText("Submit")
                        .input("Message text", null, false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                createMessageOk(input.toString());
                            }
                        })
                        .show();

                classMessageDialog.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(getClass().getName(), "AYY LMAO " + requestCode + resultCode);
        // 14 is the request code for new lesson activity, too lazy to make it a variable
        if (resultCode == RESULT_OK && requestCode == 14) {
            // Extract name value from result extras
            String name = data.getExtras().getString("name");
            try {
                Lesson newLesson = new Lesson(new JSONObject(data.getStringExtra("newLesson")));
                mCoreS.addNewClassLessonToSend(data.getStringExtra("newLesson"));
                timetablePageFragment.generatedFragments.get(timetablePageFragment.viewPager.getCurrentItem()).getAdapter().onItemAdd(newLesson);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            // Toast the name to display temporarily on screen
            Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
        }

        if (resultCode == RESULT_OK && requestCode == 20) {
            // Extract name value from result extras
            String newLesson = data.getStringExtra("newLesson");
            int lessonToEditPosition = data.getIntExtra("lessonToEditPosition", 0);
            try {
                mCoreS.addNewClassLessonToSend(newLesson);
                timetablePageFragment.generatedFragments.get(timetablePageFragment.viewPager.getCurrentItem()).getAdapter().onItemChange(new Lesson(new JSONObject(newLesson)), lessonToEditPosition);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void setUpTimer(String suppliedTime) {
        cal = Calendar.getInstance();
        try {
            if (!suppliedTime.equals("")) {
                startTimer(tf.parse(suppliedTime + ":00").getTime() - tf.parse(tf.format(cal.getTime())).getTime());
            } else {
                textTimer.setText(suppliedTime);
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
    }

    public void startTimer(final long timeInMillis) {
        Log.d(TAG, "Starting timer with " + timeInMillis);
        new CountDownTimer(timeInMillis, 1000) {

            public void onTick(long millisUntilFinished) {
                String minuteFillerText;
                String secondsFillerText = ":";
                long hours = (millisUntilFinished / (1000 * 60 * 60) % 60);
                long minutes = (millisUntilFinished / (1000 * 60) % 60);
                long seconds = ((millisUntilFinished / 1000) % 60);

                if (seconds < 10) {
                    secondsFillerText = ":0";
                }
                if (hours > 0) {
                    if (minutes < 10) {
                        minuteFillerText = ":0";
                    } else {
                        minuteFillerText = ":";
                    }
                    textTimer.setText(hours + minuteFillerText + minutes + secondsFillerText + seconds);
                } else {
                    if ((millisUntilFinished / 1000) % 60 < 10) {
                        secondsFillerText = ":0";
                    }
                    textTimer.setText(minutes + secondsFillerText + seconds);
                }
            }

            public void onFinish() {
                getTimerDataOk(currentSelectedClass.getCLassId(), Calendar.getInstance().getTime().toString());
                //textTimer.setText("");
            }
        }.start();
    }

    public void createReminderOk(final String reminderJson, final boolean updateView) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", gAcct.getIdToken())
                .add("classId", mUserAccount.getSchoolClasses().get(mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0)).getCLassId())
                .add("reminderData", reminderJson)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/create/createReminder.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.fragment_container), "createReminder : " + response.code() + " " + response.message(), Snackbar.LENGTH_INDEFINITE).show();
                } else {
                    try {
                        JSONObject responseSuccess = new JSONObject(response.body().string());
                        if (responseSuccess.getBoolean("success")) {
                            Snackbar.make(findViewById(R.id.fragment_container), "Reminder successfully created", Snackbar.LENGTH_LONG).show();
                            if (updateView) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        overviewFragment.updateAll();
                                    }
                                });
                            }
                        } else {
                            Snackbar.make(findViewById(R.id.fragment_container), "Could not create reminder.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    public void onTimetableGenerated() {
        Log.d(TAG, "onTimetableGenerated()");
    }

    private void getClassDataOk(final String classId, String dateNow, String numberOfReminders) {
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", gAcct.getIdToken())
                .add("classId", classId)
                .add("dateNow", dateNow)
                .add("numberOfReminders", numberOfReminders)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getClassData.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.fragment_container), "getClassData : " + response.code() + " " + response.message(), Snackbar.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.getJSONObject("data");
                        final JSONObject timetable = jsonData.getJSONObject("timetableData");
                        if (jsonData.has("reminders") && jsonData.getJSONArray("reminders").length() != 0) {
                            mUserAccount.setReminders(jsonData.getJSONArray("reminders"), mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0));
                        }
                        if (jsonData.has("members")) {
                            int tempClassId = mCoreS.getPreferenceInt(CoreSingleton.PREF_SELECTED_CLASS, 0);
                            mUserAccount.getSchoolClass(tempClassId).setMembers(jsonData.getJSONArray("members"));
                            if (mConversationsFragment != null)
                                mConversationsFragment = ConversationsFragment.newInstance(tempClassId);
                        }
                        mCoreS.setUpTimetable(MainActivity.this, currentSelectedClass.getCLassId());
                        overviewFragment = OverviewFragment.newInstance(timetable.toString(), timetable.getBoolean("isCurrent"));
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, overviewFragment).commit();
                        //}
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.progressBarTimetable).setVisibility(View.GONE);
                                try {
                                    //overviewFragment.getActivityOk("[\"" + currentSelectedClass.getCLassId() + "\"]", "0");
                                    Log.d(TAG, "Sent day index = " + timetable.getJSONObject("firstSubject").getInt("dayIndex"));
                                    Log.d(TAG, "Local day index = " + Helper.calendarGet(Calendar.DAY_OF_WEEK));
                                    Log.d(TAG, "isCurrent = " + timetable.getBoolean("isCurrent"));
                                    if (timetable.getJSONObject("firstSubject").getInt("dayIndex") == Helper.calendarGet(Calendar.DAY_OF_WEEK)) {
                                        JSONObject subjectToTime = timetable.getJSONObject("firstSubject");
                                        if (timetable.getBoolean("isCurrent")) {
                                            setUpTimer(subjectToTime.getString("endTime"));
                                        } else {
                                            setUpTimer(subjectToTime.getString("startTime"));
                                        }
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                    } catch (JSONException ex) {
                        ex.printStackTrace();

                    }
                }
            }
        });
    }

    private void getTimerDataOk(final String classId, String dateNow) {
        //Log.d(TAG, "getTimerData");
        RequestBody formBody = new FormBody.Builder()
                .add("idToken", gAcct.getIdToken())
                .add("classId", classId)
                .add("dateNow", dateNow)
                .build();
        Request getUserRequest = new Request.Builder()
                .url("http://www.cheatsheet.sk/php/get/getClassData.php")
                .post(formBody)
                .build();

        mNetworkClient.newCall(getUserRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Snackbar.make(findViewById(R.id.fragment_container), "getTimetableData : " + response.code() + " " + response.message(), Snackbar.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        final JSONObject timetable = jsonResponse.getJSONObject("data").getJSONObject("timetableData");
                        ArrayList<OverviewLesson> lessonsArray = new ArrayList<>();
                        lessonsArray.add(new OverviewLesson(timetable.getJSONObject("firstSubject")));
                        lessonsArray.add(new OverviewLesson(timetable.getJSONObject("secondSubject")));
                        overviewFragment.updateLessons(lessonsArray, timetable.getBoolean("isCurrent"));
                        //}
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.progressBarTimetable).setVisibility(View.GONE);
                                try {
                                    //overviewFragment.getActivityOk("[\"" + currentSelectedClass.getCLassId() + "\"]", "0", false);
                                    if (timetable.getJSONObject("firstSubject").getInt("dayIndex") == Helper.calendarGet(Calendar.DAY_OF_WEEK)) {
                                        JSONObject subjectToTime = timetable.getJSONObject("firstSubject");
                                        if (timetable.getBoolean("isCurrent")) {
                                            setUpTimer(subjectToTime.getString("endTime"));
                                        } else {
                                            setUpTimer(subjectToTime.getString("startTime"));
                                        }
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                    } catch (JSONException ex) {
                        ex.printStackTrace();

                    }
                }
            }
        });
    }

    private enum ContentType {OVERVIEW, TIMETABLE, MESSAGES, REMINDERS}
}
